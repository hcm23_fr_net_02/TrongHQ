Create Database MovieCollection
use MovieCollection

-- 1

CREATE TABLE Movie (
    MovieID INT PRIMARY KEY,
    MovieName VARCHAR(255) NOT NULL,
    Duration INT CHECK (Duration >= 60),
    Genre INT CHECK (Genre BETWEEN 1 AND 8),
    Director VARCHAR(255) NOT NULL,
    BoxOfficeMoney DECIMAL(10, 2),
    Comments VARCHAR(500),
    ImageLink VARCHAR(255) UNIQUE
);


CREATE TABLE Actor (
    ActorID INT PRIMARY KEY,
    ActorName VARCHAR(255) NOT NULL,
    Age INT,
    AvgSalary DECIMAL(10, 2),
    Nationality VARCHAR(100)
);

CREATE TABLE ActedIn (
    MovieID INT,
    ActorID INT,
    FOREIGN KEY (MovieID) REFERENCES Movie(MovieID),
    FOREIGN KEY (ActorID) REFERENCES Actor(ActorID)
);

-- 2

--a
INSERT INTO Movie (MovieID, MovieName, Duration, Genre, Director, BoxOfficeMoney, Comments, ImageLink)
VALUES
    (1, 'Action Movie 1', 120, 1, 'Director 1', 1000000.00, 'Comment 1', 'image1.jpg'),
    (2, 'Comedy Movie 2', 90, 3, 'Director 2', 800000.50, 'Comment 2', 'image2.jpg'),
    (3, 'Drama Movie 3', 150, 5, 'Director 3', 1200000.75, 'Comment 3', 'image3.jpg'),
    (4, 'Horror Movie 4', 110, 6, 'Director 4', 750000.25, 'Comment 4', 'image4.jpg'),
    (5, 'Adventure Movie 5', 135, 2, 'Director 5', 1400000.00, 'Comment 5', 'image5.jpg');


INSERT INTO Actor (ActorID, ActorName, Age, AvgSalary, Nationality)
VALUES
    (1, 'Actor 1', 45, 800000.00, 'Nationality 1'),
    (2, 'Actor 2', 60, 1200000.00, 'Nationality 2'),
    (3, 'Actor 3', 55, 950000.50, 'Nationality 3'),
    (4, 'Actor 4', 40, 700000.75, 'Nationality 4'),
    (5, 'Actor 5', 70, 1500000.25, 'Nationality 5');


INSERT INTO ActedIn (MovieID, ActorID)
VALUES
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5);

-- b
UPDATE Actor
SET ActorName = 'Corrected Actor Name'
WHERE ActorID = 2;


-- 3
-- a
SELECT *
FROM Actor
WHERE Age > 50;
-- b
SELECT ActorName, AvgSalary
FROM Actor
ORDER BY AvgSalary;
-- c
SELECT M.MovieName
FROM Movie M
INNER JOIN ActedIn AI ON M.MovieID = AI.MovieID
INNER JOIN Actor A ON AI.ActorID = A.ActorID
WHERE A.ActorName = 'Corrected Actor Name';
-- d
SELECT M.MovieName
FROM Movie M
INNER JOIN ActedIn AI ON M.MovieID = AI.MovieID
INNER JOIN Actor A ON AI.ActorID = A.ActorID
WHERE M.Genre = 1
GROUP BY M.MovieName
HAVING COUNT(A.ActorID) > 3;


