-- Create the Trainee table

create database FTM
use FTM
CREATE TABLE Trainee (
    TraineeID INT IDENTITY(1,1) PRIMARY KEY,
    Full_Name VARCHAR(255),
    Birth_Date DATE,
    Gender VARCHAR(10) CHECK (Gender IN ('male', 'female')),
    ET_IQ INT CHECK (ET_IQ BETWEEN 0 AND 20),
    ET_Gmath INT CHECK (ET_Gmath BETWEEN 0 AND 20),
    ET_English INT CHECK (ET_English BETWEEN 0 AND 50),
    Training_Class VARCHAR(20),
    Evaluation_Notes TEXT
);


INSERT INTO Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes)
VALUES
    ('John Doe', '1995-03-15', 'male', 12, 15, 40, 'ClassA', 'Good progress.'),
    ('Jane Smith', '1998-06-20', 'female', 18, 16, 48, 'ClassB', 'Excellent performance.'),
    ('Michael Johnson', '1997-09-10', 'male', 14, 18, 45, 'ClassA', 'Meeting expectations.'),
    ('Emily Wilson', '1999-01-25', 'female', 10, 12, 35, 'ClassC', 'Needs improvement.'),
    ('David Brown', '1996-11-05', 'male', 16, 19, 46, 'ClassB', 'Outstanding results.'),
    ('Sarah Lee', '2000-07-08', 'female', 8, 10, 20, 'ClassC', 'Struggling with English.'),
    ('Oliver Davis', '1998-04-12', 'male', 17, 14, 42, 'ClassA', 'Showing great potential.'),
    ('Emma Clark', '1997-02-28', 'female', 19, 17, 48, 'ClassB', 'Consistently high scores.'),
    ('William White', '1999-12-03', 'male', 11, 11, 30, 'ClassC', 'Improvement needed in all areas.'),
    ('Sophia Anderson', '2001-05-22', 'female', 7, 9, 15, 'ClassA', 'Below expectations.');

-- 2

ALTER TABLE Trainee
ADD Fsoft_Account VARCHAR(50) NOT NULL UNIQUE;

-- 3

CREATE VIEW ET_Passed_Trainees AS
SELECT *
FROM Trainee
WHERE ET_IQ + ET_Gmath >= 20 AND ET_IQ >= 8 AND ET_Gmath >= 8 AND ET_English >= 18;

-- 4
SELECT
    DATEPART(MONTH, Birth_Date) AS Birth_Month,
    COUNT(*) AS Total_Passed_Trainees
FROM ET_Passed_Trainees
GROUP BY DATEPART(MONTH, Birth_Date)
ORDER BY Birth_Month;

-- 5
	
SELECT TOP 1
    Full_Name,
    DATEDIFF(YEAR, Birth_Date, GETDATE()) AS Age,
    Gender,
    ET_IQ,
    ET_Gmath,
    ET_English,
    Training_Class,
    Evaluation_Notes
FROM Trainee
ORDER BY LEN(Full_Name) DESC;

