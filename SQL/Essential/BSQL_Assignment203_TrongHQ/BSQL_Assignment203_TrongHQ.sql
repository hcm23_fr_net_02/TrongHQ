use AdventureWorks2008R2
-- 1
SELECT ProductID, Name, Color, ListPrice
FROM Production.Product;
-- 2
SELECT ProductID, Name, Color, ListPrice
FROM Production.Product
WHERE ListPrice > 0;
-- 3
SELECT ProductID, Name, Color, ListPrice
FROM Production.Product
WHERE Color IS NULL;
-- 4
SELECT ProductID, Name, Color, ListPrice
FROM Production.Product
WHERE Color IS NOT NULL;
-- 5
SELECT ProductID, Name, Color, ListPrice
FROM Production.Product
WHERE Color IS NOT NULL AND ListPrice > 0;
-- 6
SELECT CONCAT(Name, ' : ', ISNULL(Color, 'Unknown')) AS 'Name And Color'
FROM Production.Product
WHERE Color IS NOT NULL;
-- 7
SELECT CONCAT('NAME: ', Name, '  --  COLOR: ', ISNULL(Color, 'Unknown')) AS 'Name And Color'
FROM Production.Product
WHERE Color IS NOT NULL;
-- 8 
SELECT ProductID, Name
FROM Production.Product
WHERE ProductID BETWEEN 400 AND 500;
-- 9
SELECT ProductID, Name, Color
FROM Production.Product
WHERE Color IN ('Black', 'Blue');
-- 10
SELECT Name, ListPrice
FROM Production.Product
WHERE Name LIKE 'S%';
-- 11
SELECT Name, ListPrice
FROM Production.Product
WHERE Name LIKE 'S%' OR Name LIKE 'A%'
ORDER BY Name;
-- 12
SELECT Name, ListPrice
FROM Production.Product
WHERE Name LIKE 'SPO[^K]%'
ORDER BY Name;
-- 13
SELECT DISTINCT Color
FROM Production.Product
WHERE Color IS NOT NULL;
-- 14
SELECT DISTINCT ProductSubcategoryID, Color
FROM Production.Product
WHERE ProductSubcategoryID IS NOT NULL AND Color IS NOT NULL;
-- 15
SELECT ProductSubcategoryID, Name, Color, ListPrice
FROM Production.Product
WHERE (Color NOT IN ('Red', 'Black') OR (ListPrice BETWEEN 1000 AND 2000 AND ProductSubcategoryID = 1))
ORDER BY ProductID;
-- 16
SELECT ProductID, Name, ISNULL(Color, 'Unknown') AS Color, ListPrice
FROM Production.Product;
