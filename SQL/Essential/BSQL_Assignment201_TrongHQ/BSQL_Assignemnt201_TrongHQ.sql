CREATE DATABASE EMS

use EMS

CREATE TABLE EMPLOYEE (
    EmpNo INT PRIMARY KEY,
    EmpName VARCHAR(255) NOT NULL,
    BirthDay DATE NOT NULL,
    DeptNo INT NOT NULL,
    MgrNo INT NOT NULL DEFAULT 0,
    StartDate DATE NOT NULL,
    Salary MONEY NOT NULL,
    Level INT CHECK (Level >= 1 AND Level <= 7) NOT NULL,
    Status INT CHECK (Status IN (0, 1, 2)) NOT NULL,
    Note TEXT,
    Email VARCHAR(255) UNIQUE
);

CREATE TABLE SKILL (
    SkillNo INT PRIMARY KEY,
    SkillName VARCHAR(255) NOT NULL,
    Note TEXT
);

CREATE TABLE DEPARTMENT (
    DeptNo INT PRIMARY KEY,
    DeptName VARCHAR(255) NOT NULL,
    Note TEXT
);

CREATE TABLE EMP_SKILL (
    SkillNo INT,
    EmpNo INT,
    SkillLevel INT CHECK (SkillLevel >= 1 AND SkillLevel <= 3) NOT NULL,
    RegDate DATE NOT NULL,
    PRIMARY KEY (SkillNo, EmpNo)
);

-- Q2
ALTER TABLE EMPLOYEE
ADD Email VARCHAR(255) UNIQUE;

ALTER TABLE EMPLOYEE
ADD CONSTRAINT df_MgrNo DEFAULT 0 for MgrNo;

ALTER TABLE EMPLOYEE
ADD CONSTRAINT df_status DEFAULT 0 for Status;


-- Q3

ALTER TABLE EMPLOYEE
ADD CONSTRAINT FK_Employee_Dept
FOREIGN KEY (DeptNo) REFERENCES DEPARTMENT(DeptNo);

ALTER TABLE EMP_SKILL
DROP COLUMN Description;

-- Q4


INSERT INTO EMPLOYEE (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note, Email)
VALUES
    (2, 'Jane Smith', '1992-08-20', 2, 1, '2019-12-15', 55000, 4, 0, 'Note 2', 'jane.smith@example.com'),
    (3, 'Michael Johnson', '1988-03-10', 1, 0, '2018-07-05', 60000, 5, 0, 'Note 3', 'michael.johnson@example.com'),
    (4, 'Emily Wilson', '1995-11-25', 2, 1, '2021-02-28', 48000, 3, 1, 'Note 4', 'emily.wilson@example.com'),
    (5, 'David Brown', '1985-12-03', 1, 0, '2017-09-10', 70000, 6, 0, 'Note 5', 'david.brown@example.com');

INSERT INTO SKILL (SkillName, Note)
VALUES
    ('Database Management', 'Database administration skills'),
    ('Web Development', 'Front-end and back-end web development skills'),
    ('Data Analysis', 'Data analysis and reporting skills'),
    ('Network Security', 'Network security and firewall management skills');


INSERT INTO DEPARTMENT (DeptName, Note)
VALUES
    ('Finance Department', 'Financial management and accounting'),
    ('Marketing Department', 'Marketing and advertising'),
    ('HR Department', 'Human resources and personnel management'),
    ('Sales Department', 'Sales and customer relations');

INSERT INTO EMP_SKILL (SkillNo, EmpNo, SkillLevel, RegDate)
VALUES
    (2, 2, 3, '2022-05-20'),
    (3, 3, 4, '2021-08-12'),
    (4, 4, 2, '2022-01-10'),
    (5, 5, 5, '2020-11-28');

CREATE VIEW EMPLOYEE_TRACKING AS
SELECT EmpNo, EmpName AS Emp_Name, Level
FROM EMPLOYEE
WHERE Level >= 3 AND Level <= 5;
