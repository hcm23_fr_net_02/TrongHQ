use EMS


-- 2
SELECT e.EmpName, e.Email, d.DeptName 
FROM Employee e JOIN Department d ON e.DeptNo = d.DeptNo
WHERE DATEDIFF(MONTH,e.StartDate, GETDATE()) >= 6

--3
SELECT e.EmpName
FROM Employee e JOIN Emp_Skill es ON e.EmpNo = es.EmpNo JOIN Skill s ON es.SkillNo = s.SkillNo
WHERE s.SkillName = 'C++' OR s.SkillName = '.NET'

--4
SELECT e.EmpName, m.EmpName, m.Email
FROM Employee e JOIN Employee m ON e.MgrNo = m.EmpNo
WHERE e.MgrNo IS NOT NULL

--5
SELECT *
FROM Employee e JOIN Department d  on d.DeptNo = e.DeptNo
WHERE e.DeptNo IN (
	SELECT DeptNo 
	FROM Employee
	GROUP BY DeptNo
	HAVING COUNT(*) >= 2
	)

--6
SELECT e.EmpName, e.Email, COUNT(es.SkillNo) as Number_Of_Skills
FROM Employee e JOIN Emp_Skill es ON e.EmpNo = es.EmpNo
GROUP BY e.EmpName, e.Email
ORDER BY e.EmpName asc

--7
SELECT *
FROM Employee e
WHERE e.Status = 1
	AND e.EmpNo IN (
		SELECT EmpNo
		FROM Emp_Skill
		GROUP BY EmpNo
		HAVING Count(*) > 1
	)

--8
CREATE VIEW [LIST ALL EMPLOYEE] AS
SELECT e.EmpName, e.Email, e.BirthDay, d.DeptName
FROM Employee e JOIN Department d ON e.DeptNo = d.DeptNo

