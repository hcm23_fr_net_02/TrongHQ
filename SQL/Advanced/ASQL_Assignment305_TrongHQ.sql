use AdventureWorks2008R2
-- 1
SELECT Name
FROM Production.Product
WHERE ProductSubcategoryID = (
    SELECT ProductSubcategoryID
    FROM Production.ProductSubcategory
    WHERE Name = 'Saddles'
);

-- 2
SELECT Name
FROM Production.Product
WHERE ProductSubcategoryID = (
    SELECT ProductSubcategoryID
    FROM Production.ProductSubcategory
    WHERE Name LIKE 'Bo%'
);
-- 3
SELECT Name
FROM Production.Product
WHERE ListPrice = (
    SELECT MIN(ListPrice)
    FROM Production.Product
    WHERE ProductSubcategoryID = 3
);
-- 4 Part 1
SELECT CountryRegion.Name
FROM Person.CountryRegion
JOIN Person.StateProvince
    ON CountryRegion.CountryRegionCode = StateProvince.CountryRegionCode
GROUP BY CountryRegion.Name
HAVING COUNT(*) < 10;
-- 4 Part 2
SELECT CountryRegion.Name
FROM Person.CountryRegion
LEFT JOIN Person.StateProvince
    ON CountryRegion.CountryRegionCode = StateProvince.CountryRegionCode
GROUP BY CountryRegion.Name
HAVING COUNT(StateProvince.Name) < 10;
-- 5
SELECT P.FirstName, P.LastName
FROM Sales.SalesPerson AS SP
JOIN HumanResources.Employee AS E
    ON E.BusinessEntityID = SP.BusinessEntityID
JOIN Person.Person AS P
    ON E.BusinessEntityID = P.BusinessEntityID
WHERE SP.Bonus > 5000;
-- 6
SELECT FirstName, LastName
FROM Person.Person
WHERE BusinessEntityID IN (
    SELECT BusinessEntityID
    FROM HumanResources.Employee
    WHERE BusinessEntityID IN (
        SELECT BusinessEntityID
        FROM Sales.SalesPerson
        WHERE Bonus > 5000
    )
);



