--Create Database QLNV
use QLNV
-- Create Department
CREATE TABLE Department (
    Department_Number INT PRIMARY KEY,
    Department_Name VARCHAR(255) NOT NULL
);

-- Insert sample data into Department
INSERT INTO Department (Department_Number, Department_Name)
VALUES
    (1, 'HR'),
    (2, 'IT'),
    (3, 'Finance');

-- Create Employee_Table
CREATE TABLE Employee_Table (
    Employee_Number INT PRIMARY KEY,
    Employee_Name VARCHAR(255) NOT NULL,
    Department_Number INT,
    FOREIGN KEY (Department_Number) REFERENCES Department(Department_Number)
);

-- Insert sample data into Employee_Table
INSERT INTO Employee_Table (Employee_Number, Employee_Name, Department_Number)
VALUES
    (1, 'Hoang Quoc Trong', 1),
    (2, 'Hoang Nhat Tien', 2),
    (3, 'Cao Nguyen Minh Quan', 1);

-- Create Skill_Table
CREATE TABLE Skill_Table (
    Skill_Code INT PRIMARY KEY,
    Skill_Name VARCHAR(255) NOT NULL
);

-- Insert sample data into Skill_Table
INSERT INTO Skill_Table (Skill_Code, Skill_Name)
VALUES
    (1, 'Java'),
    (2, 'SQL'),
    (3, 'Python');

-- Create Employee_Skill_Table
CREATE TABLE Employee_Skill_Table (
    Employee_Number INT,
    Skill_Code INT,
    Date_Registered DATE,
    FOREIGN KEY (Employee_Number) REFERENCES Employee_Table(Employee_Number),
    FOREIGN KEY (Skill_Code) REFERENCES Skill_Table(Skill_Code)
);

-- Insert sample data into Employee_Skill_Table
INSERT INTO Employee_Skill_Table (Employee_Number, Skill_Code, Date_Registered)
VALUES
    (1, 1, '2023-09-26'),
    (1, 2, '2023-09-27'),
    (2, 1, '2023-09-28');
