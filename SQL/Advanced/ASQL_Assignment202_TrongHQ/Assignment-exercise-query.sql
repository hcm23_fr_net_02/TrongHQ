-- 3 Write a stored procedure (without parameter) to update employee level to 2 of the
--   employees that has employee level = 1 and has been working at least
--   three year ago (from starting date). Print out the number updated records.

INSERT INTO Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level, Email)
VALUES
    (9, 'Quan', '1980-05-15', 1, 0, '2005-01-10', 10200334, 1, NULL, 1, 'trong@example.com'),
	(10, 'Luong', '1980-05-15', 1, 0, '2005-01-10', 120000, 1, NULL, 1, 'luong@example.com')


CREATE PROCEDURE UpdateEmployeeLevel
AS
BEGIN
    DECLARE @ThreeYearsAgo DATE = DATEADD(YEAR, -3, GETDATE());
    UPDATE Employee
    SET Level = 2
    WHERE Level = 1 AND StartDate <= @ThreeYearsAgo;
END;

exec UpdateEmployeeLevel

-- 4 Write a stored procedure (with EmpNo parameter) to print out employee�s name, 
--   employee�s email address and department�s name of employee that has been out.

CREATE PROCEDURE GetEmployeeDetails
    @EmpNo INT
AS
BEGIN
    SELECT
        e.EmpName AS EmployeeName,
        e.Email AS EmployeeEmail,
        d.DeptName AS DepartmentName
    FROM Employee e
    INNER JOIN Department d ON e.DeptNo = d.DeptNo
    WHERE e.EmpNo = @EmpNo;
END

EXEC GetEmployeeDetails @EmpNo = 9

-- 5. Write a user function named Emp_Tracking (with EmpNo parameter) that return the
--    salary of the employee has been working.

CREATE FUNCTION Emp_Salary (@EmpNo INT)
RETURNS MONEY
AS
BEGIN
    DECLARE @Salary MONEY;

    SELECT @Salary = Salary
    FROM Employee
    WHERE EmpNo = @EmpNo;

    RETURN @Salary;
END

SELECT dbo.Emp_Salary(2) AS EmployeeSalary;


-- 6 Write the trigger(s) to prevent the case that the end user to input invalid
--   employees information (level = 1 and salary >10.000.000).

CREATE TRIGGER PreventInvalidEmployeeInfo
ON Employee
AFTER INSERT, UPDATE
AS
BEGIN
    IF EXISTS (
        SELECT 1
        FROM inserted
        WHERE Level = 1 AND Salary > 10000000
    )
    BEGIN
        ROLLBACK;
        RAISERROR('Invalid employee information: Level 1 employees cannot have a salary greater than 10,000,000.', 16, 1);
    END;
END;

-- Test Trigger
INSERT INTO Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level, Email)
VALUES
    (11, 'Quan', '1980-05-15', 1, 0, '2005-01-10', 1022200334, 1, NULL, 1, 'quan@example.com')



