use DMS

-- 2.
SELECT * 
FROM EMP e
WHERE e.emp_no IN (
	SELECT ep.emp_no
	FROM EMPPROJACT ep
	WHERE e.emp_no = ep.emp_no
)

-- 3.
SELECT *
FROM EMP e JOIN EMPMAJOR em ON e.emp_no = em.emp_no
WHERE em.major = 'MAT' OR em.major = 'CSI'

-- 4.
SELECT *
FROM EMP e JOIN EMPPROJACT ep ON e.emp_no = ep.emp_no
WHERE ep.act_no BETWEEN 90 AND 110

-- 5. provide report based on salary and give out those that have higher salary than average
-- salary in department
-- method 1: JOIN
SELECT e.emp_no , e.last_name,e.first_name, e.salary, e.dept_no,d.dept_name, AVG(e.salary) OVER (PARTITION BY e.dept_no) AS DEPT_AVG_SAL
FROM EMP e JOIN DEPT d on e.dept_no = d.dept_no 
-- method 2: CTE
WITH DEPT_AVG_SAL AS
(
    SELECT
        e.dept_no,
        AVG(e.salary) AS DEPT_AVG_SAL
    FROM EMP e
    GROUP BY e.dept_no
)
SELECT
    e.emp_no,
    e.last_name,
    e.first_name,
    e.salary,
    e.dept_no,
    d.dept_name,
    das.DEPT_AVG_SAL
FROM EMP e
JOIN DEPT d ON e.dept_no = d.dept_no
JOIN DEPT_AVG_SAL das ON e.dept_no = das.dept_no

-- 6.
WITH DEPT_AVG_EDUCATION AS
(
    SELECT
        e.dept_no,
        AVG(e.ed_level) AS DEPT_AVG_EDUCATION
    FROM EMP e
    GROUP BY e.dept_no
)
SELECT
    e.emp_no,
    e.last_name,
    e.first_name,
    e.salary,
    e.dept_no,
	e.ed_level,
    d.dept_name,
    dae.DEPT_AVG_EDUCATION
FROM EMP e
JOIN DEPT d ON e.dept_no = d.dept_no
JOIN DEPT_AVG_EDUCATION dae ON e.dept_no = dae.dept_no
WHERE e.ed_level > dae.DEPT_AVG_EDUCATION

-- 7.
WITH DEPT_PAYROLL AS
(
	SELECT e.dept_no, SUM(e.salary + ISNULL(e.bonus, 0)) AS PAYROLL
	FROM EMP e
	GROUP BY e.dept_no
)
SELECT TOP 1 d.dept_no, d.dept_name, dp.PAYROLL
FROM DEPT d JOIN DEPT_PAYROLL dp ON d.dept_no = dp.dept_no
ORDER BY dp.PAYROLL desc

--8
SELECT emp_no, last_name, first_name, salary
FROM (
    SELECT emp_no, last_name, first_name, salary,
           DENSE_RANK() OVER (ORDER BY salary DESC) AS SalaryRank
    FROM EMP
) RankedEmployees
WHERE SalaryRank <= 5;