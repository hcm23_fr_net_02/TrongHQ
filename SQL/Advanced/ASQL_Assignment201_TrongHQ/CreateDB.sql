use ABC

-- Create the Employee table
CREATE TABLE Employee (
    EmployeeID INT PRIMARY KEY,
    EmployeeLastName VARCHAR(255) NOT NULL,
    EmployeeFirstName VARCHAR(255) NOT NULL,
    EmployeeHireDate DATE NOT NULL,
    EmployeeStatus VARCHAR(50) NOT NULL,
    SupervisorID INT,
    SocialSecurityNumber VARCHAR(20) UNIQUE,
    CONSTRAINT FK_Supervisor FOREIGN KEY (SupervisorID) REFERENCES Employee(EmployeeID)
);

-- Create the Projects table
CREATE TABLE Projects (
    ProjectID INT PRIMARY KEY,
    ProjectName VARCHAR(255) NOT NULL,
    ProjectStartDate DATE NOT NULL,
    ProjectDescription TEXT,
    ProjectDetail TEXT,
    ProjectCompletedOn DATE,
    ProjectManagerID INT NOT NULL,
    CONSTRAINT FK_ProjectManager FOREIGN KEY (ProjectManagerID) REFERENCES Employee(EmployeeID),
    CHECK (ProjectStartDate <= ProjectCompletedOn)
);

-- Create the Project_Modules table
CREATE TABLE Project_Modules (
    ModuleID INT PRIMARY KEY,
    ProjectID INT NOT NULL,
    EmployeeID INT NOT NULL,
    ProjectModulesDate DATE NOT NULL,
    ProjectModulesCompletedOn DATE,
    ProjectModulesDescription TEXT,
    CONSTRAINT FK_ProjectID FOREIGN KEY (ProjectID) REFERENCES Projects(ProjectID),
    CONSTRAINT FK_ModuleEmployee FOREIGN KEY (EmployeeID) REFERENCES Employee(EmployeeID),
    CHECK (ProjectModulesDate <= ProjectModulesCompletedOn)
);

-- Create the Work_Done table
CREATE TABLE Work_Done (
    WorkDoneID INT PRIMARY KEY,
    ModuleID INT NOT NULL,
    WorkDoneDate DATE,
    WorkDoneDescription TEXT,
    WorkDoneStatus VARCHAR(50) NOT NULL,
    CONSTRAINT FK_ModuleID FOREIGN KEY (ModuleID) REFERENCES Project_Modules(ModuleID)
);

INSERT INTO Employee (EmployeeID, EmployeeLastName, EmployeeFirstName, EmployeeHireDate, EmployeeStatus, SupervisorID, SocialSecurityNumber)
VALUES
    (1, 'Smith', 'John', '2020-01-15', 'Active', NULL, '123-45-6789'),
    (2, 'Johnson', 'Jane', '2019-07-20', 'Active', NULL, '987-65-4321'),
    (3, 'Williams', 'Robert', '2022-03-10', 'Active', 1, '555-55-5555'),
    (4, 'Brown', 'Emily', '2021-08-05', 'Active', 2, '111-22-3333'),
    (5, 'Jones', 'Mary', '2023-02-22', 'Active', 3, '444-55-6666'),
    (6, 'Davis', 'Michael', '2020-11-30', 'Active', 1, '777-88-9999'),
    (7, 'Garcia', 'Samantha', '2022-05-12', 'Active', 4, '888-77-6666'),
    (8, 'Martinez', 'David', '2021-09-18', 'Active', 2, '222-33-4444'),
    (9, 'Wilson', 'Olivia', '2022-07-07', 'Active', 1, '999-11-2222'),
    (10, 'Lopez', 'James', '2019-03-25', 'Active', NULL, '333-44-5555');

INSERT INTO Projects (ProjectID, ProjectName, ProjectStartDate, ProjectDescription, ProjectDetail, ProjectCompletedOn, ProjectManagerID)
VALUES
    (101, 'Project Alpha', '2022-01-05', 'Alpha Project Description', 'Alpha Project Detail', '2022-04-05', 1),
    (102, 'Project Beta', '2021-08-15', 'Beta Project Description', 'Beta Project Detail', '2021-11-15', 3),
    (103, 'Project Gamma', '2022-03-20', 'Gamma Project Description', 'Gamma Project Detail', '2022-05-30', 2),
    (104, 'Project Delta', '2021-11-10', 'Delta Project Description', 'Delta Project Detail', '2022-03-01', 4),
    (105, 'Project Epsilon', '2023-05-22', 'Epsilon Project Description', 'Epsilon Project Detail', '2023-08-15', 5),
    (106, 'Project Zeta', '2022-09-30', 'Zeta Project Description', 'Zeta Project Detail', '2023-01-10', 1),
    (107, 'Project Eta', '2023-02-12', 'Eta Project Description', 'Eta Project Detail', '2023-04-20', 6),
    (108, 'Project Theta', '2021-10-18', 'Theta Project Description', 'Theta Project Detail', '2022-02-28', 7),
    (109, 'Project Iota', '2022-08-07', 'Iota Project Description', 'Iota Project Detail', '2023-01-01', 10),
    (110, 'Project Kappa', '2021-04-15', 'Kappa Project Description', 'Kappa Project Detail', '2021-06-30', 8);

INSERT INTO Project_Modules (ModuleID, ProjectID, EmployeeID, ProjectModulesDate, ProjectModulesCompletedOn, ProjectModulesDescription)
VALUES
    (1001, 101, 1, '2022-01-10', '2022-01-20', 'Alpha Module 1'),
    (1002, 101, 2, '2022-01-15', '2022-01-25', 'Alpha Module 2'),
    (1003, 102, 3, '2021-08-20', '2021-09-05', 'Beta Module 1'),
    (1004, 103, 4, '2022-03-25', '2022-04-05', 'Gamma Module 1'),
    (1005, 103, 5, '2022-03-30', '2022-04-15', 'Gamma Module 2'),
    (1006, 104, 6, '2021-11-15', '2021-11-28', 'Delta Module 1'),
    (1007, 105, 7, '2023-05-25', '2023-06-10', 'Epsilon Module 1'),
    (1008, 106, 8, '2022-10-05', '2022-10-20', 'Zeta Module 1'),
    (1009, 107, 9, '2023-02-18', '2023-03-05', 'Eta Module 1'),
    (1010, 110, 10, '2021-04-20', '2021-04-30', 'Kappa Module 1');

INSERT INTO Work_Done (WorkDoneID, ModuleID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus)
VALUES
    (5001, 1001, '2022-01-19', 'Work done for Alpha Module 1', 'Completed'),
    (5002, 1002, '2022-01-23', 'Work done for Alpha Module 2', 'Completed'),
    (5003, 1003, '2021-08-30', 'Work done for Beta Module 1', 'Completed'),
    (5004, 1004, '2022-04-02', 'Work done for Gamma Module 1', 'Completed'),
    (5005, 1005, '2022-04-10', 'Work done for Gamma Module 2', 'In Progress'),
    (5006, 1006, '2021-11-25', 'Work done for Delta Module 1', 'Completed'),
    (5007, 1007, '2023-06-05', 'Work done for Epsilon Module 1', 'In Progress'),
    (5008, 1008, '2022-10-15', 'Work done for Zeta Module 1', 'Completed'),
    (5009, 1009, '2023-03-02', 'Work done for Eta Module 1', 'Completed'),
    (5010, 1010, '2021-04-25', 'Work done for Kappa Module 1', 'Completed');
