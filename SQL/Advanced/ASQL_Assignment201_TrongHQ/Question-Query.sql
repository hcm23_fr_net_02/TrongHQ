
-- B Write a stored procedure (with parameter)
-- to print out the modules that a specific employee has been working on.

CREATE PROCEDURE GetEmployeeModules
    @EmployeeID INT
AS
BEGIN
    SELECT 
        pm.ModuleID,
        pm.ProjectModulesDescription
    FROM Project_Modules pm
    WHERE pm.EmployeeID = @EmployeeID
END

EXEC GetEmployeeModules @EmployeeID = 2

-- C Write a user function (with parameter) that return the Works information that a 
--   specific employee has been involving though those were not assigned to him/her.

CREATE FUNCTION GetEmployeeInvolvement (@EmployeeID INT)
RETURNS TABLE
AS
RETURN (
SELECT 
        wd.WorkDoneID,
        wd.WorkDoneDate,
        wd.WorkDoneDescription
    FROM Work_Done wd
    INNER JOIN Project_Modules pm ON wd.ModuleID = pm.ModuleID
    WHERE pm.EmployeeID = @EmployeeID AND wd.WorkDoneStatus = 'Completed'
)

SELECT *
FROM GetEmployeeInvolvement(3);


-- D Write the trigger(s) to prevent the case that the end user
--   to input invalid Projects and Project Modules information

CREATE TRIGGER PreventInvalidProjectsAndModules
ON Project_Modules
FOR INSERT, UPDATE
AS
BEGIN
    IF EXISTS (
        SELECT 1
        FROM inserted i
        INNER JOIN Projects p ON i.ProjectID = p.ProjectID
        WHERE i.ProjectModulesDate < p.ProjectStartDate 
			OR i.ProjectModulesCompletedOn > p.ProjectCompletedOn
    )
    BEGIN
        RAISERROR('Invalid Project Modules. Check Module dates against Project dates.', 16, 1);
        ROLLBACK;
    END
END
