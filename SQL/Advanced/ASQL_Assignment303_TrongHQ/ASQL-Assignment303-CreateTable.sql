--CREATE DATABASE ESM2

use ESM2

-- Create the DEPARTMENT table
CREATE TABLE [dbo].[DEPARTMENT](
	[DeptNo] [int] IDENTITY(1,1) PRIMARY KEY,
	[DeptName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
);

-- Create the SKILL table
CREATE TABLE [dbo].[SKILL](
	[SkillNo] [int] IDENTITY(1,1) PRIMARY KEY,
	[SkillName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
);

-- Create the EMPLOYEE table
CREATE TABLE [dbo].[EMPLOYEE](
	[EmpNo] [int] PRIMARY KEY,
	[EmpName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[BirthDay] [datetime] NOT NULL,
	[Email] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS UNIQUE NOT NULL,
	[DeptNo] [int] NOT NULL,
	[MgrNo] [int] NOT NULL DEFAULT 0,
	[StartDate] [datetime] NOT NULL,
	[Salary] [money] NOT NULL,
	[Level] [int] NOT NULL CHECK ([Level] BETWEEN 1 AND 7),
	[Status] [int] NOT NULL DEFAULT 0 CHECK ([Status] IN (0, 1, 2)),
	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	FOREIGN KEY ([DeptNo]) REFERENCES [dbo].[DEPARTMENT]([DeptNo])
);

-- Create the EMP_SKILL table
CREATE TABLE [dbo].[EMP_SKILL](
	[SkillNo] [int] NOT NULL,
	[EmpNo] [int] NOT NULL,
	[SkillLevel] [int] NOT NULL CHECK ([SkillLevel] BETWEEN 1 AND 3),
	[RegDate] [datetime] NOT NULL,
	[Description] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	PRIMARY KEY ([SkillNo], [EmpNo]),
	FOREIGN KEY ([SkillNo]) REFERENCES [dbo].[SKILL]([SkillNo]),
	FOREIGN KEY ([EmpNo]) REFERENCES [dbo].[EMPLOYEE]([EmpNo])
);
-- Add records to the DEPARTMENT table
INSERT INTO DEPARTMENT (DeptName, Note) VALUES
('HR', 'Human Resources Department'),
('IT', 'Information Technology Department'),
('SALES', 'Sales Department'),
('FINANCE', 'Finance Department'),
('MARKETING', 'Marketing Department'),
('ENGINEERING', 'Engineering Department'),
('SUPPORT', 'Customer Support Department'),
('ADMIN', 'Administration Department');

-- Add records to the SKILL table
INSERT INTO SKILL (SkillName, Note) VALUES
('Java', 'Java programming language'),
('C++', 'C++ programming language'),
('.NET', '.NET framework'),
('SQL', 'SQL database management'),
('Python', 'Python programming language'),
('Web Development', 'Web development skills'),
('Project Management', 'Project management skills'),
('Communication', 'Effective communication skills');

-- Add records to the EMPLOYEE table
INSERT INTO EMPLOYEE (EmpNo, EmpName, BirthDay, Email, DeptNo, StartDate, Salary, Level, Status, Note) VALUES
(1, 'John Doe', '1990-05-15', 'john.doe@fsoft.com', 1, '2023-01-15', 60000000, 4, 0, 'Senior Developer'),
(2, 'Jane Smith', '1988-08-20', 'jane.smith@fsoft.com', 2, '2022-03-10', 75000000, 5, 0, 'Marketing Manager'),
(3, 'Bob Johnson', '1995-03-02', 'bob.johnson@fsoft.com', 3, '2023-02-28', 55000000, 3, 0, 'Sales Representative'),
(4, 'Alice Brown', '1992-11-12', 'alice.brown@fsoft.com', 1, '2021-07-05', 70000000, 6, 0, 'HR Specialist'),
(5, 'David Wilson', '1987-06-30', 'david.wilson@fsoft.com', 2, '2022-06-15', 90000000, 7, 0, 'IT Director'),
(6, 'Eva Green', '1994-09-25', 'eva.green@fsoft.com', 3, '2023-03-20', 60000000, 4, 1, 'On Leave'),
(7, 'Michael Lee', '1998-12-18', 'michael.lee@fsoft.com', 4, '2023-04-10', 65000000, 3, 0, 'Financial Analyst'),
(8, 'Sophia Turner', '1991-04-08', 'sophia.turner@fsoft.com', 5, '2022-08-12', 72000000, 5, 0, 'Web Developer');

-- Add records to the EMP_SKILL table
INSERT INTO EMP_SKILL (SkillNo, EmpNo, SkillLevel, RegDate, Description) VALUES
(1, 1, 2, '2023-02-01', 'Java programming'),
(2, 1, 3, '2023-02-01', 'C++ programming'),
(3, 2, 2, '2022-04-01', '.NET development'),
(4, 3, 1, '2023-03-15', 'SQL expertise'),
(5, 4, 3, '2021-08-01', 'Python programming'),
(6, 5, 2, '2022-07-01', 'Web application development'),
(7, 6, 1, '2023-04-01', 'Sales strategies'),
(8, 7, 3, '2023-05-01', 'Financial modeling');
