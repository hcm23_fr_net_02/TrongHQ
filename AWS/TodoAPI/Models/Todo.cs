using Amazon.DynamoDBv2.DataModel;

namespace TodoAPI.Models
{
    [DynamoDBTable("Todo")]
    public class Todo
    {
        [DynamoDBHashKey("id")]
        public string? Id { get; set; }
        [DynamoDBProperty("description")]
        public string? Description { get; set; } = string.Empty;
        [DynamoDBProperty("title")]
        public string? Title { get; set; } = string.Empty;
    }
}