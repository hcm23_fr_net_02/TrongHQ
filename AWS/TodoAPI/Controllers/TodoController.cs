using Amazon.DynamoDBv2.DataModel;
using Microsoft.AspNetCore.Mvc;
using TodoAPI.Models;

namespace TodoAPI.Controllers;

[Route("[controller]")]
[ApiController]
public class TodoController : ControllerBase
{
    private readonly IDynamoDBContext _context;
    public TodoController(IDynamoDBContext context)
    {
        _context = context;
    }
    [HttpGet("{todoId}")]
    public async Task<IActionResult> GetById(int todoId)
    {
        var todo = await _context.LoadAsync<Todo>(todoId.ToString());
        if (todo == null) return NotFound();
        return Ok(todo);
    }
    [HttpGet]
    public async Task<IActionResult> GetAllTodo()
    {
        var student = await _context.ScanAsync<Todo>(default).GetRemainingAsync();
        return Ok(student);
    }
    [HttpPost]
    public async Task<IActionResult> CreateTodo([FromBody] Todo todoRequest)
    {
        var todo = await _context.LoadAsync<Todo>(todoRequest.Id);
        if (todo != null) return BadRequest($"Student with Id {todo.Id} Already Exists");
        await _context.SaveAsync(todoRequest);
        return Ok(todoRequest);
    }
    [HttpDelete("{todoId}")]
    public async Task<IActionResult> DeleteTodo(string todoId)
    {
        var todo = await _context.LoadAsync<Todo>(todoId);
        if (todo == null) return NotFound();
        await _context.DeleteAsync(todo);
        return NoContent();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateTodo([FromBody] Todo todoRequest)
    {
        var todo = await _context.LoadAsync<Todo>(todoRequest.Id);
        if (todo == null) return NotFound();
        await _context.SaveAsync(todoRequest);
        return Ok(todoRequest);
    }
}