﻿// See https://aka.ms/new-console-template for more information
using System.Text;
using TrongHQ_CSharpAssignment;

Console.OutputEncoding = Encoding.UTF8;
QuanLySinhVien QLSV = new QuanLySinhVien();

bool isChoosing = true;
while (isChoosing)
{
    try
    {
        QLSV.Menu();
        int choice;
        if (int.TryParse(Console.ReadLine(), out choice))
        {
            Console.Clear();
            switch (choice)
            {
                case 1:
                    Console.Write("Nhập họ tên: ");
                    string name = Console.ReadLine();
                    Console.Write("Nhập tuổi: ");
                    int age = -1;

                    while (!int.TryParse(Console.ReadLine(), out age) || age < 0)
                    {
                        Console.WriteLine("Tuổi không hợp lệ vui lòng nhập lại!");
                        Console.Write("Nhập tuổi: ");
                    }
                    double GPA = -1;
                    Console.Write("Nhập GPA: ");

                    while (!double.TryParse(Console.ReadLine(), out GPA) || GPA < 0 || GPA > 10)
                    {
                        Console.WriteLine("GPA không hợp lệ vui lòng nhập lại!");
                        Console.Write("Nhập GPA: ");
                    }
                    SinhVien sv = new SinhVien { Id = QLSV.CountId, Age = age, Name = name, GPA = GPA };
                    QLSV.CountId++;
                    QLSV.AddSinhVien(sv);
                    Console.WriteLine("Thêm sinh viên thành công!");
                    Console.WriteLine("Nhập bất kỳ nút nào để tiếp tục");
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 2:
                    QLSV.DanhSachSinhVien();
                    Console.WriteLine("Nhập bất kỳ nút nào để tiếp tục");
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 3:
                    Console.Write("Nhập từ khóa tìm kiếm (Mã SV hoặc Tên): ");
                    string keyWord = Console.ReadLine();
                    QLSV.TimSinhVien(keyWord);
                    break;
                case 4:
                    Console.Write("Nhập từ khóa để xóa (Mã SV): ");
                    int idDelete;
                    while (!int.TryParse(Console.ReadLine(), out idDelete))
                    {
                        Console.WriteLine("Id không hợp lệ vui lòng nhập lại");
                        Console.Write("Nhập từ khóa để xóa (Mã SV): ");
                    }
                    QLSV.XoaSinhVien(idDelete);
                    break;
                case 5:
                    isChoosing = false;
                    Console.WriteLine("Cảm ơn bạn đã sử dụng chương trình!");
                    Console.WriteLine("Nhập bất kỳ nút nào để thoát");
                    Console.ReadKey();
                    break;
                default:
                    throw new Exception("Lựa chọn menu không hợp lệ!");
            }
        }
        else
        {
            throw new Exception("Dữ liệu nhập vào không hợp lệ vui lòng nhập lại");
        }
    }
    catch (Exception ex)
    { 
        Console.Clear();
        Console.WriteLine(ex.Message);        
    }

}

