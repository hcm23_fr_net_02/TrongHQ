﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrongHQ_CSharpAssignment
{
    public class QuanLySinhVien
    {
        public List<SinhVien> SinhVien { get; set; }
        public int CountId { get; set; }
        public QuanLySinhVien()
        {
            SinhVien = new List<SinhVien>();
            CountId = 0;
        }
       
        public void Menu()
        {
            Console.WriteLine("********* Menu chính *********");
            Console.WriteLine("1. Thêm sinh viên");
            Console.WriteLine("2. Xem danh sách sinh viên");
            Console.WriteLine("3. Tìm sinh viên");
            Console.WriteLine("4. Nhập mã sinh viên để xóa");
            Console.WriteLine("5. Thoát");
        }
        public void MenuAdd()
        {
            Console.WriteLine("********* Menu xem danh sách sinh viên *********");
            Console.WriteLine("1. Trang trước");
            Console.WriteLine("2. Trang tiếp theo");
            Console.WriteLine("3. Quay lại menu chính");
        }

        public void AddSinhVien(SinhVien sv)
        {
            SinhVien.Add(sv);
        }
        public void DanhSachSinhVien()
        {
            bool isChoosing = true;
            int numPage = 0;
            int choice;
            while (isChoosing)
            {
                MenuAdd();
                Console.WriteLine("\n\n");
                Console.WriteLine("------------------\n");
                Console.WriteLine("Trang hiện tại: " + (numPage + 1));
                var listSinhVien = SinhVien.Skip(numPage * 5).Take(5).ToList();
                Console.WriteLine("Mã SV || Họ tên || Tuổi || GPA");
                foreach(var sv in listSinhVien)
                {
                    Console.WriteLine(sv.Id + " || " + sv.Name + " || " + sv.Age + " || " + sv.GPA);
                }
                Console.WriteLine("------------------\n");
                if (int.TryParse(Console.ReadLine(), out choice))
                {
                    switch(choice)
                    {
                        case 1:
                            if(numPage == 0)
                            {
                                Console.Clear();
                                Console.WriteLine("Đang ở trang đầu tiên vui lòng nhập lại");
                            }
                            else
                            {
                                numPage--;
                                Console.Clear();
                            }
                            break;
                        case 2:
                            numPage++;
                            listSinhVien = SinhVien.Skip(numPage * 5).Take(5).ToList();
                            if (listSinhVien.Count == 0)
                            {
                                Console.Clear();
                                Console.WriteLine("Không có dữ liệu cho trang tiếp vui lòng nhập lại");
                                numPage--;
                            }
                            break;
                        case 3:
                            isChoosing = false;
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Lựa chọn không hợp lệ vui lòng nhập lại!");
                            break;
                    }
                }
            }
        }

        public void TimSinhVien(string keyWord)
        {
            int idSV;
            SinhVien findSinhvien;
            if(int.TryParse(keyWord,out idSV))
            {
                 findSinhvien = SinhVien.Find(sv => sv.Id == Int32.Parse(keyWord));
            }
            else
            {
                findSinhvien = SinhVien.Find(sv => sv.Name == keyWord);
            }
            if(findSinhvien != null)
            {
                Console.WriteLine("Kết quả tìm được là: ");
                Console.WriteLine(findSinhvien.Id + " || " + findSinhvien.Name + " || " + findSinhvien.Age + " || " + findSinhvien.GPA);
            }
            else
            {
                Console.WriteLine("Không thể tìm được sinh viên!");
            }
            Console.WriteLine("Nhập bất kỳ nút nào để tiếp tục");
            Console.ReadKey();
            Console.Clear();
        }

        public void XoaSinhVien(int id)
        {
            var findSinhvien = SinhVien.Find(sv => sv.Id == id);
            if(findSinhvien is null)
            {
                Console.WriteLine("Mã SV không tìm được");
            }
            else
            {
                SinhVien.Remove(findSinhvien);
                Console.WriteLine("Xóa sinh viên thành công");
            }
            Console.WriteLine("Nhập bất kỳ nút nào để tiếp tục");
            Console.ReadKey();
            Console.Clear();

        }


    }
}
