﻿
using Microsoft.Extensions.DependencyInjection;
using Moq;
using MovieReview.Helper;
using MovieReview.Models;
using MovieReview.Services.Interfaces;

namespace MovieReviewTest
{
    public class ReviewTest
    {
        [Fact]
        public async void AddMovie_ValidMovie_CallsService()
        {
            // Arrange
            IServiceCollection services = new ServiceCollection().ConfigureDependencyInjection();

            var movieServiceMock = new Mock<IMovieService>();

            var expectedMovie = new Movie() { Id = 1, Title = "TestTitle", Description = "Test description", PublicationYear = 2021, Reviews = new List<Review>() };

            movieServiceMock.Setup(service => service.GetById(It.IsAny<int>())).ReturnsAsync(expectedMovie);

            var reviewServiceMock = new Mock<IReviewService>();
            services.AddScoped(_ => reviewServiceMock.Object);
            services.AddScoped(_ => movieServiceMock.Object);

            var serviceProvider = services.BuildServiceProvider();

            var reviewService = serviceProvider.GetRequiredService<IReviewService>();
            var movieService = serviceProvider.GetRequiredService<IMovieService>();

            var movieToAdd = await movieService.GetById(1);
            var reviewToAdd = new Review() { Title = "Test review", Description = "Test description", Rating = 4, MovieId = movieToAdd.Id };

            // Act
            await reviewService.AddReview(reviewToAdd, movieToAdd);

            // Assert
            reviewServiceMock.Verify(service => service.AddReview(It.IsAny<Review>(), It.IsAny<Movie>()), Times.Once);
        }
    }
}
