﻿
using Microsoft.Extensions.DependencyInjection;
using Moq;
using MovieReview.Models;
using MovieReview.Helper;
using MovieReview.Services.Interfaces;

namespace MovieReviewTest
{
    public class MovieTest
    {
        public readonly Mock<IMovieService> _movieServiceMock = new();

        [Fact]
        public async Task GetById_ExistingMovie_ReturnsMovie()
        {

            // Arrange
            var expectedMovie = new Movie() { Id = 1, Title = "TestTitle", Description = "Test description", PublicationYear = 2021, Reviews = new List<Review>() };
            var movieServiceMock = new Mock<IMovieService>();
            movieServiceMock.Setup(service => service.GetById(1))
                .ReturnsAsync(expectedMovie);
            var movieService = movieServiceMock.Object;

            // Act
            var result = await movieService.GetById(1);

            // Assert
            Assert.Equal(expectedMovie, result);
        }

        [Fact]
        public void AddMovie_ValidMovie_CallsService()
        {
            // Arrange
            IServiceCollection services = new ServiceCollection().ConfigureDependencyInjection();
            var movieServiceMock = new Mock<IMovieService>();
            services.AddScoped(_ => movieServiceMock.Object);

            var serviceProvider = services.BuildServiceProvider();

            var movieService = serviceProvider.GetRequiredService<IMovieService>();

            var movieToAdd = new Movie { Title = "New Movie", Description = "Description", PublicationYear = 2023 };

            // Act
            movieService.AddMovie(movieToAdd);

            // Assert
            movieServiceMock.Verify(service => service.AddMovie(It.IsAny<Movie>()), Times.Once);
        }

        [Fact]
        public async Task RemoveMovie_ValidMovie_CallsServiceRemove()
        {
            // Arrange
            IServiceCollection services = new ServiceCollection().ConfigureDependencyInjection();
            var movieServiceMock = new Mock<IMovieService>();

            var expectedMovie = new Movie() { Id = 1, Title = "TestTitle", Description = "Test description", PublicationYear = 2021, Reviews = new List<Review>() };

            movieServiceMock.Setup(service => service.GetById(It.IsAny<int>())).ReturnsAsync(expectedMovie);

            services.AddScoped(_ => movieServiceMock.Object);

            var serviceProvider = services.BuildServiceProvider();

            var movieService = serviceProvider.GetRequiredService<IMovieService>();

            var movieToRemove = await movieService.GetById(1);

            // Act
            await movieService.RemoveMovie(movieToRemove);

            // Assert
            movieServiceMock.Verify(service => service.RemoveMovie(It.IsAny<Movie>()), Times.Once);
        }

        [Fact]
        public async Task UpdateMovie_ValidMovie_CallsServiceUpdate()
        {
            // Arrange
            IServiceCollection services = new ServiceCollection().ConfigureDependencyInjection();
            var movieServiceMock = new Mock<IMovieService>();
            var expectedMovie = new Movie() { Id = 1, Title = "TestTitle", Description = "Test description", PublicationYear = 2021, Reviews = new List<Review>() };
            movieServiceMock.Setup(service => service.GetById(It.IsAny<int>())).ReturnsAsync(expectedMovie);

            services.AddScoped(_ => movieServiceMock.Object);
            var serviceProvider = services.BuildServiceProvider();
            var movieService = serviceProvider.GetRequiredService<IMovieService>();
            var movieToUpdate = await movieService.GetById(1);
            movieToUpdate.Title = "Test";

            // Act
            await movieService.UpdateMovie(movieToUpdate);
            var result = await movieService.GetById(1);

            // Assert
            Assert.Equal(movieToUpdate, result);
            movieServiceMock.Verify(service => service.UpdateMovie(It.IsAny<Movie>()), Times.Once);
        }
    }
}
