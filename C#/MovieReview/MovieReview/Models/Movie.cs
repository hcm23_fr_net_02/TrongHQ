﻿using MovieReview.Common;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieReview.Models
{
    public class Movie
    {

        private int _id;
        private string _title;
        private string _description;
        private int _publicationYear;
        private string _director;
        private string _country;
        private string _genre;
        private List<Review> _reviews;

        [JsonConstructor]
        public Movie(int id, string title, string description, int publicationYear, string director, string country, string genre, List<Review> reviews)
        {
            Id = id;
            Title = title;
            Description = description;
            PublicationYear = publicationYear;
            Director = director;
            Country = country;
            Genre = genre;
            Reviews = reviews;
        }
        public Movie(string title, string description, int publicationYear, string director, string country, string genre, List<Review> reviews)
        {
            Title = title;
            Description = description;
            PublicationYear = publicationYear;
            Director = director;
            Country = country;
            Genre = genre;
            Reviews = new List<Review>();
        }

        public Movie() { }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [Required]
        public string Title
        {
            get { return _title; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.MessageTitle  + MovieConstants.DataNotValid);
                _title = value;
            }
        }

        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
            }
        }

        [Required]
        public int PublicationYear
        {
            get { return _publicationYear; }
            set
            {
                if (_publicationYear > DateTime.Now.Year)
                {
                    throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.MessagePublicationYear + MovieConstants.DataNotValid);
                }
                _publicationYear = value;
            }
        }

        public string Director
        {
            get { return _director; }
            set { _director = value; }
        }

        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }

        public string Genre
        {
            get { return _genre; }
            set { _genre = value; }
        }

        public List<Review> Reviews
        {
            get { return _reviews; }
            set { _reviews = value; }
        }

        public override string ToString()
        {
            return $"{_id} || {_title} || {_description} || {_publicationYear} || {_director} || {_country} || {_genre}";
        }
    }
}

