﻿using MovieReview.Common;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieReview.Models
{
    public class Review
    {
        private int _id;
        private string _title;
        private string _description;
        private int _rating;
        private int _movieId;
        private Movie _movie;

        public Review(int id, string title, string description, int rating, int movieId, Movie movie)
        {
            Id = id;
            Title = title;
            Description = description;
            Rating = rating;
            MovieId = movieId;
            Movie = movie;
        }

        [JsonConstructor]
        public Review(string title, string description, int rating, int movieId, Movie movie)
        {
            Title = title;
            Description = description;
            Rating = rating;
            MovieId = movieId;
            Movie = movie;
        }

        public Review() { }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [Required]
        public string Title
        {
            get { return _title; }
            set
            {
                if(string.IsNullOrEmpty(value))
                    throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.MessageTitle + MovieConstants.DataNotValid);
                _title = value;
            }
        }

        [Required]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [Required]
        public int Rating
        {
            get { return _rating; }
            set 
            {
                if(value < 1 || value > 5)
                    throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.MessageRating + MovieConstants.DataNotValid);
                _rating = value;
            }
        }

        [Required]
        public int MovieId
        {
            get { return _movieId; }
            set { _movieId = value; }
        }

        [ForeignKey("MovieId")]
        public Movie Movie
        {
            get { return _movie; }
            set { _movie = value; }
        }

        public override string ToString()
        {
            return $"{_id} || {_title} || {_description} || {_rating}" ;

        }
    }
}
