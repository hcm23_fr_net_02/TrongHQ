﻿
using MovieReview.Models;

namespace MovieReview.Services.Interfaces
{
    public interface IReviewService
    {
        Task<List<Review>> GetReviewsByMovie(Movie movie);
        Task AddReview(Review review, Movie movie);
        Task SaveChanges();
    }
}
