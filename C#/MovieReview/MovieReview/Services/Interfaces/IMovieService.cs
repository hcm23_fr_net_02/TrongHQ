﻿
using MovieReview.Models;

namespace MovieReview.Services.Interfaces
{
    public interface IMovieService
    {
        Task AddMovie(Movie movie);
        Task<Movie> GetById(int id);
        Task<List<Movie>> GetAll();
        Task RemoveMovie(Movie movie);
        Task UpdateMovie(Movie movie);
        Task<List<Movie>> GetPaginationMovies(int page);
        Task<List<Movie>> SearchMovie(string keyword, int type);
        Task SaveChanges();
    }
}
