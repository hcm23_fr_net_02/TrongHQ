﻿using Microsoft.Extensions.DependencyInjection;
using MovieReview.Models;
using MovieReview.Repositories.Interfaces;
using MovieReview.Services.Interfaces;

namespace MovieReview.Services.Implements
{
    public class ReviewService : IReviewService
    {
        private IUnitOfWork _unitOfWork;
        private IServiceProvider _serviceProvider;

        public ReviewService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task AddReview(Review review, Movie movie)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                _unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                if (review == null || movie == null)
                    throw new Exception("Dữ liệu rỗng");
                await _unitOfWork.ReviewRepository.Add(new Review() { Title = review.Title, Description = review.Description, MovieId = movie.Id, Rating = review.Rating});
                try
                {
                    await SaveChanges();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }
            }
            
        }

        public async Task<List<Review>> GetReviewsByMovie(Movie movie)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                
                _unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                return await _unitOfWork.ReviewRepository.GetReviews(movie);
            }
        }

        public async Task SaveChanges()
        {
            await _unitOfWork.SaveChanges();
        }
    }
}
