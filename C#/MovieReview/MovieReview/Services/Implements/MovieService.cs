﻿
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using MovieReview.Common;
using MovieReview.Models;
using MovieReview.Repositories.Interfaces;
using MovieReview.Services.Interfaces;

namespace MovieReview.Services.Implements
{
    public class MovieService : IMovieService
    {
        private IUnitOfWork _unitOfWork;
        private IServiceProvider _serviceProvider;

        public MovieService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task AddMovie(Movie movie)
        {
            if (movie.PublicationYear > DateTime.Now.Year)
                throw new Exception("Thời gian phát hành không hợp lệ");
            using (var scope = _serviceProvider.CreateScope())
            {
                _unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                await _unitOfWork.MovieRepository.Add(movie);
                await _unitOfWork.SaveChanges();
            }

        }


        public async Task<Movie> GetById(int id)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                _unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                Movie movie = await _unitOfWork.MovieRepository.GetById(id);
                if (movie == null)
                    throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.DataEmpty);
                return movie;
            }

        }

        public async Task<List<Movie>> GetPaginationMovies(int page)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                _unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                if (page < 0)
                {
                    throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.FirstPage);
                }
                List<Movie> movies = await _unitOfWork.MovieRepository.GetListPagination(page);
                if (movies.IsNullOrEmpty())
                {
                    throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.LastPage);
                }
                return movies;
            }

        }

        public async Task RemoveMovie(Movie movie)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                _unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();

                foreach(Review review in movie.Reviews)
                {
                    _unitOfWork.ReviewRepository.Delete(review);
                }

                _unitOfWork.MovieRepository.Delete(movie);
                await SaveChanges();
            }
        }

        public async Task<List<Movie>> SearchMovie(string keyword, int type)
        {

            List<Movie> movies;
            List<Movie> allMovies;
            using (var scope = _serviceProvider.CreateScope())
            {
                _unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                allMovies = await _unitOfWork.MovieRepository.GetAll();
            }
            switch (type)
            {
                case (int)EnumSearchMovie.Director:
                    movies = allMovies.FindAll(movie => movie.Director.Contains(keyword));
                    break;
                case (int)EnumSearchMovie.Country:
                    movies = allMovies.FindAll(movie => movie.Country.Contains(keyword));
                    break;
                case (int)EnumSearchMovie.Name:
                    movies = allMovies.FindAll(movie => movie.Title.Contains(keyword));
                    break;
                case (int)EnumSearchMovie.PublicationYear:
                    if (int.TryParse(keyword, out int publicationYear))
                    {
                        movies = allMovies.FindAll(movie => movie.PublicationYear == publicationYear);
                    }
                    else
                    {
                        throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.DataNotValid);

                    }
                    break;
                default:
                    throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.DataNotValid);

            }
            if (movies.IsNullOrEmpty())
            {
                throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.DataEmpty);

            }
            return movies;

        }

        public async Task UpdateMovie(Movie movie)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                _unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                _unitOfWork.MovieRepository.Update(movie);
                await SaveChanges();
            }

        }

        public async Task SaveChanges()
        {
            await _unitOfWork.SaveChanges();
        }

        public async Task<List<Movie>> GetAll()
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                _unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                return await _unitOfWork.MovieRepository.GetAll();
            }

        }
    }
}
