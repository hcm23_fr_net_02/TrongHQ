﻿
using MovieReview.Models;

namespace MovieReview.Repositories.Interfaces
{
    public interface IMovieRepository : IGenericRepository<Movie>
    {
        Task<List<Movie>> GetListPagination(int pageIndex);
    }
}
