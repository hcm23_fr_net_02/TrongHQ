﻿namespace MovieReview.Repositories.Interfaces
{
    public interface IUnitOfWork
    {
        IMovieRepository MovieRepository { get; }
        IReviewRepository ReviewRepository { get; }
        Task<int> SaveChanges();
    }
}
