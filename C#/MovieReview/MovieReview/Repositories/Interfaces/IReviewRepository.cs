﻿
using MovieReview.Models;

namespace MovieReview.Repositories.Interfaces
{
    public interface IReviewRepository : IGenericRepository<Review>
    {
        Task<List<Review>> GetReviews(Movie movie);
    }
}
