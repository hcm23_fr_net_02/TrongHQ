﻿
using Microsoft.EntityFrameworkCore;
using MovieReview.DataAccess;
using MovieReview.Models;
using MovieReview.Repositories.Interfaces;

namespace MovieReview.Repositories.Implements
{
    public class MovieRepository : GenericRepository<Movie>, IMovieRepository
    {
        public MovieRepository(DataContext context) : base(context)
        {
        }

        public async Task<List<Movie>> GetListPagination(int pageIndex)
        {
            try
            {
                var datas = await DbSet.Include(movie => movie.Reviews).Skip(pageIndex * 5).Take(5).ToListAsync();
                return datas;
            }
            catch
            {
                throw;
            }
        }

        public override async Task<List<Movie>> GetAll()
        {
            return await DbSet.Include(movie => movie.Reviews).ToListAsync();
        }

        public override async Task<Movie> GetById(int id)
        {
            return DbSet.Include(movie => movie.Reviews).FirstOrDefault(movie => movie.Id == id);
        }
    }
}
