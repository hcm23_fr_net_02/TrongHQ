﻿
using Microsoft.EntityFrameworkCore;
using MovieReview.DataAccess;
using MovieReview.Models;
using MovieReview.Repositories.Interfaces;

namespace MovieReview.Repositories.Implements
{
    public class ReviewRepository : GenericRepository<Review>, IReviewRepository
    {
        public ReviewRepository(DataContext context) : base(context)
        {

        }

        public async Task<List<Review>> GetReviews(Movie movie)
        {
            return await DbSet.Where(review => review.MovieId == movie.Id).ToListAsync();
        }
    }
}
