﻿
using Microsoft.EntityFrameworkCore;
using MovieReview.DataAccess;
using MovieReview.Repositories.Interfaces;

namespace MovieReview.Repositories.Implements
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private DataContext _context;
        private DbSet<T> _dbSet;

        public DataContext Context { get { return _context; } }
        public DbSet<T> DbSet { get { return _dbSet; } }

        public GenericRepository(DataContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }

        public async Task Add(T entity)
        {
            await _dbSet.AddAsync(entity);
        }

        public async void Delete(T entity)
        {
            _dbSet.Remove(entity);
        }

        public virtual async Task<List<T>> GetAll()
        {
            return await _dbSet.ToListAsync();
        }

        public virtual async Task<T> GetById(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public void Update(T entity)
        {
            _dbSet.Update(entity);
        }
    }
}
