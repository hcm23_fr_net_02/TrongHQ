﻿using Microsoft.EntityFrameworkCore;
using MovieReview.DataAccess;
using MovieReview.Repositories.Interfaces;

namespace MovieReview.Repositories.Implements
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _context;
        public IMovieRepository MovieRepository { get; }
        public IReviewRepository ReviewRepository { get; }


        public UnitOfWork(DataContext context, IMovieRepository movieRepository, IReviewRepository reviewRepository)
        {
            _context = context;
            MovieRepository = movieRepository;
            ReviewRepository = reviewRepository;
        }
        //public void Dispose()
        //{
        //    _context.Dispose();
        //    GC.SuppressFinalize(this);
        //}

        public Task<int> SaveChanges()
        {
            return _context.SaveChangesAsync();
        }
    }
}
