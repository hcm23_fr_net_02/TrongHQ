﻿
using Microsoft.Extensions.DependencyInjection;
using MovieReview.Helper;
using MovieReview.Presenter.Interfaces;
using System.Text;

IServiceProvider serviceProvider = new ServiceCollection().ConfigureDependencyInjection().BuildServiceProvider();
IMainPresenter presenter = serviceProvider.GetRequiredService<IMainPresenter>();

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;

await presenter.ShowMenu();