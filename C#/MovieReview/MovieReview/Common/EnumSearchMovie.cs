﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieReview.Common
{
    public enum EnumSearchMovie
    {
        Name = 1,
        Director = 2,
        PublicationYear = 3,
        Country = 4
    }
}
