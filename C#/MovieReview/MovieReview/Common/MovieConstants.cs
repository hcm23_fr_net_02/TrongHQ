﻿
namespace MovieReview.Common
{
    public static class MovieConstants
    {

        #region Await
        public const string Wait = "Vui lòng chờ...";
        public const string Ended = "Hoàn thành";
        #endregion

        #region Exception
        public const string Exception = "Exception";
        public const string DatabaseError = "Lỗi kết nối database";
        public const string UnexpectedError = "Lỗi không xác định";
        public const string DataNotValid = "Lỗi Data nhập vào không hợp lệ";
        public const string DataDuplicated = "Lỗi Data nhập bị trùng lặp";
        public const string DataNoChange = "Không có sự thay đổi!";
        public const string DataEmpty = "Lỗi Data rỗng hoặc không tìm thấy";
        public const string FirstPage = "Trang đầu tiên!";
        public const string LastPage = "Trang cuối cùng!";
        #endregion

        #region Choice
        public const string Choice = "Nhập lựa chọn của bạn: ";

        public const string NextPage = "N";
        public const string PreviousPage = "P";
        public const string ToBack = "B";
        public const string Find = "F";

        public const string Yes = "Y"; 
        public const string No = "N";

        public const string FirstChoice = "1";
        public const string SecondChoice = "2";
        public const string ThirdChoice = "3";
        public const string FourthChoice = "4";
        public const string FifthChoice = "5";
        public const string SixthChoice = "6";
        #endregion

        #region Common
        public const string HeaderMovie = "ID || Title || Description " +
            "|| Description || Publication Year || Director || Country || Genres";
        public const string MessageInput = "Nhập ";
        public const string MessageTitle = "Tiêu đề";
        public const string MessageRating = "Rating";
        public const string MessageRatingScore = "(1 - 5)";
        public const string MessageDescription = "Nội dung";
        public const string MessageDirector = "Tên đạo diễn";
        public const string MessagePublicationYear = "Năm sản xuất";
        public const string MessageQuantity = "Số lượng";
        public const string MessageGenre = "Thể loại";
        public const string MessageCountry = "Quốc gia";
        public const string MessageColon = ": ";
        public const string MessageAddSuccess = "Thêm thành công!";
        public const string MessageUpdateSuccess = "Chỉnh sửa thành công!";
        public const string MessageKeyPress = "Nhấn bất kỳ nút nào để thoát";
        public const string MessageDeleteMovie = "Bạn có muốn xóa bộ phim? (Y,N)";
        public const string MessageDeleteSuccess = "Xóa phim thành công";
        public const string MessageKeyword = "keyword";


        public const string MessageFindId = "Nhập ID để xem chi tiết: ";
        #endregion

        #region Menu
        public const string MainMenu =
              "----------- Menu phim -----------\n"
            + "1. Xem danh sách phim\n"
            + "2. Thêm phim mới\n"
            + "3. Lọc phim\n"
            + "4. Thoát ứng dụng\n";

        public const string SubMenuViewMovie =
              "----------- Menu danh sách -----------\n"
            + "\"N\" để xem trang tiếp theo\n"
            + "\"P\" để quay lại trang trước\n"
            + "\"B\" để lưu những thay đổi và quay lại menu chính\n"
            + "\"F\" để tìm thông tin chi tiết của bộ phim\n";

        public const string SubMovieMenuDetail =
              "----------- Menu chi tiết bộ phim -----------\n"
            + "1. Chỉnh sửa thông tin phim\n"
            + "2. Xóa bộ phim\n"
            + "3. Thêm đánh giá cho bộ phim\n"
            + "4. Xem danh sách tất cả đánh giá của bộ phim\n"
            + "5. Quay lại menu danh sách\n";

        public const string SubMenuEditMovie =
              "----------- Menu chỉnh sửa thông tin bộ phim -----------\n"
            + "1. Chỉnh sửa tiêu đề bộ phim\n"
            + "2. Chỉnh sửa tên đạo diễn\n"
            + "3. Chỉnh sửa năm xuất bản\n"
            + "4. Chỉnh sửa quốc gia\n"
            + "5. Hoàn thành và quay về trang chi tiết\n";

        public const string SubMenuSearchMovie =
              "----------- Menu tìm phim -----------\n"
            + "1. Tìm theo tựa đề (Title)\n"
            + "2. Tìm theo tên đạo diễn (Director)\n"
            + "3. Tìm theo năm xuất bản (Publication Year)\n"
            + "4. Tìm theo quốc gia (Country)\n"
            + "5. Quay lại menu chính\n";

        #endregion

    }
}
