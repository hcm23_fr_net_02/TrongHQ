﻿
namespace MovieReview.Presenter.Interfaces
{
    public interface IMainPresenter
    {
        Task ShowMenu();
        Task ToListMovie();
        Task ToAddMovie();
        Task ToSearchMovie();
        void ToExit();
    }
}
