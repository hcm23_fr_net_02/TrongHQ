﻿
using MovieReview.Models;

namespace MovieReview.Presenter.Interfaces
{
    public interface IMovieDetailPresenter
    {
        Task ShowMenu();
        Movie ToUpdate(Movie movie);
        Task<bool> Remove(Movie movie);
        Task AddReview(Movie movie);
        Task<List<Review>> ListReview(Movie movie);
    }
}
