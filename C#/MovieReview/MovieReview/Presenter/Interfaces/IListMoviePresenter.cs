﻿
using MovieReview.Models;

namespace MovieReview.Presenter.Interfaces
{
    public interface IListMoviePresenter
    {
        Task ShowListAsync();
        Task ToMovieDetail();
    }
}
