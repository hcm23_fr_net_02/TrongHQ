﻿
namespace MovieReview.Presenter.Interfaces
{
    public interface IAddMoviePresenter
    {
        Task AddMovie();
    }
}
