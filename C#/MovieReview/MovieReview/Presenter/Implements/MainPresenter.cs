﻿
using Microsoft.Extensions.DependencyInjection;
using MovieReview.Common;
using MovieReview.Helper;
using MovieReview.Presenter.Interfaces;

namespace MovieReview.Presenter.Implements
{
    public class MainPresenter : IMainPresenter
    {
        private IServiceProvider _serviceProvider;
        public MainPresenter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task ShowMenu()
        {
            while (true)
            {
                try
                {
                    MessageHelper.ShowMessage(MovieConstants.MainMenu);
                    MessageHelper.ShowMessage(MovieConstants.Choice);
                    string choice = Console.ReadLine();
                    switch (choice)
                    {
                        case MovieConstants.FirstChoice:
                            await ToListMovie();
                            MessageHelper.MessageClear();
                            break;
                        case MovieConstants.SecondChoice:
                            await ToAddMovie();
                            break;
                        case MovieConstants.ThirdChoice:
                            await ToSearchMovie();
                            break;
                        case MovieConstants.FourthChoice:
                            ToExit();
                            break;
                        default:
                            throw new Exception(MovieConstants.DataNotValid);
                    }
                }
                catch (Exception ex)
                {
                    MessageHelper.MessageClear();
                    MessageHelper.ShowMessage(ex.Message);
                }
            }
        }

        public async Task ToAddMovie()
        {
            MessageHelper.ShowMessage(MovieConstants.Wait);
            IAddMoviePresenter addMoviePresenter = _serviceProvider.GetRequiredService<IAddMoviePresenter>();
            await addMoviePresenter.AddMovie();
        }

        public void ToExit()
        {
            return;
        }

        public async Task ToListMovie()
        {
            MessageHelper.ShowMessage(MovieConstants.Wait);

            IListMoviePresenter listMoviePresenter = _serviceProvider.GetRequiredService<IListMoviePresenter>();
            await listMoviePresenter.ShowListAsync();

        }

        public async Task ToSearchMovie()
        {
            MessageHelper.ShowMessage(MovieConstants.Wait);

            ISearchMoviePresenter searchMoviePresenter = _serviceProvider.GetRequiredService<ISearchMoviePresenter>();
            await searchMoviePresenter.ShowMenu();
        }
    }
}
