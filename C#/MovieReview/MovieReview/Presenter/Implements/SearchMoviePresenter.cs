﻿
using MovieReview.Common;
using MovieReview.Helper;
using MovieReview.Presenter.Interfaces;
using MovieReview.Services.Interfaces;
using System.Reflection.Metadata;

namespace MovieReview.Presenter.Implements
{
    public class SearchMoviePresenter : ISearchMoviePresenter
    {
        private IMovieService _movieService;

        public SearchMoviePresenter(IMovieService movieService)
        {
            _movieService = movieService;
        }

        public async Task ShowMenu()
        {
            MessageHelper.MessageClear();
            while (true)
            {
                try
                {
                    MessageHelper.ShowMessage(MovieConstants.SubMenuSearchMovie);
                    MessageHelper.ShowMessage(MovieConstants.Choice);
                    if (int.TryParse(Console.ReadLine(), out int choice))
                    {
                        if (choice < 0 || choice > 5)
                        {
                            throw new Exception(MovieConstants.DataNotValid);
                        }
                        if (choice == 5)
                        {
                            MessageHelper.MessageClear();
                            break;
                        }
                        MessageHelper.ShowMessage(MovieConstants.MessageInput + MovieConstants.MessageKeyword + MovieConstants.MessageColon);
                        string keyword = Console.ReadLine();
                        MessageHelper.ShowMessage(MovieConstants.Wait);
                        var movies = await _movieService.SearchMovie(keyword, choice);
                        if (movies.Count == 0)
                            throw new Exception(MovieConstants.DataEmpty);
                        Console.Clear();
                        MessageHelper.ShowMessageLine(MovieConstants.HeaderMovie);
                        foreach (var movie in movies)
                        {
                            MessageHelper.ShowMessageLine(movie.ToString());
                        }
                    }
                    else
                    {
                        throw new Exception(MovieConstants.DataNotValid);
                    }
                }
                catch (Exception ex)
                {
                    MessageHelper.MessageClear();
                    MessageHelper.ShowMessageLine(ex.Message);
                }
                
            }
        }
    }
}
