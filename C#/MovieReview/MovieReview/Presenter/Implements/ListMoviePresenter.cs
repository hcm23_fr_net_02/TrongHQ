﻿using Microsoft.Extensions.DependencyInjection;
using MovieReview.Common;
using MovieReview.Helper;
using MovieReview.Models;
using MovieReview.Presenter.Interfaces;
using MovieReview.Services.Interfaces;

namespace MovieReview.Presenter.Implements
{
    public class ListMoviePresenter : IListMoviePresenter
    {
        private IServiceProvider _serviceProvider;
        private IMovieService _movieService;

        public ListMoviePresenter(IMovieService movieService, IServiceProvider serviceProvider)
        {
            _movieService = movieService;
            _serviceProvider = serviceProvider;
        }
        public async Task ShowListAsync()
        {
            MessageHelper.MessageClear();
            int page = 0;
            while (true)
            {
                try
                {
                    List<Movie> movies = new List<Movie>();
                    MessageHelper.ShowMessageLine(MovieConstants.Wait);
                    movies = await _movieService.GetPaginationMovies(page);
                    MessageHelper.ShowMessageLine(MovieConstants.Ended);
                    MessageHelper.ShowMessageLine(MovieConstants.HeaderMovie);
                    movies.ForEach(movie =>
                    {
                        MessageHelper.ShowMessageLine(movie.ToString());
                    });
                    MessageHelper.ShowMessage(MovieConstants.SubMenuViewMovie);
                    MessageHelper.ShowMessage(MovieConstants.Choice);
                    string choice = Console.ReadLine();
                    switch (choice.ToUpper())
                    {
                        case MovieConstants.NextPage:
                            page++;
                            break;
                        case MovieConstants.PreviousPage:
                            page--;
                            break;
                        case MovieConstants.ToBack:
                            return;
                        case MovieConstants.Find:
                            await ToMovieDetail();
                            break;
                        default:
                            throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.DataNotValid);
                    }
                    MessageHelper.ShowMessageLine(MovieConstants.Wait);
                    MessageHelper.MessageClear();
                }
                catch (Exception ex)
                {
                    if (ex.Message == MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.FirstPage)
                        page = 0;
                    else if (ex.Message == MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.LastPage)
                        page--;
                    Console.Clear();
                    MessageHelper.ShowMessageLine(ex.Message);
                }

            }
        }

        public async Task ToMovieDetail()
        {
            IMovieDetailPresenter movieDetailPresenter = _serviceProvider.GetRequiredService<IMovieDetailPresenter>();
            await movieDetailPresenter.ShowMenu();
        }
    }
}
