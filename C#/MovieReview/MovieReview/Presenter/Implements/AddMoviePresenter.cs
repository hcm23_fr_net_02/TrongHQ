﻿
using MovieReview.Common;
using MovieReview.Helper;
using MovieReview.Models;
using MovieReview.Presenter.Interfaces;
using MovieReview.Services.Interfaces;

namespace MovieReview.Presenter.Implements
{
    public class AddMoviePresenter : IAddMoviePresenter
    {
        private readonly IMovieService _movieService;
        public AddMoviePresenter(IMovieService movieService)
        {
            _movieService = movieService;
        }
        public async Task AddMovie()
        {
            try
            {
                Console.Clear();
                MessageHelper.ShowMessageLine(MovieConstants.MessageInput + MovieConstants.MessageTitle.ToLower() + MovieConstants.MessageColon);
                string title = Console.ReadLine();
                MessageHelper.ShowMessageLine(MovieConstants.MessageInput + MovieConstants.MessageDescription.ToLower() + MovieConstants.MessageColon);
                string description = Console.ReadLine();
                MessageHelper.ShowMessageLine(MovieConstants.MessageInput + MovieConstants.MessageDirector.ToLower() + MovieConstants.MessageColon);
                string director = Console.ReadLine();
                MessageHelper.ShowMessageLine(MovieConstants.MessageInput + MovieConstants.MessageCountry.ToLower() + MovieConstants.MessageColon);
                string country = Console.ReadLine();
                MessageHelper.ShowMessageLine(MovieConstants.MessageInput + MovieConstants.MessageGenre.ToLower() + MovieConstants.MessageColon);
                string genre = Console.ReadLine();
                MessageHelper.ShowMessageLine(MovieConstants.MessageInput + MovieConstants.MessagePublicationYear.ToLower() + MovieConstants.MessageColon);
                int publicationYear;
                if (!int.TryParse(Console.ReadLine(), out publicationYear))
                    throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.DataNotValid);

                Movie movie = new Movie(title, description, publicationYear, director, country, genre, new List<Review>());
                MessageHelper.ShowMessageLine(MovieConstants.Wait);
                await _movieService.AddMovie(movie);
                MessageHelper.ShowMessageLine(MovieConstants.Ended);
                MessageHelper.ShowMessageLine(MovieConstants.MessageAddSuccess);
                MessageHelper.ShowMessageLine(MovieConstants.MessageKeyPress);
                Console.ReadKey();
                MessageHelper.MessageClear();


            }
            catch (Exception ex)
            {
                MessageHelper.MessageClear();
                MessageHelper.ShowMessageLine(ex.Message);
            }

        }
    }
}
