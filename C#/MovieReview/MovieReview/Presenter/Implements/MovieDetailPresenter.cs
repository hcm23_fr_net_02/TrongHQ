﻿
using Microsoft.IdentityModel.Tokens;
using MovieReview.Common;
using MovieReview.Helper;
using MovieReview.Models;
using MovieReview.Presenter.Interfaces;
using MovieReview.Services.Interfaces;

namespace MovieReview.Presenter.Implements
{
    public class MovieDetailPresenter : IMovieDetailPresenter
    {
        private IMovieService _movieService;
        private IReviewService _reviewService;
        public MovieDetailPresenter(IServiceProvider serviceProvider, IMovieService movieService, IReviewService reviewService)
        {
            _movieService = movieService;
            _reviewService = reviewService;
        }

        public async Task AddReview(Movie movie)
        {
            Console.Clear();
            MessageHelper.ShowMessageLine(MovieConstants.MessageInput + MovieConstants.MessageTitle.ToLower() + MovieConstants.MessageColon);
            string title = Console.ReadLine();
            MessageHelper.ShowMessageLine(MovieConstants.MessageInput + MovieConstants.MessageDescription.ToLower() + MovieConstants.MessageColon);
            string description = Console.ReadLine();
            MessageHelper.ShowMessageLine(MovieConstants.MessageInput + MovieConstants.MessageRating.ToLower() + MovieConstants.MessageRatingScore + MovieConstants.MessageColon);
            int rating;
            if (!int.TryParse(Console.ReadLine(), out rating))
                throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.DataNotValid);
            Review review = new Review(title, description, rating, movie.Id, movie);
            MessageHelper.ShowMessage(MovieConstants.Wait);
            await _reviewService.AddReview(review, movie);

        }

        public async Task<List<Review>> ListReview(Movie movie)
        {
            return await _reviewService.GetReviewsByMovie(movie);
        }

        public async Task<bool> Remove(Movie movie)
        {
            if (Console.ReadLine().ToUpper() == MovieConstants.Yes)
            {
                MessageHelper.ShowMessageLine(MovieConstants.Wait);
                await _movieService.RemoveMovie(movie);
                return true;
            }
            return false;

        }

        public async Task ShowMenu()
        {
            MessageHelper.ShowMessageLine(MovieConstants.MessageFindId);
            int id;
            if (!int.TryParse(Console.ReadLine(), out id))
            {
                throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.DataNotValid);
            }
            MessageHelper.ShowMessageLine(MovieConstants.Wait);
            Movie movie = await _movieService.GetById(id);

            MessageHelper.MessageClear();
            while (true)
            {
                try
                {
                    MessageHelper.ShowMessageLine(MovieConstants.HeaderMovie);
                    MessageHelper.ShowMessageLine(movie.ToString());
                    MessageHelper.ShowMessage(MovieConstants.SubMovieMenuDetail);
                    MessageHelper.ShowMessage(MovieConstants.Choice);
                    string choice;
                    switch (choice = Console.ReadLine())
                    {
                        case MovieConstants.FirstChoice:
                            Movie editMovie = ToUpdate(new Movie(movie.Id, movie.Title, movie.Description, movie.PublicationYear, movie.Director, movie.Country, movie.Genre, movie.Reviews));
                            if (editMovie == null)
                                throw new Exception(MovieConstants.DataNoChange);
                            movie.Title = editMovie.Title;
                            movie.Director = editMovie.Director;
                            movie.Country = editMovie.Country;
                            movie.PublicationYear = editMovie.PublicationYear;
                            await _movieService.UpdateMovie(movie);
                            MessageHelper.MessageClear();
                            break ;
                        case MovieConstants.SecondChoice:
                            MessageHelper.ShowMessageLine(MovieConstants.MessageDeleteMovie);
                            bool isRemoved = await Remove(movie);
                            if (isRemoved)
                            {
                                MessageHelper.MessageClear();
                                MessageHelper.ShowMessageLine(MovieConstants.MessageDeleteSuccess);
                                return;
                            }
                            break;

                        case MovieConstants.ThirdChoice:
                            await AddReview(movie);
                            MessageHelper.MessageClear();
                            MessageHelper.ShowMessage(MovieConstants.MessageAddSuccess);
                            break;
                        case MovieConstants.FourthChoice:
                            Console.Clear();
                            List<Review> reviews = await ListReview(movie);
                            if (reviews.IsNullOrEmpty())
                                throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.DataEmpty);
                            MessageHelper.MessageClear();
                            MessageHelper.ShowMessageLine(MovieConstants.HeaderMovie);
                            foreach(Review review in reviews)
                            {
                                MessageHelper.ShowMessageLine(review.ToString());
                            }
                            MessageHelper.ShowMessageLine(MovieConstants.MessageKeyPress);
                            Console.ReadKey();
                            MessageHelper.MessageClear();
                            break;
                        case MovieConstants.FifthChoice:
                            MessageHelper.MessageClear();
                            return;
                        default:
                            throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.DataNotValid);
                    }
                }
                catch (Exception e)
                {
                    Console.Clear();
                    Console.WriteLine(e.Message);
                }

            }

        }

        public Movie ToUpdate(Movie movie)
        {
            bool isChanged = false;
            Console.Clear();
            while (true)
            {
                MessageHelper.ShowMessageLine(MovieConstants.HeaderMovie);
                MessageHelper.ShowMessageLine(movie.ToString());
                try
                {
                    MessageHelper.ShowMessage(MovieConstants.SubMenuEditMovie);
                    MessageHelper.ShowMessage(MovieConstants.Choice);
                    string choice;
                    switch (choice = Console.ReadLine())
                    {
                        case MovieConstants.FirstChoice:
                            MessageHelper.ShowMessage(MovieConstants.MessageInput + MovieConstants.MessageTitle + MovieConstants.MessageColon);
                            string title = Console.ReadLine();
                            if (movie.Title == title)
                                throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.DataDuplicated);

                            movie.Title = title;
                            isChanged = true;
                            Console.Clear();
                            break;
                        case MovieConstants.SecondChoice:
                            MessageHelper.ShowMessage(MovieConstants.MessageInput + MovieConstants.MessageDirector + MovieConstants.MessageColon);
                            MessageHelper.ShowMessage(MovieConstants.MessageInput + MovieConstants.MessageDirector + MovieConstants.MessageColon);
                            string director = Console.ReadLine();
                            if (movie.Director == director)
                                throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.DataDuplicated);

                            movie.Director = director;
                            isChanged = true;
                            Console.Clear();
                            break;
                        case MovieConstants.ThirdChoice:
                            MessageHelper.ShowMessage(MovieConstants.MessageInput + MovieConstants.MessagePublicationYear + MovieConstants.MessageColon);

                            if (int.TryParse(Console.ReadLine(), out int publicationYear))
                            {
                                if (movie.PublicationYear == publicationYear)
                                    throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.DataDuplicated);

                                movie.PublicationYear = publicationYear;
                                isChanged = true;
                            }
                            else
                            {
                                throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.DataNotValid);
                            }
                            Console.Clear();
                            break;
                        case MovieConstants.FourthChoice:
                            MessageHelper.ShowMessage(MovieConstants.MessageInput + MovieConstants.MessageCountry + MovieConstants.MessageColon);

                            string country = Console.ReadLine();
                            if (movie.Country == country)
                                throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.DataDuplicated);

                            movie.Country = country;
                            isChanged = true;
                            Console.Clear();
                            break;
                        case MovieConstants.FifthChoice:
                            Console.Clear();
                            if (!isChanged)
                            {
                                return null;
                            }
                            return movie;
                        default:
                            throw new Exception(MovieConstants.Exception + MovieConstants.MessageColon + MovieConstants.DataNotValid);

                    }
                }
                catch (Exception e)
                {
                    Console.Clear();
                    Console.WriteLine(e.Message);
                }
            }
        }
    }
}
