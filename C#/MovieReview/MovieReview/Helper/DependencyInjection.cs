﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MovieReview.DataAccess;
using MovieReview.Presenter.Implements;
using MovieReview.Presenter.Interfaces;
using MovieReview.Repositories.Implements;
using MovieReview.Repositories.Interfaces;
using MovieReview.Services.Implements;
using MovieReview.Services.Interfaces;

namespace MovieReview.Helper
{
    public static class DependencyInjection
    {
        public static IServiceCollection ConfigureDependencyInjection(this IServiceCollection services)
        {
            // Unit of work
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            // Repositories
            services.AddScoped<IMovieRepository, MovieRepository>();
            services.AddScoped<IReviewRepository, ReviewRepository>();

            // Services
            services.AddScoped<IMovieService, MovieService>();
            services.AddScoped<IReviewService, ReviewService>();

            // Database
            services.AddDbContext<DataContext>();

            // Presenter
            services.AddScoped<IMainPresenter, MainPresenter>();
            services.AddScoped<IListMoviePresenter, ListMoviePresenter>();
            services.AddScoped<IAddMoviePresenter, AddMoviePresenter>();
            services.AddScoped<IMovieDetailPresenter, MovieDetailPresenter>();
            services.AddScoped<ISearchMoviePresenter, SearchMoviePresenter>();

            return services;
        }
    }

}
