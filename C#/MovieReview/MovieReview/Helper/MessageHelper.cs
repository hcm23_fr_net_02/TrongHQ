﻿
namespace MovieReview.Helper
{
    public static class MessageHelper
    {
        public static void ShowMessage(string message)
        {
            Console.Write(message);
        }
        public static void ShowMessageLine(string message)
        {
            Console.WriteLine(message);
        }
        public static void MessageClear()
        {
            Console.Clear();
        }
    }
}
