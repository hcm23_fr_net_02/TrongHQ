﻿
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MovieReview.Models;
using Newtonsoft.Json;
using System.Data;
using System.Diagnostics;

namespace MovieReview.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Review> Reviews { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory().Split("bin").FirstOrDefault())
            .AddJsonFile("appsettings.json");

            var configuration = builder.Build();

            optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>()
                    .HasMany(r => r.Reviews)
                    .WithOne(m => m.Movie)
                    .HasForeignKey(ra => ra.MovieId);

            SeedDataFromJsonFile(modelBuilder);

        }

        public void CheckConnect(string connectionString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    if (!(connection.State == ConnectionState.Open))
                    {
                        throw new Exception("Không tìm thấy local db");
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }


        private void SeedDataFromJsonFile(ModelBuilder modelBuilder)
        {
            // Load JSON data from the file
            string jsonPath = AppDomain.CurrentDomain.BaseDirectory.Split("bin").First() + "Data\\movie-data.json";
            var json = File.ReadAllText(jsonPath);

            // Deserialize the JSON data into objects
            List<Movie> seedData = JsonConvert.DeserializeObject<List<Movie>>(json);
            int reviewIndex = 1;
            foreach (Movie movie in seedData)
            {
                modelBuilder.Entity<Movie>().HasData(new Movie(movie.Id, movie.Title, movie.Description, movie.PublicationYear, movie.Director, movie.Country, movie.Genre, new List<Review>()));
                foreach (Review review in movie.Reviews)
                {
                    modelBuilder.Entity<Review>().HasData(new Review
                    {
                        Id = reviewIndex++,
                        Description = review.Description,
                        Title = review.Title,
                        Rating = review.Rating,
                        MovieId = movie.Id,
                    });
                }
            }
        }
    }
}
