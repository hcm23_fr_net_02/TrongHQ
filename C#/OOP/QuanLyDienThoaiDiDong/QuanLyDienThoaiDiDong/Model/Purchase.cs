﻿
namespace QuanLyDienThoaiDiDong.Model
{
    public class Purchase
    {
        public string PurchaseId { get; set; }
        public DateTime PurchaseDate { get; set; }
        public List<MobilePhone> PhonesPurchased { get; set; } = new List<MobilePhone>();
        public Customer Customer { get; set; } = new Customer();

        public void AddPhone(MobilePhone phone)
        {
            PhonesPurchased.Add(phone);
        }

        public void ListPurchasedPhones()
        {
            foreach (var phone in PhonesPurchased)
            {
                Console.WriteLine($"Product Id: {phone.ProductId}, Name: {phone.Name}, Price: {phone.Price:C}");
            }
        }
    }
}
