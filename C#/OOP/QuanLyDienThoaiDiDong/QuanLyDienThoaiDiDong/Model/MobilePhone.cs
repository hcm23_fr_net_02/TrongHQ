﻿
namespace QuanLyDienThoaiDiDong.Model
{
    public class MobilePhone
    {
        public string ProductId { get; set; }
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
