﻿
namespace QuanLyDienThoaiDiDong.Model
{
    public class Customer
    {
        public string MemberId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public List<Purchase> Purchases { get; set; } = new List<Purchase>();
    }
}
