﻿
namespace QuanLyDienThoaiDiDong.Model
{
    public class Store
    {
        public List<MobilePhone> Inventory {  get; set; } = new List<MobilePhone>();

        public void AddPhone(MobilePhone phone)
        {
            Inventory.Add(phone);
        }

        public void RemovePhone(string productCode)
        {
            Inventory.RemoveAll(phone => phone.ProductId == productCode);
        }

        public void ListPhones()
        {
            foreach (var phone in Inventory)
            {
                Console.WriteLine($"Phone Id: {phone.ProductId}, Name: {phone.Name}, Manufacturer: {phone.Manufacturer}, Quantity: {phone.Quantity}, Price: {phone.Price}");
            }
        }
    }
}
