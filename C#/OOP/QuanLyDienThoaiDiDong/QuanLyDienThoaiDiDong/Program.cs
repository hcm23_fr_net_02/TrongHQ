﻿using QuanLyDienThoaiDiDong.Model;
using static System.Formats.Asn1.AsnWriter;

bool exit = false;
Store store = new Store();
List<Customer> customers = new List<Customer>();
List<Purchase> purchases = new List<Purchase>();
int choice;
while (!exit)
{
    Console.WriteLine("Mobile Phone Store Management System");
    Console.WriteLine("1. Add a new phone to the store");
    Console.WriteLine("2. Remove a phone from the store");
    Console.WriteLine("3. List phones in the store");
    Console.WriteLine("4. Allow a customer to make a purchase");
    Console.WriteLine("5. List purchases");
    Console.WriteLine("6. Search for a phone by product id");
    Console.WriteLine("7. Exit");
    Console.Write("Enter your choice (1-7): ");



    if (int.TryParse(Console.ReadLine(), out choice))
    {
        switch (choice)
        {
            case 1:
                Console.WriteLine("Enter phone details:");
                Console.Write("Product id: ");
                string productid = Console.ReadLine();
                Console.Write("Name: ");
                string name = Console.ReadLine();
                Console.Write("Manufacturer: ");
                string manufacturer = Console.ReadLine();
                Console.Write("Quantity: ");
                int quantity;
                if (int.TryParse(Console.ReadLine(), out quantity))
                {
                    Console.Write("Price: ");
                    decimal price;
                    if (decimal.TryParse(Console.ReadLine(), out price))
                    {
                        MobilePhone phone = new MobilePhone
                        {
                            ProductId = productid,
                            Name = name,
                            Manufacturer = manufacturer,
                            Quantity = quantity,
                            Price = price
                        };
                        store.AddPhone(phone);
                        Console.WriteLine("Phone added to the store.");
                    }
                    else
                    {
                        Console.WriteLine("Invalid price. Please enter a valid number.");
                    }
                }
                else
                {
                    Console.WriteLine("Invalid quantity. Please enter a valid number.");
                }
                break;
            case 2:
                Console.Write("Enter the product id of the phone to remove: ");
                string productidRemove = Console.ReadLine();
                if (store.Inventory.Any(x => x.ProductId == productidRemove))
                {
                    store.RemovePhone(productidRemove);
                    Console.WriteLine("Phone removed from the store.");
                }
                else
                {
                    Console.WriteLine("Phone not found");
                }
                    
                break;
            case 3:
                if(store.Inventory.Count == 0)
                {
                    Console.WriteLine("Store have no phone!");
                }
                else
                {
                    Console.WriteLine("Phones in the store:");
                    store.ListPhones();
                }
                break;
            case 4:
                Console.WriteLine("Enter customer details:");
                Console.Write("Member ID: ");
                string memberId = Console.ReadLine();
                Console.Write("Name: ");
                string namePurchase = Console.ReadLine();
                Console.Write("Address: ");
                string address = Console.ReadLine();

                Customer customer = new Customer
                {
                    MemberId = memberId,
                    Name = namePurchase,
                    Address = address
                };

                Console.WriteLine("Select a phone for purchase:");
                Console.WriteLine("Phones in the store:");
                store.ListPhones();
                Console.Write("Enter the product id of the phone to purchase: ");
                string productId = Console.ReadLine();
                MobilePhone selectedPhone = store.Inventory.Find(phone => phone.ProductId == productId);

                if (selectedPhone != null)
                {
                    Purchase purchase = new Purchase
                    {
                        PurchaseId = Guid.NewGuid().ToString(),
                        PurchaseDate = DateTime.Now
                    };
                    purchase.AddPhone(selectedPhone);
                    purchases.Add(purchase);
                    customer.Purchases.Add(purchase);
                    customers.Add(customer);
                    Console.WriteLine("Purchase completed.");
                }
                else
                {
                    Console.WriteLine("Phone with the given product id not found.");
                }
                break;
            case 5:
                Console.WriteLine("List of Purchases:");
                foreach (var purchase in purchases)
                {
                    Console.WriteLine($"Purchase id: {purchase.PurchaseId}, Purchase Date: {purchase.PurchaseDate}");
                    purchase.ListPurchasedPhones();
                    Console.WriteLine();
                }
                break;
            case 6:
                Console.Write("Enter the product id to search for: ");
                string productIdSearch = Console.ReadLine();
                MobilePhone foundPhone = store.Inventory.Find(phone => phone.ProductId == productIdSearch);

                if (foundPhone != null)
                {
                    Console.WriteLine("Phone Found:");
                    Console.WriteLine($"Product id: {foundPhone.ProductId}, Name: {foundPhone.Name}, Manufacturer: {foundPhone.Manufacturer}, Quantity: {foundPhone.Quantity}, Price: {foundPhone.Price:C}");
                }
                else
                {
                    Console.WriteLine("Phone with the given product id not found.");
                }
                break;
            case 7:
                exit = true;
                break;
            default:
                Console.WriteLine("Invalid choice. Please enter a number between 1 and 7.");
                break;
        }
    }
    else
    {
        Console.WriteLine("Invalid input. Please enter a valid number.");
    }
}