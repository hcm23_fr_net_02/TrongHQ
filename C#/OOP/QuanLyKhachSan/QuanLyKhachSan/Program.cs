﻿using QuanLyKhachSan;

Hotel hotel = new Hotel();
List<Booking> bookings = new List<Booking>();

Room room1 = new Room(101, "Single", 100, new List<string> { "WiFi", "TV" });
Room room2 = new Room(102, "Double", 150, new List<string> { "WiFi", "TV", "Minibar" });
Room room3 = new Room(201, "Family", 200, new List<string> { "WiFi", "TV", "Minibar" });

hotel.AddRoom(room1);
hotel.AddRoom(room2);
hotel.AddRoom(room3);

while (true)
{
    Console.WriteLine("\nHotel Management System");
    Console.WriteLine("1. Add a room");
    Console.WriteLine("2. Remove a room");
    Console.WriteLine("3. List all rooms");
    Console.WriteLine("4. Make a booking");
    Console.WriteLine("5. List bookings");
    Console.WriteLine("6. Generate Invoice");
    Console.WriteLine("7. Exit");

    string choice = Console.ReadLine();

    switch (choice)
    {
        case "1":
            // Add a room to the hotel
            Console.Write("Enter Room Number: ");
            int roomNumber = int.Parse(Console.ReadLine());
            Console.Write("Enter Room Type: ");
            string roomType = Console.ReadLine();
            Console.Write("Enter Price Per Night: ");
            double pricePerNight = double.Parse(Console.ReadLine());
            Console.Write("Enter Amenities (comma-separated): ");
            List<string> amenities = Console.ReadLine().Split(',').ToList();
            Room newRoom = new Room(roomNumber, roomType, pricePerNight, amenities);
            hotel.AddRoom(newRoom);
            Console.WriteLine("Room added successfully!");
            break;

        case "2":
            // Remove a room from the hotel
            Console.Write("Enter Room Number to remove: ");
            int roomNumberToRemove = int.Parse(Console.ReadLine());
            hotel.RemoveRoom(roomNumberToRemove);
            Console.WriteLine("Room removed successfully!");
            break;

        case "3":
            // List all rooms in the hotel
            hotel.ListRooms();
            break;

        case "4":
            // Make a booking
            Console.Write("Enter Booking ID: ");
            string bookingId = Console.ReadLine();
            Console.Write("Enter Booking Date: ");
            DateTime bookingDate = DateTime.Parse(Console.ReadLine());
            Console.Write("Enter Check-In Date: ");
            DateTime checkInDate = DateTime.Parse(Console.ReadLine());
            Console.Write("Enter Special Requests: ");
            string specialRequests = Console.ReadLine();

            Console.WriteLine("\nAvailable rooms:");
            hotel.ListRooms();

            Console.Write("Enter Room Numbers to reserve (comma-separated): ");
            List<int> roomNumbersToReserve = Console.ReadLine().Split(',').Select(int.Parse).ToList();
            List<Room> reservedRooms = new List<Room>();

            foreach (int roomNumberToReserve in roomNumbersToReserve)
            {
                Room roomToReserve = hotel.rooms.Find(r => r.RoomNumber == roomNumberToReserve && r.IsAvailable);
                if (roomToReserve != null)
                {
                    roomToReserve.IsAvailable = false;
                    reservedRooms.Add(roomToReserve);
                }
            }
            Console.Write("Enter Customer Name: ");
            string customerName = Console.ReadLine();
            Console.Write("Enter Customer Phone Number: ");
            string customerPhoneNumber = Console.ReadLine();
            Console.Write("Enter Customer Address: ");
            string customerAddress = Console.ReadLine();
            Console.Write("Enter Customer Member ID: ");
            string customerMemberId = Console.ReadLine();

            Customer customer = new Customer
            {
                Name = customerName,
                PhoneNumber = customerPhoneNumber,
                Address = customerAddress,
                MemberId = customerMemberId
            };

            Booking booking = new Booking
            {
                BookingId = bookingId,
                BookingDate = bookingDate,
                CheckInDate = checkInDate,
                ReservedRooms = reservedRooms,
                SpecialRequests = specialRequests,
                Customer = customer
            };
            bookings.Add(booking);
            Console.WriteLine("Booking created successfully!");
            break;

        case "5":
            // List all bookings
            Console.WriteLine("\nList of bookings:");
            foreach (Booking bookingInfo in bookings)
            {
                Console.WriteLine($"Booking ID: {bookingInfo.BookingId}");
                Console.WriteLine($"Booking Date: {bookingInfo.BookingDate}");
                Console.WriteLine($"Check-In Date: {bookingInfo.CheckInDate}");
                Console.WriteLine($"Customer: {bookingInfo.Customer.Name} ({bookingInfo.Customer.MemberId})");
                Console.WriteLine("Reserved Rooms:");
                foreach (Room room in bookingInfo.ReservedRooms)
                {
                    Console.WriteLine($"Room Number: {room.RoomNumber}");
                    Console.WriteLine($"Room Type: {room.RoomType}");
                    Console.WriteLine($"Price Per Night: {room.PricePerNight:C}");
                    Console.WriteLine("--------------------------------------------------");
                }
                Console.WriteLine($"Special Requests: {bookingInfo.SpecialRequests}");
            }
            break;

        case "6":
            Console.Write("Enter Booking ID to generate invoice: ");
            string bookingIdToGenerateInvoice = Console.ReadLine();
            Booking bookingToGenerateInvoice = bookings.Find(b => b.BookingId == bookingIdToGenerateInvoice);
            if (bookingToGenerateInvoice != null)
            {
                Invoice invoice = new Invoice
                {
                    InvoiceId = Guid.NewGuid().ToString(),
                    TotalAmount = bookingToGenerateInvoice.CalculateTotalCost(),
                    ServicesUsed = new List<string> { "Room" }
                };
                Console.WriteLine("\nInvoice Details:");
                Console.WriteLine($"Invoice ID: {invoice.InvoiceId}");
                Console.WriteLine($"Total Amount: {invoice.TotalAmount:C}");
                Console.WriteLine($"Services Used: {string.Join(", ", invoice.ServicesUsed)}");
            }
            else
            {
                Console.WriteLine("Booking not found.");
            }
            break;

        case "7":
            // Exit the program
            return;

        default:
            Console.WriteLine("Invalid choice. Please try again.");
            break;
    }
}