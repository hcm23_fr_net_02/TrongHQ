﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyKhachSan
{
    public class Booking
    {
        public string BookingId { get; set; }
        public DateTime BookingDate { get; set; }
        public DateTime CheckInDate { get; set; }
        public List<Room> ReservedRooms { get; set; }
        public string SpecialRequests { get; set; }
        public Customer Customer { get; set; }

        public double CalculateTotalCost()
        {
            int totalNights = CalculateTotalNights();
            double totalCost = 0;

            foreach (var room in ReservedRooms)
            {
                totalCost += room.PricePerNight * totalNights;
            }

            return totalCost;
        }
        private int CalculateTotalNights()
        {
            // Calculate the number of nights booked based on the check-in and booking dates
            int totalNights = (CheckInDate - BookingDate).Days;
            return totalNights;
        }
    }
}
