﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyKhachSan
{
    internal class Hotel
    {
        public List<Room> rooms { get; set; }
        public Hotel() { 
            rooms = new List<Room>();
        }

        public void AddRoom(Room room)
        {
            rooms.Add(room);
        }

        public void RemoveRoom(int roomNumber)
        {
            Room roomToRemove = rooms.Find(r => r.RoomNumber == roomNumber);
            if (roomToRemove != null)
            {
                rooms.Remove(roomToRemove);
            }
        }

        public void ListRooms()
        {
            Console.WriteLine("\nList of rooms in the hotel:");
            foreach (var room in rooms)
            {
                Console.WriteLine($"Room Number: {room.RoomNumber}");
                Console.WriteLine($"Room Type: {room.RoomType}");
                Console.WriteLine($"Price Per Night: {room.PricePerNight:C}");
                Console.WriteLine($"Amenities: {string.Join(", ", room.Amenities)}");
                Console.WriteLine($"Is Available: {room.IsAvailable}");
                Console.WriteLine("--------------------------------------------------");
            }
        }
    }
}
