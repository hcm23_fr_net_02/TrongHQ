﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyThuVien
{
    public class Library
    {
        public List<Book> books { get; set; }
        public Library() { 
            books = new List<Book>();
        }

        public void AddBook(Book book)
        {
            books.Add(book);
        }

        public void RemoveBook(string bookId)
        {
            Book bookToRemove = books.Find(b => b.BookId == bookId);
            if (bookToRemove != null)
            {
                books.Remove(bookToRemove);
            }
        }

        public void ListBooks()
        {
            foreach (var book in books)
            {
                Console.WriteLine($"Title: {book.Title}, Author: {book.Author}, Stock: {book.Stock}, Price: {book.Price}, Book ID: {book.BookId}");
            }
        }
    }
}
