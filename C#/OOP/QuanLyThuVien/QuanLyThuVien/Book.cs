﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyThuVien
{
    public class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public int Stock { get; set; }
        public double Price { get; set; }
        public string BookId { get; set; }
    }
}
