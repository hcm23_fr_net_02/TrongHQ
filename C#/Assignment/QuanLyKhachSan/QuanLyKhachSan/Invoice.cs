﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyKhachSan
{
    public class Invoice
    {
        public string InvoiceId { get; set; }
        public double TotalAmount { get; set; }
        public List<string> ServicesUsed { get; set; }
    }
}
