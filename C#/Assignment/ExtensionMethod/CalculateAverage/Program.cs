﻿// See https://aka.ms/new-console-template for more information

List<int> numbers = new List<int> { 1, 2, 3, 4, 5,6,7,4,5,3,4,5 };
double average = numbers.GetAverage(); // Kết quả: 3.0
Console.WriteLine(average);

public static class Average
{
    public static double GetAverage(this List<int> num)
    {
        int sum = 0;
        foreach (int i in num)
        {
            sum += i;
        }
        return (double)sum / num.Count;
    }
}
