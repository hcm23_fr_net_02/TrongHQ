﻿string text = "Hello, world!";
string reversedText = text.DaoChuoi();

Console.WriteLine("Dao chuoi: " + reversedText);

public static class ReverseString
{
    public static string DaoChuoi(this string str)
    {
        string temp = "";
        for (int i = str.Length; i > 0; i--)
        {
            temp = temp + str.Substring(i - 1, 1);
        }
        return temp;
    }
}
