﻿// See https://aka.ms/new-console-template for more information

using System.Text;

string input = "Hello World";
string result = input.RemoveDuplicate();
Console.WriteLine(result);



public static class Duplicate
{
    public static string RemoveDuplicate(this string input)
    {
        StringBuilder strBulder = new StringBuilder();
        for (int i = 0; i < input.Length; i++)
        {
            if (!strBulder.ToString().Contains(input[i]))
            {
                strBulder.Append(input[i]);
            }
        }
        return strBulder.ToString();
    }
}

