﻿using QuanLyThuVien;

Library library = new Library();
List<Loan> loans = new List<Loan>();

while (true)
{
    Console.WriteLine("\nLibrary Management System");
    Console.WriteLine("1. Add a book");
    Console.WriteLine("2. Remove a book");
    Console.WriteLine("3. List all books");
    Console.WriteLine("4. Loan a book");
    Console.WriteLine("5. List loans");
    Console.WriteLine("6. Search by book ID");
    Console.WriteLine("7. Exit");

    string choice = Console.ReadLine();

    switch (choice)
    {
        case "1":
            Console.Write("Enter book title: ");
            string title = Console.ReadLine();
            Console.Write("Enter book author: ");
            string author = Console.ReadLine();
            Console.Write("Enter stock quantity: ");
            int stock = int.Parse(Console.ReadLine());
            Console.Write("Enter book price: ");
            double price = double.Parse(Console.ReadLine());
            Console.Write("Enter book ID: ");
            string bookId = Console.ReadLine();
            Book newBook = new Book
            {
                Title = title,
                Author = author,
                Stock = stock,
                Price = price,
                BookId = bookId
            };
            library.AddBook(newBook);
            Console.WriteLine("Book added successfully!");
            break;

        case "2":
            Console.Write("Enter book ID to remove: ");
            string bookIdToRemove = Console.ReadLine();
            library.RemoveBook(bookIdToRemove);
            Console.WriteLine("Book removed successfully!");
            break;

        case "3":
            Console.WriteLine("\nList of books in the library:");
            library.ListBooks();
            break;

        case "4":
            Console.Write("Enter loan ID: ");
            string loanId = Console.ReadLine();
            Console.Write("Enter loan date: ");
            DateTime loanDate = DateTime.Parse(Console.ReadLine());
            Console.Write("Enter due date: ");
            DateTime dueDate = DateTime.Parse(Console.ReadLine());

            Console.WriteLine("\nAvailable books:");
            library.ListBooks();

            Console.Write("Enter book IDs to loan (comma-separated): ");
            string[] bookIds = Console.ReadLine().Split(',');
            List<Book> booksToLoan = new List<Book>();

            foreach (string id in bookIds)
            {
                Book bookToLoan = library.books.Find(b => b.BookId == id && b.Stock > 0);
                if (bookToLoan != null)
                {
                    booksToLoan.Add(bookToLoan);
                    bookToLoan.Stock--;
                }
            }
            Console.Write("Enter borrower's name: ");
            string borrowerName = Console.ReadLine();
            Console.Write("Enter borrower's address: ");
            string borrowerAddress = Console.ReadLine();
            Console.Write("Enter borrower's member ID: ");
            string borrowerMemberId = Console.ReadLine();

            Customer borrower = new Customer
            {
                Name = borrowerName,
                Address = borrowerAddress,
                MemberId = borrowerMemberId
            };



            Loan loan = new Loan
            {
                LoanId = loanId,
                LoanDate = loanDate,
                DueDate = dueDate,
                Books = booksToLoan,
                Borrower = borrower
            };
            loans.Add(loan);
            Console.WriteLine("Loan created successfully!");
            break;

        case "5":
            Console.WriteLine("\nList of loans:");
            foreach (Loan loanInfo in loans)
            {
                Console.WriteLine($"Loan ID: {loanInfo.LoanId}, Loan Date: {loanInfo.LoanDate}, Due Date: {loanInfo.DueDate}, Borrower : {loanInfo.Borrower.Name} (ID: {loanInfo.Borrower.MemberId}") );
                foreach (Book book in loanInfo.Books)
                {
                    Console.WriteLine($"Title: {book.Title}, Author: {book.Author}, Book ID: {book.BookId}" );
                }
            }
            break;

        case "6":
            Console.Write("Enter book ID to search for: ");
            string searchBookId = Console.ReadLine();
            Book foundBook = library.books.Find(b => b.BookId == searchBookId);
            if (foundBook != null)
            {
                Console.WriteLine($"Title: {foundBook.Title}, Author: {foundBook.Author}, Stock: {foundBook.Stock}, Price: {foundBook.Price}, Book ID: {foundBook.BookId}");
            }
            else
            {
                Console.WriteLine("Book not found.");
            }
            break;

        case "7":
            return;

        default:
            Console.WriteLine("Invalid choice. Please try again.");
            break;
    }
}