using Microsoft.VisualStudio.TestTools.UnitTesting;
using FR_TrongHQ_Training;

namespace TestCalculator
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Calculator c   = new Calculator();
            var a = 2;
            var b = 3;
            var expectedAdd = 5;

            var param = 2;
            var ExpectedDate = "Tuesday";

            var actual = c.Add(a, b);
            var actualDate = c.GetDayOfWeek(param);

            Assert.AreEqual(expectedAdd, actual);
            Assert.AreEqual(ExpectedDate, actualDate);
        }
    }
}