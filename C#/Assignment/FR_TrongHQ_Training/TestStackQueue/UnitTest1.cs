using Xunit;
using System;
using FR_TrongHQ_Training;
namespace TestStackQueue
{
    public class UnitTest1
    {
        [Fact]
        public void Push_Pop_Peek_Clear_Test()
        {
            // Arrange
            var stack = new LStack<int>(5);

            // Act
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);

            // Assert
            Assert.Equal(3, stack.Peek());
            Assert.Equal(2, stack.Pop());
            Assert.Equal(1, stack.Pop());
            Assert.Throws<ArgumentException>(() => stack.Pop());
            Assert.True(stack.IsEmpty());
        }
        [Fact]
        public void Enqueue_Dequeue_Peek_Clear_Test()
        {
            // Arrange
            var queue = new LQueue<int>(5);

            // Act
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);

            // Assert
            Assert.Equal(1, queue.Peek());
            Assert.Equal(2, queue.Dequeue());
            Assert.Equal(3, queue.Dequeue());
            Assert.Throws<ArgumentException>(() => queue.Dequeue());
            Assert.True(queue.IsEmpty());
        }
    }
}