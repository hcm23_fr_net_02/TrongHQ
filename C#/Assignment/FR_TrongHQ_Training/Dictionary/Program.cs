﻿// See https://aka.ms/new-console-template for more information

Dictionary<string, string> dict = new Dictionary<string, string>();
int choice = 0;
do
{
    Console.WriteLine("Choose your option:");
    Console.WriteLine("1. Add new word ");
    Console.WriteLine("2. Search for a word");
    Console.WriteLine("3. Remove a word");
    Console.WriteLine("4. Show all words");
    Console.WriteLine("5. Exit");
    switch (int.Parse(Console.ReadLine()))
    {
        case 1:
            Console.Write("English word: ");
            string englishWord = Console.ReadLine();
            Console.Write("Vietnamese word: ");
            string vietnameseWord = Console.ReadLine();
            dict.Add(englishWord, vietnameseWord);
            Console.WriteLine("Added word succesfully!");
            break;
        case 2:
            Console.Write("Please input your word: ");
            string word = Console.ReadLine();
            if (dict.TryGetValue(word, out string value))
            {
                Console.WriteLine("The translate of your word is: " + value);
            }
            else if (dict.ContainsValue(word))
            {
                string key = dict.FirstOrDefault(x => x.Value == word).Key;
                Console.WriteLine("The translate of your word is: " + key);
            }
            else
            {
                Console.WriteLine("Word not found!");
            }
            break;
        case 3:
            Console.Write("Please input your word: ");
            string wordRemove = Console.ReadLine();
            if (dict.TryGetValue(wordRemove, out string valueRemove))
            {
                dict.Remove(wordRemove);
                Console.WriteLine("The translate of your word is: " + valueRemove);
            }
            else if (dict.ContainsValue(wordRemove))
            {
                string key = dict.FirstOrDefault(x => x.Value == wordRemove).Key;
                dict.Remove(key);
                Console.WriteLine("Removed key successfully ");
            }
            else
            {
                Console.WriteLine("Word not found!");
            }
            break;
        case 4:
            Console.WriteLine("List in dictionary: ");
            dict.Select(i => $"{i.Key}: {i.Value}").ToList().ForEach(Console.WriteLine);
            break;
        case 5:
            break;
        default:
            Console.WriteLine("Invalid choice! please choose again");
            break;
    }
} while (choice != 5);

