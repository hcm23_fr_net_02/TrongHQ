﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");



Dictionary<string, double> studentScores = new Dictionary<string, double>();

bool shouldExit = false;

while (!shouldExit)
{
    Console.WriteLine("===== Student Score Management =====");
    Console.WriteLine("1. Add a Student and Score");
    Console.WriteLine("2. Search for a Student's Score");
    Console.WriteLine("3. Display the List of Students and Scores");
    Console.WriteLine("4. Exit the Program");
    Console.Write("Choose an option (1-4): ");

    int choice;
    if (int.TryParse(Console.ReadLine(), out choice))
    {
        switch (choice)
        {
            case 1:
                AddStudentScore(studentScores);
                break;
            case 2:
                SearchStudentScore(studentScores);
                break;
            case 3:
                DisplayStudentScores(studentScores);
                break;
            case 4:
                shouldExit = true;
                break;
            default:
                Console.WriteLine("Invalid choice. Please choose from 1 to 4.");
                break;
        }
    }
    else
    {
        Console.WriteLine("Invalid choice. Please choose from 1 to 4.");
    }
}

    static void AddStudentScore(Dictionary<string, double> scores)
    {
        Console.Write("Enter the student's name: ");
        string name = Console.ReadLine();

        Console.Write("Enter the student's score: ");
        if (double.TryParse(Console.ReadLine(), out double score))
        {
            if (scores.ContainsKey(name))
            {
                Console.WriteLine("This student already exists in the list.");
            }
            else
            {
                scores.Add(name, score);
                Console.WriteLine("The student's score has been added to the list.");
            }
        }
        else
        {
            Console.WriteLine("Invalid score.");
        }
    }

    static void SearchStudentScore(Dictionary<string, double> scores)
    {
        Console.Write("Enter the student's name to search for the score: ");
        string name = Console.ReadLine();

        if (scores.ContainsKey(name))
        {
            double score = scores[name];
            Console.WriteLine($"Score of student {name}: {score}");
        }
        else
        {
            Console.WriteLine("Student with this name was not found in the list.");
        }
    }

    static void DisplayStudentScores(Dictionary<string, double> scores)
    {
        Console.WriteLine("===== List of Students and Scores =====");
        foreach (var kvp in scores)
        {
            Console.WriteLine($"{kvp.Key}: {kvp.Value}");
        }
        Console.WriteLine("=======================================");
    }