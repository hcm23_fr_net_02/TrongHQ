﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR_TrongHQ_Training
{
    public class LQueue<T>
    {
        private T[] array;
        private int rear;
        private int capacity;

        public LQueue(int capacity)
        {
            if (capacity <= 0)
            {
                throw new ArgumentException("Capacity must be greater than zero.");
            }

            this.capacity = capacity;
            array = new T[capacity];
            rear = -1;
        }

        public void Enqueue(T item)
        {
            if (rear + 1 == capacity)
            {
                throw new ArgumentException("Queue is full");
            }

            rear = rear + 1;
            array[rear] = item;
        }

        public T Dequeue()
        {
            if (IsEmpty())
            {
                throw new ArgumentException("Queue is empty");
            }
            
            rear--;
            if (rear == -1)
            {
                throw new ArgumentException("Queue is empty");
            }
            for (int i = 0; i <= rear; i++)
            {
                array[i] = array[i + 1];
            }

            return array[0];
        }

        public T Peek()
        {
            if (IsEmpty())
            {
                throw new ArgumentException("Queue is full");
            }

            return array[0];
        }

        public bool IsEmpty()
        {
            return rear + 1 == 0;
        }

        public int Count
        {
            get { return rear + 1; }
        }

        public void Clear()
        {
            rear = -1;
        }
    }
}
