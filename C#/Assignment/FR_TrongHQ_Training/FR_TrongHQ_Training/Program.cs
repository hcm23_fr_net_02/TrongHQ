﻿// See https://aka.ms/new-console-template for more information
using FR_TrongHQ_Training;

Console.WriteLine("Hello, World!");
try
{
    //---------------Stack------------------
    int capacity = 0;
    LStack<int> stack = null;
    bool isStacking = true;

    while (isStacking)
    {
        
        Console.WriteLine("---------------Stack Menu----------------");
        Console.WriteLine("1. Set Stack Capacity");
        Console.WriteLine("2. Push");
        Console.WriteLine("3. Pop");
        Console.WriteLine("4. Peek");
        Console.WriteLine("5. Clear");
        Console.WriteLine("6. Exit");
        Console.Write("Enter your choice: ");

        if (int.TryParse(Console.ReadLine(), out int choice))
        {
            switch (choice)
            {
                case 1:
                    Console.Write("Enter stack capacity: ");
                    if (int.TryParse(Console.ReadLine(), out int newCapacity) && newCapacity > 0)
                    {
                        capacity = newCapacity;
                        stack = new LStack<int>(capacity);
                        Console.WriteLine($"Stack capacity set to {capacity}.");
                    }
                    else
                    {
                        Console.WriteLine("Invalid capacity. Capacity must be greater than 0.");
                    }
                    break;

                case 2:
                    if (stack == null)
                    {
                        Console.WriteLine("Please set the stack capacity first.");
                    }
                    else
                    {
                        Console.Write("Enter an item to push: ");
                        if (int.TryParse(Console.ReadLine(), out int item))
                        {
                            stack.Push(item);
                            Console.WriteLine($"Pushed {item} onto the stack.");
                        }
                        else
                        {
                            Console.WriteLine("Invalid input. Please enter an integer.");
                        }
                    }
                    break;

                case 3:
                    if (stack == null)
                    {
                        Console.WriteLine("Please set the stack capacity first.");
                    }
                    else
                    {
                        try
                        {
                            int poppedItem = stack.Pop();
                            Console.WriteLine($"Popped item: {poppedItem}");
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                    break;

                case 4:
                    if (stack == null)
                    {
                        Console.WriteLine("Please set the stack capacity first.");
                    }
                    else
                    {
                        try
                        {
                            int peekedItem = stack.Peek();
                            Console.WriteLine($"Top item: {peekedItem}");
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                    break;

                case 5:
                    if (stack == null)
                    {
                        Console.WriteLine("Please set the stack capacity first.");
                    }
                    else
                    {
                        stack.Clear();
                        Console.WriteLine("Stack cleared.");
                    }
                    break;

                case 6:
                    isStacking = false;
                    break;

                default:
                    Console.WriteLine("Invalid choice. Please select a valid option.");
                    break;
            }
        }
        else
        {
            Console.WriteLine("Invalid input. Please enter a number.");
        }
    }

    //---------------Queue------------------

    int capacityQueue = 0;
    LQueue<int> queue = null;
    bool isQueueing = true;

    while (isQueueing)
    {
        Console.WriteLine("---------------Queue Menu----------------");
        Console.WriteLine("1. Set Queue Capacity");
        Console.WriteLine("2. Enqueue");
        Console.WriteLine("3. Dequeue");
        Console.WriteLine("4. Peek");
        Console.WriteLine("5. Clear");
        Console.WriteLine("6. Exit");
        Console.Write("Enter your choice: ");

        if (int.TryParse(Console.ReadLine(), out int choice))
        {
            switch (choice)
            {
                case 1:
                    Console.Write("Enter queue capacity: ");
                    if (int.TryParse(Console.ReadLine(), out int newCapacity) && newCapacity > 0)
                    {
                        capacity = newCapacity;
                        queue = new LQueue<int>(capacity);
                        Console.WriteLine($"Queue capacity set to {capacity}.");
                    }
                    else
                    {
                        Console.WriteLine("Invalid capacity. Capacity must be greater than 0.");
                    }
                    break;

                case 2:
                    if (queue == null)
                    {
                        Console.WriteLine("Please set the queue capacity first.");
                    }
                    else
                    {
                        Console.Write("Enter an item to enqueue: ");
                        if (int.TryParse(Console.ReadLine(), out int item))
                        {
                            queue.Enqueue(item);
                            Console.WriteLine($"Enqueued {item} into the queue.");
                        }
                        else
                        {
                            Console.WriteLine("Invalid input. Please enter an integer.");
                        }
                    }
                    break;

                case 3:
                    if (queue == null)
                    {
                        Console.WriteLine("Please set the queue capacity first.");
                    }
                    else
                    {
                        try
                        {
                            int dequeuedItem = queue.Dequeue();
                            Console.WriteLine($"Dequeued item: {dequeuedItem}");
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                    break;

                case 4:
                    if (queue == null)
                    {
                        Console.WriteLine("Please set the queue capacity first.");
                    }
                    else
                    {
                        try
                        {
                            int peekedItem = queue.Peek();
                            Console.WriteLine($"Front item: {peekedItem}");
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                    break;

                case 5:
                    if (queue == null)
                    {
                        Console.WriteLine("Please set the queue capacity first.");
                    }
                    else
                    {
                        queue.Clear();
                        Console.WriteLine("Queue cleared.");
                    }
                    break;

                case 6:
                    isQueueing = false;
                    break;

                default:
                    Console.WriteLine("Invalid choice. Please select a valid option.");
                    break;
            }
        }
        else
        {
            Console.WriteLine("Invalid input. Please enter a number.");
        }
    }
}
catch (Exception ex) 
{ 
    Console.WriteLine(ex.Message);
}


//---------------Struc,Enum,String------------------
Console.WriteLine("Struct,Enum,String:");
StructEnumString.Test();



//---------------DateTime---------------------
// Creating DateTime instances
DateTime now = DateTime.Now; // Current date and time
DateTime today = DateTime.Today; // Current date (time set to midnight)

// Displaying current date and time
Console.WriteLine("Current Date and Time: " + now);

// Displaying current date without time
Console.WriteLine("Current Date (Time Set to Midnight): " + today);

// Creating DateTime instances with specific dates and times
DateTime customDateTime = new DateTime(2023, 10, 4, 14, 30, 0); // October 4, 2023, 2:30 PM
DateTime utcDateTime = DateTime.UtcNow; // Current UTC date and time

// Displaying custom date and time
Console.WriteLine("Custom Date and Time: " + customDateTime);

// Displaying current UTC date and time
Console.WriteLine("Current UTC Date and Time: " + utcDateTime);

// Formatting DateTime to a custom string
string formattedDateTime = now.ToString("yyyy-MM-dd HH:mm:ss");
Console.WriteLine("Formatted Date and Time: " + formattedDateTime);

// Performing DateTime operations
DateTime futureDateTime = now.AddDays(7); // Adding 7 days to the current date
TimeSpan timeDifference = futureDateTime - now; // Calculating time difference
Console.WriteLine("Future Date: " + futureDateTime);
Console.WriteLine("Time Difference (Days): " + timeDifference.TotalDays);

// Parsing a string to DateTime
string dateStr = "2023-12-25 09:00:00";
if (DateTime.TryParseExact(dateStr, "yyyy-MM-dd HH:mm:ss", null, System.Globalization.DateTimeStyles.None, out DateTime parsedDateTime))
{
    Console.WriteLine("Parsed Date and Time: " + parsedDateTime);
}
else
{
    Console.WriteLine("Failed to parse the date.");
}


//------------Generic------------
var o = new A<string, int>();
o.a = "1000";
o.b = 1;
Console.WriteLine(o.a);
public class A<T, K>
{
    public T a { get; set; }
    public K b { get; set; }
}