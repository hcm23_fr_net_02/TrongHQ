﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR_TrongHQ_Training
{
    public class LStack<T>
    {
        private T[] array;
        private int top;

        public LStack(int capacity)
        {
            if (capacity <= 0)
            {
                throw new ArgumentException("Capacity must be greater than zero.");
            }

            array = new T[capacity];
            top = -1;
        }

        public void Push(T item)
        {
            if (top == array.Length - 1)
            {
                throw new ArgumentException("Stack is over sized!");
            }

            array[top + 1] = item;
            top++;
        }

        public T? Pop()
        {
            if (IsEmpty())
            {
                throw new ArgumentException("Stack is empty!");
            }
            top--;
            if(top == -1)
                throw new ArgumentException("Stack is empty!");

            return array[top];
        }

        public T? Peek()
        {
            if (IsEmpty())
            {
                throw new ArgumentException("Stack is empty!");
            }

            return array[top];
        }

        public bool IsEmpty()
        {
            return top == -1;
        }

        public int Count
        {
            get { return top + 1; }
        }

        public void Clear()
        {
            top = -1;
        }
    }
}
