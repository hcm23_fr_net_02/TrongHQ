﻿namespace FR_TrongHQ_Training
{
    public enum Color
    {
        Red,
        Green,
        Blue
    }

    public struct Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
    static public class StructEnumString
    {
        public static void Test()
        {
            // Create an instance of the Color enum
            Color color = Color.Red;

            // Use the Color enum in a switch statement
            switch (color)
            {
                case Color.Red:
                    Console.WriteLine("The color is Red.");
                    break;
                case Color.Green:
                    Console.WriteLine("The color is Green.");
                    break;
                case Color.Blue:
                    Console.WriteLine("The color is Blue.");
                    break;
                default:
                    Console.WriteLine("Unknown color.");
                    break;
            }

            // Create an instance of the Point struct
            Point point = new Point(10, 20);

            // Access and display the Point's X and Y coordinates
            Console.WriteLine($"X: {point.X}, Y: {point.Y}");

            // Modify the Point's coordinates
            point.X = 30;
            point.Y = 40;

            // Display the modified coordinates
            Console.WriteLine($"Modified X: {point.X}, Modified Y: {point.Y}");
        }
    }

}
