﻿// See https://aka.ms/new-console-template for more information
using Newtonsoft.Json;
using System;

Console.WriteLine("Hello, World!");

var todoJson = File.ReadAllText("Todo_list.json");
List<Todo>? todoList = JsonConvert.DeserializeObject<List<Todo>>(todoJson);

bool shouldExit = false;

while (!shouldExit)
{
    Console.WriteLine("===== Todo List Management =====");
    Console.WriteLine("1. Display Todo List");
    Console.WriteLine("2. Add a New Todo");
    Console.WriteLine("3. Delete a Todo");
    Console.WriteLine("4. Mark a Todo as Completed");
    Console.WriteLine("5. Exit the Application");
    Console.Write("Choose an option (1-5): ");

    int choice;
    if (int.TryParse(Console.ReadLine(), out choice))
    {
        switch (choice)
        {
            case 1:
                DisplayTodoList(todoList);
                break;
            case 2:
                AddTodo(ref todoList);
                break;
            case 3:
                RemoveTodo(ref todoList);
                break;
            case 4:
                MarkTodoAsCompleted(ref todoList);
                break;
            case 5:
                shouldExit = true;
                break;
            default:
                Console.WriteLine("Invalid option");
                break;
        }
    }
    else
    {
        Console.WriteLine("Invalid option");
    }
}

static void DisplayTodoList(List<Todo> todoList)
{
    Console.WriteLine("===== List of Todo =====");
    foreach (var todo in todoList)
    {
        Console.WriteLine($"- {todo.Description} (Completed: {todo.completed})");
    }
    Console.WriteLine("===============================");
}

static void AddTodo(ref List<Todo> todoList)
{
    Console.Write("Input new Task: ");
    string taskName = Console.ReadLine();
    Console.Write("Input new Description: ");
    string description = Console.ReadLine();

    Todo newTodo = new Todo
    {
        Description = description,
        completed = false,
        task_name = taskName
    };

    todoList.Add(newTodo);
    Console.WriteLine("New todo has been Added.");
}

static void RemoveTodo(ref List<Todo> todoList)
{
    Console.Write("Input task name that you want to remove: ");
    string taskName = Console.ReadLine();


    Todo todoToRemove = todoList.Find(t => t.task_name == taskName);
    if (todoToRemove != null)
    {
        todoList.Remove(todoToRemove);
        Console.WriteLine("Todo has been removed.");
    }
    else
    {
        Console.WriteLine("Cannot find the todo.");
    }
}

static void MarkTodoAsCompleted(ref List<Todo> todoList)
{
    Console.Write("Input task name that you want to marked as completed: ");
    string taskName = Console.ReadLine();


    Todo todoToMark = todoList.Find(t => t.task_name == taskName);
    if (todoToMark != null)
    {
        todoToMark.completed = true;
        Console.WriteLine("Todo is marked as completed.");
    }
    else
    {
        Console.WriteLine("Cannot find the todo.");
    }
}

public class Todo {
    public string Description { get; set; }
    public bool completed { get; set; }
    public string task_name { get; set; }

}