﻿// See https://aka.ms/new-console-template for more information
//string a = "hello";
//string b = a.S();
//Console.WriteLine(b);

//string input = "hello";
//Console.WriteLine(input.CustomUpper()); // Output: Hello
//Console.WriteLine(input.CustomUpper(1)); // Output: hEllo
//Console.WriteLine(input.CustomUpper(3).CustomUpper(1)); // Output: HEllo

string text = "Hello, world!";
string reversedText = text.DaoChuoi();

Console.WriteLine("Dao chuoi: " + reversedText);


public static class ReverseString
{
    public static string DaoChuoi(this string str)
    {
        string temp = "";
        for (int i = str.Length; i > 0; i--)
        {
            temp = temp + str.Substring(i - 1,1);
        }
        return temp;
    }
}