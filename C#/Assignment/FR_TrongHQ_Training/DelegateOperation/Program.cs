﻿// See https://aka.ms/new-console-template for more information


MathOperation<int> mathOp = Addition;
//Add
int num1 = 10;
int num2 = 5;
int result = PerformOperation(num1, num2, mathOp);
Console.WriteLine($"Result of addition: {result}");

//Subtract
mathOp = Subtraction;
result = PerformOperation(num1, num2, mathOp);
Console.WriteLine($"Result of subtraction: {result}");

//Multiply
mathOp = Multiplication;
result = PerformOperation(num1, num2, mathOp);
Console.WriteLine($"Result of multiplication: {result}");

//Divide
mathOp = Division;
result = PerformOperation(num1, num2, mathOp);
Console.WriteLine($"Result of division: {result}");

// Func

double doubleNum1 = 10.5, doubleNum2 = 3.5;
string str1 = "Hello, ", str2 = "World!";

//  Func<int, int, int> 
Func<int, int, int> addIntegers = (a, b) => a + b;
MathOperation<int> addIntegersOp = ConvertToMathOperation(addIntegers);
int intResult = PerformOperation(num1, num2, addIntegersOp);
Console.WriteLine($"Addition of integers: {intResult}");

// Func<double, double, double> 
Func<double, double, double> multiplyDoubles = (a, b) => a * b;
MathOperation<double> multiplyDoubleOp = ConvertToMathOperation(multiplyDoubles);
double doubleResult = PerformOperation(doubleNum1, doubleNum2, multiplyDoubleOp);
Console.WriteLine($"Multiplication of doubles: {doubleResult}");

// Func<string, string, string> 
Func<string, string, string> concatenateStrings = (a, b) => a + b;
MathOperation<string> concatenateStrOp = ConvertToMathOperation(concatenateStrings);
string stringResult = PerformOperation(str1, str2, concatenateStrOp);
Console.WriteLine($"Concatenation of strings: {stringResult}");


int Addition(int a, int b)
{
    return a + b;
}

int Subtraction(int a, int b)
{
    return a - b;
}

int Multiplication(int a, int b)
{
    return a * b;
}

int Division(int a, int b)
{
    if (b != 0)
    {
        return a / b;
    }
    else
    {
        Console.WriteLine("Error: Division by zero.");
        return 0;
    }
}

T PerformOperation<T>(T a, T b, MathOperation<T> operation)
{
    return operation(a, b);
}

MathOperation<T> ConvertToMathOperation<T>(Func<T, T, T> func)
{
    return (a, b) => func(a, b);
}

public delegate T MathOperation<T>(T a, T b);

