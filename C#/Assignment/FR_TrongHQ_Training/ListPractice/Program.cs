﻿// See https://aka.ms/new-console-template for more information


List<Student> students = new List<Student>();
StudentManager studentManager = new StudentManager();
int choice;
bool isChoosing = true;
int ID = 1;

do
{
    Menu.DisplayMenu();


    try
    {
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                string name;
                int Age;
                double Gpa;

                try
                {
                    Console.Write("Input Name:");
                    name = Console.ReadLine();
                    Console.Write("Input Age:");
                    Age = int.Parse(Console.ReadLine());
                    Console.Write("Input GPA:");
                    Gpa = double.Parse(Console.ReadLine());
                    studentManager.AddStudent(new Student(ID,name,Age,Gpa));
                    ID++;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Data not valid");
                }
                break;
            case 2:
                Console.Write("Please input the name you want to find: ");
                string nameFind = Console.ReadLine();
                Student student = studentManager.FindStudent(nameFind);
                if (student != null)
                {
                    Console.WriteLine("Student founded: " + student.ToString());
                }
                else
                {
                    Console.WriteLine("Student not found!");
                }
                break;
            case 3:
                Console.Write("Please input the name you want to remove: ");
                string nameFindRemove = Console.ReadLine();
                studentManager.RemoveStudent(nameFindRemove);
                break;
            case 4:
                studentManager.ShowListStudents();
                break;

            default:
                Console.WriteLine();
                break;
        }


    }
    catch (Exception)
    {

        throw;
    }

} while (choice != 5);
public class StudentManager
{
    public List<Student> students = new List<Student>();
    public void ShowListStudents()
    {
        if (students.Count > 0)
        {
            Console.WriteLine("List of students:");
            //students.ForEach(student => Console.WriteLine(student));
            foreach (var item in students)
            {
                Console.WriteLine($"Name: {item.Name} | GPA: {item.gpa} | Age: {item.Age}");
            }
        }
        else
        {
            Console.WriteLine("No students available");
        }
        
    }
    public void AddStudent(Student student)
    {
        students.Add(student);
        Console.WriteLine("Add student successfully");
    }

    public void RemoveStudent(string name)
    {
        var student = FindStudent(name);
        students.Remove(student);
    }
    public Student? FindStudent(string name)
    {
        var student = students.Find(student => student.Name == name);
        if (student == null)
            throw new Exception("Student not found");
        return student;
    }



}
public class Student
{
    public Student(int Id, string Name, int Age, double gpa)
    {
        this.Id = Id;
        this.Name = Name;
        this.Age = Age;
        this.gpa = gpa;
    }
    public int Id { get; set; }
    public string Name { get; set; }
    public int Age { get; set; }
    public double gpa { get; set; }

    //public override string ToString()
    //{
    //    return Id + " | " + Name + " | " + Age + " | " + gpa;
    //}
}

public class Menu
{
    public static void DisplayMenu()
    {
        Console.WriteLine("Choose your option:");
        Console.WriteLine("1. Add new student");
        Console.WriteLine("2. Find a student by name");
        Console.WriteLine("3. Remove a student by name");
        Console.WriteLine("4. Show list of students");
        Console.WriteLine("5. Exit");
    }
}


