﻿// See https://aka.ms/new-console-template for more information
// đọc file mock-data.json trong project để lấy dữ liệu
using Newtonsoft.Json;

string jsonPath = AppDomain.CurrentDomain.BaseDirectory.Split("bin").First() + "MOCK_DATA.json";
string jsonData = File.ReadAllText(jsonPath);
List<Person> persons = JsonConvert.DeserializeObject<List<Person>>(jsonData);
Console.WriteLine(persons.Count);
int numOfValue = 10;
int currentPage = 0;
var personPage = persons.Skip(currentPage * numOfValue).Take(numOfValue).ToList();
personPage.ForEach(x => Console.WriteLine(x.Id + "||" + x.Name + "||" + x.Price + "||" + x.Category));

// Create Menu
bool isMenu = true;
while (isMenu)
{
    Console.WriteLine("===========================Menu=============================");
    Console.WriteLine($"|1. Chon so phan tu trong 1 trang({numOfValue} trang)                |");
    Console.WriteLine("|2. trang hien tai                                         |");
    Console.WriteLine("|3. trang tiep                                             |");
    Console.WriteLine("|4. trang truoc                                            |");
    Console.WriteLine("|5. Ket thuc chuong trinh                                  |");
    Console.WriteLine("============================================================");
    int choice;
    if (int.TryParse(Console.ReadLine(), out choice))
    {
        switch (choice)
        {
            case 1:
                Console.Write("So luong phan tu: ");
                numOfValue = int.Parse(Console.ReadLine());
                currentPage = 0;
                Console.WriteLine("Thay doi thanh cong reset ve trang dau ! \n \n");
                break;
            case 2:
                personPage = persons.Skip(currentPage * numOfValue).Take(numOfValue).ToList();
                personPage.ForEach(x => Console.WriteLine(x.Id + "||" + x.Name + "||" + x.Price + "||" + x.Category));
                break;
            case 3:
                currentPage = currentPage + 1;
                personPage = persons.Skip(currentPage * numOfValue).Take(numOfValue).ToList();

                if (personPage.Count() != 0)
                {
                    personPage.ForEach(x => Console.WriteLine(x.Id + "||" + x.Name + "||" + x.Price + "||" + x.Category));
                }
                else
                {
                    Console.WriteLine("Day la trang cuoi cung khong the hien thi tiep");
                    currentPage = currentPage - 1;
                }
                break;
            case 4:
                if (currentPage == 0)
                {
                    Console.WriteLine("Day la trang dau tien khong the ve trang truoc!");
                    break;
                }
                currentPage = currentPage - 1;
                personPage = persons.Skip(currentPage * numOfValue).Take(numOfValue).ToList();
                personPage.ForEach(x => Console.WriteLine(x.Id + "||" + x.Name + "||" + x.Price + "||" + x.Category));
                break;
            case 5:
                isMenu = false;
                break;
            default:
                Console.WriteLine("Lua chon khong hop le vui long nhap lai!");
                break;
        }
    }
    else
    {
        Console.WriteLine("Lua chon khong hop le vui long nhap lai!");
    }
}

public class Person
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Category { get; set; }
    public double Price { get; set; }
}
