﻿// See https://aka.ms/new-console-template for more information
using Newtonsoft.Json;


// đọc file mock-data.json trong project để lấy dữ liệu
string jsonPath = AppDomain.CurrentDomain.BaseDirectory.Split("bin").First() + "mock-data.json";
string jsonData = File.ReadAllText(jsonPath);
List<Person> persons = JsonConvert.DeserializeObject<List<Person>>(jsonData);
Console.WriteLine(persons.Count);

// sử dụng LinQ để lọc các dữ liệu như yêu cầu bên dưới

//Lấy ra danh sách các FirstName của tất cả các nhân viên.
var listFirstName = persons.Select(x => x.FirstName).ToList();
//listFirstName.ForEach(x => Console.WriteLine("First name: " + x));

//Lấy ra danh sách các nhân viên có Salary lớn hơn 50000$.
var listEmployee = persons.Where(x => x.Salary > 50000).ToList();
//listEmployee.ForEach(x => Console.WriteLine(x.FirstName + " " + x.LastName + ": " + x.Salary));

//Lấy ra danh sách các nhân viên có Gender là "Male" và sắp xếp tăng dần theo FirstName.
var listMaleEmployee = persons.Where(x => x.Gender == "Male").OrderBy(x => x.FirstName).ToList();
//listMaleEmployee.ForEach(x => Console.WriteLine(x.FirstName + " Gender: " + x.Gender));

//Lấy ra danh sách các nhân viên có Country là "Indonesia" và JobTitle chứa "Manager".
var listIndoManager = persons.Where(x => x.country == "Indonesia" && (x.JobTitle?.Contains("Manager") ?? false)).ToList();
//listIndoManager.ForEach(x => Console.WriteLine(x.FirstName + " " + x.country + " " + x.JobTitle));

//Lấy ra danh sách các nhân viên có Email và sắp xếp giảm dần theo LastName.
var listEmployeeEmail = persons.Where(x => !string.IsNullOrEmpty(x.Email)).OrderByDescending(x => x.LastName).ToList();
//listEmployeeEmail.ForEach(x => Console.WriteLine(x.FirstName + " " + x.Email + " " + x.LastName));

//Lấy ra danh sách các nhân viên có StartDate trước ngày "2022-01-01" và Salary lớn hơn 60000$.
var listEmployeeStartDate = persons.Where(x => x.StartDate < new DateTime(2022, 1, 1) && x.Salary > 60000).ToList();
//listEmployeeStartDate.ForEach(x => Console.WriteLine(x.LastName + " " + x.StartDate));

//Lấy ra danh sách các nhân viên có PostalCode là null hoặc rỗng và sắp xếp tăng dần theo LastName.
var listPostalCode = persons.Where(x => string.IsNullOrEmpty(x.PostalCode)).OrderBy(persons => persons.LastName).ToList();
//listPostalCode.ForEach(x => Console.WriteLine(x.LastName + " Check Postal null: " + x.PostalCode));

//Lấy ra danh sách các nhân viên có FirstName và LastName được viết hoa và sắp xếp giảm dần theo Id.
var listUpperName = persons.Select(x => new Person
{
    Id = x.Id,
    FirstName = x.FirstName.ToUpper(),
    LastName = x.LastName.ToUpper(),
    Email = x.Email,
}
).OrderByDescending(x => x.Id).ToList();
//listUpperName.ForEach(x => Console.WriteLine(x.Id + " " + x.FirstName + " " + x.LastName));

//Lấy ra danh sách các nhân viên có Salary nằm trong khoảng từ 50000$ đến 70000$ và sắp xếp tăng dần theo Salary.
var listEmployeeSalary = persons.Where(x => x.Salary >= 50000 && x.Salary <= 70000).OrderBy(x => x.Salary).ToList();
//listEmployeeSalary.ForEach(x => Console.WriteLine(x.FirstName + " Salary: " + x.Salary));

//Lấy ra danh sách các nhân viên có FirstName chứa chữ "a" hoặc "A" và sắp xếp giảm dần theo LastName.
var listEmployeeFirstName = persons.Where(x => x.FirstName.Contains("a") || x.FirstName.Contains("A")).OrderByDescending(x => x.LastName).ToList();
//listEmployeeFirstName.ForEach(x => Console.WriteLine(x.FirstName + " LastName: " + x.LastName));


Console.ReadKey();

public class Person
{ 
    public int Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Gender { get; set; }
    public string country { get; set; }
    public string PostalCode { get; set; }
    public string? JobTitle { get; set; }
    public DateTime? StartDate { get; set; }
    public double Salary { get; set; }
}