﻿// Đọc dữ liệu từ file JSON
using Newtonsoft.Json;

string jsonPath = AppDomain.CurrentDomain.BaseDirectory.Split("bin").First();
var productsJson = File.ReadAllText(jsonPath + "products.json");
var categoriesJson = File.ReadAllText(jsonPath + "categories.json");

// Chuyển đổi JSON thành danh sách các đối tượng
var products = JsonConvert.DeserializeObject<List<Product>>(productsJson);
var categories = JsonConvert.DeserializeObject<List<Category>>(categoriesJson);

//Lấy danh sách tên sản phẩm và giá cho các sản phẩm có giá trị lớn hơn $100, sắp xếp theo giá giảm dần.
var productsPrice100 = products.Where(x => x.Price > 100).Select(x => new Product { Name = x.Name, Price = x.Price}).OrderByDescending(x => x.Price).ToList();
//productsPrice100.ForEach(x => Console.WriteLine(x.Name + " || " + x.Price));

//Lấy danh sách tất cả các danh mục và số lượng sản phẩm trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
var listCategories = products.Join(categories,
                                    product => product.CategoryId,
                                    category => category.Id,
                                    (product, category) => new
                                    {
                                        categoryName = category.Name,
                                        productName = product.Name
                                    }).GroupBy(x => x.categoryName).OrderBy(x => x.Key).ToList();

//var listProductGroupByCategory = products.GroupBy(x => x.CategoryId).ToList();
//listCategories.ForEach(x => Console.WriteLine(x.Key.ToString() + " || Count: " + x.Count()));

//Lấy giá trung bình của các sản phẩm cho mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
var avgPrice = products.Join(categories,
                                    product => product.CategoryId,
                                    category => category.Id,
                                    (product, category) => new
                                    {
                                        categoryName = category.Name,
                                        productName = product.Name,
                                        productPrice = product.Price
                                    }).GroupBy(x => x.categoryName)
                                    .Select(x => new
                                    {
                                        CategoryName = x.Key,
                                        AveragePrice = x.Average(x => x.productPrice)
                                    }).OrderBy(x => x.AveragePrice).ToList();
//avgPrice.ForEach(x => Console.WriteLine("Name: " + x.CategoryName + "|| Average price: " + x.AveragePrice));

//Lấy danh sách tên sản phẩm và giá cho 10 sản phẩm đắt nhất, sắp xếp theo giá giảm dần.
var listProduct = products.OrderByDescending(x => x.Price).Take(10).ToList();
//Console.WriteLine("\n\n Top 10 san pham dat nhat");
//listProduct.ForEach(x => Console.WriteLine("Name: " + x.Name + "Price: " + x.Price));

//Lấy danh sách tất cả các danh mục và giá trung bình của các sản phẩm trong mỗi danh mục, sắp xếp theo giá trung bình giảm dần.
var avgPriceDesc = products.Join(categories,
                                    product => product.CategoryId,
                                    category => category.Id,
                                    (product, category) => new
                                    {
                                        categoryName = category.Name,
                                        productName = product.Name,
                                        productPrice = product.Price
                                    }).GroupBy(x => x.categoryName)
                                    .Select(x => new
                                    {
                                        CategoryName = x.Key,
                                        AveragePrice = x.Average(x => x.productPrice)
                                    }).OrderByDescending(x => x.AveragePrice).ToList();
//avgPrice.ForEach(x => Console.WriteLine("Name: " + x.CategoryName + "|| Average price: " + x.AveragePrice));

//Lấy danh sách tên sản phẩm và danh mục cho các sản phẩm có giá trị nhỏ hơn $50, sắp xếp theo tên danh mục tăng dần.
var listProductLower50 = products.Where(x => x.Price < 200).Join(categories,
                                                                product => product.CategoryId,
                                                                category => category.Id,
                                                                (product, category) => new
                                                                {
                                                                    ProductName = product.Name,
                                                                    CategoryName = category.Name,
                                                                    ProductPrice = product.Price
                                                                }).OrderBy(x => x.CategoryName).ToList();
//listProductLower50.ForEach(x => Console.WriteLine("Category: " + x.CategoryName + " || Product Name: " + x.ProductName + " || Product Price: " + x.ProductPrice));


//Tính tổng giá của tất cả sản phẩm trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
var sumPrice = products.Join(categories,
                                    product => product.CategoryId,
                                    category => category.Id,
                                    (product, category) => new
                                    {
                                        categoryName = category.Name,
                                        productName = product.Name,
                                        productPrice = product.Price
                                    }).GroupBy(x => x.categoryName)
                                    .Select(x => new
                                    {
                                        CategoryName = x.Key,
                                        SumPrice = x.Sum(x => x.productPrice)
                                    }).OrderBy(x => x.SumPrice).ToList();
//sumPrice.ForEach(x => Console.WriteLine("Name: " + x.CategoryName + "|| Sum price: " + x.SumPrice));


//Lấy danh sách tên sản phẩm và danh mục cho các sản phẩm có tên chứa từ "Apple", sắp xếp theo tên sản phẩm tăng dần.
var listApple = products.Where(x => x.Name.Contains("Apple")).Join(categories,
                                                                product => product.CategoryId,
                                                                category => category.Id,
                                                                (product, category) => new
                                                                {
                                                                    ProductName = product.Name,
                                                                    CategoryName = category.Name,
                                                                    ProductPrice = product.Price
                                                                }).OrderBy(x => x.ProductName).ToList();
//listApple.ForEach(x => Console.WriteLine("Category: " + x.CategoryName + " || Product Name: " + x.ProductName + " || Product Price: " + x.ProductPrice));

//Lấy danh sách tất cả các danh mục và tổng giá của các sản phẩm trong mỗi danh mục, chỉ lấy những danh mục có tổng giá trị lớn hơn $1000.
var sumPriceBigger1000 = products.Join(categories,
                                    product => product.CategoryId,
                                    category => category.Id,
                                    (product, category) => new
                                    {
                                        categoryName = category.Name,
                                        productName = product.Name,
                                        productPrice = product.Price
                                    }).GroupBy(x => x.categoryName)
                                    .Select(x => new
                                    {
                                        CategoryName = x.Key,
                                        SumPrice = x.Sum(x => x.productPrice)
                                    }).Where(x => x.SumPrice > 1000).ToList();
//sumPrice.ForEach(x => Console.WriteLine("Name: " + x.CategoryName + "|| Sum price: " + x.SumPrice));

//Kiểm tra xem có danh mục nào có sản phẩm có giá trị nhỏ hơn $10 không, nếu có, lấy danh sách tên của những danh mục đó.
var listProductLower10 = products.Where(x => x.Price < 10).Join(categories,
                                                                product => product.CategoryId,
                                                                category => category.Id,
                                                                (product, category) => new
                                                                {
                                                                    ProductName = product.Name,
                                                                    CategoryName = category.Name,
                                                                    ProductPrice = product.Price
                                                                }).OrderBy(x => x.CategoryName).ToList();

//if(listProductLower10.Any(x => x.ProductPrice < 10))
//{
//    Console.WriteLine("Product with price < 10: ");
//    listProductLower10.ForEach(x => Console.WriteLine("Category: " + x.CategoryName + " || Product Name: " + x.ProductName + " || Product Price: " + x.ProductPrice));
//}
//else
//{
//    Console.WriteLine("There's no category with product having price < 10");
//}
//Lấy danh sách các sản phẩm có giá trị lớn nhất trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
var listHighestProductInCategory = products.Join(categories,
                                    product => product.CategoryId,
                                    category => category.Id,
                                    (product, category) => new
                                    {
                                        categoryName = category.Name,
                                        productName = product.Name,
                                        productPrice = product.Price
                                    }).GroupBy(x => x.categoryName)
                                    .Select(group => new
                                    {
                                        CategoryName = group.Key,
                                        highestProducts = group.Where(x => x.productPrice == group.Max(p => p.productPrice))
                                    }).ToList();

//foreach (var category in listHighestProductInCategory)
//{
//    Console.WriteLine($"Category: {category.CategoryName}");
//    foreach (var product in category.highestProducts)
//    {
//        Console.WriteLine($"- Product: {product.productName}, Price: {product.productPrice}");
//    }
//}

//Lấy danh sách tất cả các danh mục và tổng số tiền của các sản phẩm trong mỗi danh mục, sắp xếp theo tổng số tiền giảm dần.
var sumPriceDesc = products.Join(categories,
                                    product => product.CategoryId,
                                    category => category.Id,
                                    (product, category) => new
                                    {
                                        categoryName = category.Name,
                                        productName = product.Name,
                                        productPrice = product.Price
                                    }).GroupBy(x => x.categoryName)
                                    .Select(x => new
                                    {
                                        CategoryName = x.Key,
                                        SumPrice = x.Sum(x => x.productPrice)
                                    }).OrderByDescending(x => x.SumPrice).ToList();

//sumPriceDesc.ForEach(x => Console.WriteLine("Name: " + x.CategoryName + "|| Sum price: " + x.SumPrice));

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị lớn hơn giá trị trung bình của tất cả các sản phẩm.
decimal averagePrice = products.Average(p => p.Price);

var selectedProducts = products
          .Where(p => p.Price > averagePrice)
          .Select(p => new
          {
              ProductName = p.Name,
              CategoryName = categories.FirstOrDefault(c => c.Id == p.CategoryId).Name,
              ProductPrice = p.Price
          })
          .ToList();
//selectedProducts.ForEach(x => Console.WriteLine("Product Name: " + x.ProductName + "Category: " + x.CategoryName + "Price: " + x.ProductPrice));



//Tính tổng số tiền của tất cả các sản phẩm.
var sumProducts = products.Sum(p => p.Price);
//Console.WriteLine("Sum of all product: " + sumProducts);

//Lấy danh sách các danh mục và số lượng sản phẩm có giá trị cao nhất trong mỗi danh mục, sắp xếp theo số lượng sản phẩm giảm dần.
var listHighestProductInCategoryDesc = products.Join(categories,
                                    product => product.CategoryId,
                                    category => category.Id,
                                    (product, category) => new
                                    {
                                        categoryName = category.Name,
                                        productName = product.Name,
                                        productPrice = product.Price
                                    }).GroupBy(x => x.categoryName)
                                    .Select(group => new
                                    {
                                        CategoryName = group.Key,
                                        numHighestProducts = group.Where(x => x.productPrice == group.Max(p => p.productPrice)).Count()
                                    }).OrderByDescending(x => x.numHighestProducts).ToList();
//listHighestProductInCategoryDesc.ForEach(x => Console.WriteLine(x.CategoryName + " || " + x.numHighestProducts));
//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị nhỏ nhất trong mỗi danh mục, sắp xếp theo giá trị giảm dần.
var listLowestProductInCategoryDesc = products.Join(categories,
                                    product => product.CategoryId,
                                    category => category.Id,
                                    (product, category) => new
                                    {
                                        categoryName = category.Name,
                                        productName = product.Name,
                                        productPrice = product.Price
                                    }).GroupBy(x => x.categoryName)
                                    .Select(group => new
                                    {
                                        CategoryName = group.Key,
                                        LowestPrice = group.Where(x => x.productPrice == group.Min(p => p.productPrice)).OrderByDescending(p => p.productPrice)
                                    }).ToList();
//foreach(var category in listLowestProductInCategoryDesc)
//{
//    Console.WriteLine("Category:" + category.CategoryName);
//    foreach (var product in category.LowestPrice)
//    {
//        Console.WriteLine(product.productName + " || " + product.productPrice);
//    }
//}


//Lấy danh sách các danh mục có ít sản phẩm nhất.
var minProduct = products.Join(categories,
                                    product => product.CategoryId,
                                    category => category.Id,
                                    (product, category) => new
                                    {
                                        categoryName = category.Name,
                                        productName = product.Name,
                                        productPrice = product.Price
                                    }).GroupBy(x => x.categoryName)
                                    .Select(group => new
                                    {
                                        CountProduct = group.Count()
                                    }).Min(p => p.CountProduct);

var listLeastProducts = products.Join(categories,
                                    product => product.CategoryId,
                                    category => category.Id,
                                    (product, category) => new
                                    {
                                        categoryName = category.Name,
                                        productName = product.Name,
                                        productPrice = product.Price
                                    }).GroupBy(x => x.categoryName)
                                    .Select(group => new
                                    {
                                        CategoryName = group.Key,
                                        CountProduct = group.Count()
                                    })
                                    .Where(x => x.CountProduct == minProduct)
                                    .ToList();


//listLeastProducts.ForEach(x => Console.WriteLine(x.CategoryName + " || " + x.CountProduct));

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị cao nhất trong mỗi danh mục, sắp xếp theo giá trị tăng dần.
 var listHighestProductInCategoryAsc = products.Join(categories,
                                    product => product.CategoryId,
                                    category => category.Id,
                                    (product, category) => new
                                    {
                                        categoryName = category.Name,
                                        productName = product.Name,
                                        productPrice = product.Price
                                    }).GroupBy(x => x.categoryName)
                                    .Select(group => new
                                    {
                                        CategoryName = group.Key,
                                        HighestPrice = group.Where(x => x.productPrice == group.Max(p => p.productPrice)).OrderBy(p => p.productPrice)
                                    }).ToList();
//foreach (var category in listHighestProductInCategoryAsc)
//{
//    Console.WriteLine("Category:" + category.CategoryName);
//    foreach (var product in category.HighestPrice)
//    {
//        Console.WriteLine(product.productName + " || " + product.productPrice);
//    }
//}


//Tính tổng số tiền của các sản phẩm có giá trị lớn hơn $50.

var sumProductHigher50 = products.Where(p => p.Price > 50).Sum(p => p.Price);
Console.WriteLine("sum higher than 50" + sumProductHigher50);

    //Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị nhỏ nhất trong mỗi danh mục, sắp xếp theo giá trị tăng dần.
    var listLowestProductInCategoryAsc = products.Join(categories,
                                        product => product.CategoryId,
                                        category => category.Id,
                                        (product, category) => new
                                        {
                                            categoryName = category.Name,
                                            productName = product.Name,
                                            productPrice = product.Price
                                        }).GroupBy(x => x.categoryName)
                                        .Select(group => new
                                        {
                                            CategoryName = group.Key,
                                            ProductLowestPrice = group.Where(x => x.productPrice == group.Min(p => p.productPrice)),
                                            LowestPrice = group.Where(x => x.productPrice == group.Min(p => p.productPrice)).Select(x => x.productPrice)
                                        }).OrderBy(p => p.LowestPrice.First()).ToList();
foreach (var category in listLowestProductInCategoryAsc)
{
    Console.WriteLine("Category:" + category.CategoryName);
    Console.WriteLine("price: " + category.LowestPrice.First()); 
    foreach (var product in category.ProductLowestPrice)
    {
        Console.WriteLine(product.productName + " || " + product.productPrice);
    }
}

class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int CategoryId { get; set; }
    public decimal Price { get; set; }
}

class Category
{
    public int Id { get; set; }
    public string Name { get; set; }
}
