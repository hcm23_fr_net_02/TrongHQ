﻿// See https://aka.ms/new-console-template for more information
var numbers = Enumerable.Range(0, 50);

int numOfValue = 10;
int currentPage = 0;
var valuePage = numbers.Skip(currentPage * numOfValue).Take(numOfValue);
foreach (var value in valuePage)
{
    Console.WriteLine(value);
}



// Create Menu
bool isMenu = true;
while (isMenu)
{
    Console.WriteLine("===========================Menu=============================");
    Console.WriteLine($"1. Chon so phan tu trong 1 trang({numOfValue} phan tu)   ");
    Console.WriteLine("2. trang hien tai                                         ");
    Console.WriteLine("3. trang tiep                                             ");
    Console.WriteLine("4. trang truoc                                            ");
    Console.WriteLine("5. Ket thuc chuong trinh                                  ");
    Console.WriteLine("============================================================");
    int choice;
    if (int.TryParse(Console.ReadLine(), out choice))
    {
        switch (choice)
        {
            case 1:
                Console.Write("So luong phan tu: ");
                numOfValue = int.Parse(Console.ReadLine());
                currentPage = 0;
                Console.WriteLine("Thay doi thanh cong Trang hien tai reset ve trang dau ! \n \n");
                break;
            case 2:
                valuePage = numbers.Skip(currentPage * numOfValue).Take(numOfValue);
                foreach (var value in valuePage)
                {
                    Console.WriteLine(value);
                }
                break;
            case 3:
                currentPage = currentPage + 1;
                valuePage = numbers.Skip(currentPage * numOfValue).Take(numOfValue);
                if (valuePage.Count() != 0)
                {
                    foreach (var value in valuePage)
                    {
                        Console.WriteLine(value);
                    }
                }
                else
                {
                    Console.WriteLine("Day la trang cuoi cung khong the hien thi tiep");
                    currentPage = currentPage - 1;
                }
                break;
            case 4:
                if (currentPage == 0)
                {
                    Console.WriteLine("Day la trang dau tien khong the ve trang truoc!");
                    break;
                }
                currentPage = currentPage - 1;
                valuePage = numbers.Skip(currentPage * numOfValue).Take(numOfValue);
                foreach (var value in valuePage)
                {
                    Console.WriteLine(value);
                }
                break;
            case 5:
                isMenu = false;
                break;
            default:
                Console.WriteLine("Lua chon khong hop le vui long nhap lai!");
                break;
        }
    }
    else
    {
        Console.WriteLine("Lua chon khong hop le vui long nhap lai!");
    }
}