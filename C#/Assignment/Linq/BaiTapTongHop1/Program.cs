﻿// Hãy giả định chúng ta có một danh sách các sản phẩm của một cửa hàng với các thuộc tính sau: 
// tên sản phẩm, giá, và danh mục (category). 
// Hãy tạo một danh sách các sản phẩm và thực hiện các yêu cầu sau bằng cách sử dụng Linq:

// Tạo danh sách các sản phẩm:
var products = new List<Product>
{
    new Product { Name = "Áo sơ mi", Price = 300000, Category = "Áo" },
    new Product { Name = "Quần jeans", Price = 500000, Category = "Quần" },
    new Product { Name = "Giày thể thao", Price = 800000, Category = "Giày" },
    new Product { Name = "Ví da", Price = 200000, Category = "Phụ kiện" },
    new Product { Name = "Mũ nón", Price = 100000, Category = "Phụ kiện" },
    new Product { Name = "Đầm nhảy", Price = 1200000, Category = "Đầm" },
    new Product { Name = "Áo thun", Price = 500000, Category = "Áo" },
    new Product { Name = "Quần lửng", Price = 2500000, Category = "Quần" },
    new Product { Name = "Quần kaki", Price = 3500000, Category = "Quần" },
    new Product { Name = "Dép đi mưa", Price = 100000, Category = "Dép" },
    new Product { Name = "Áo vest", Price = 5000000, Category = "Áo" },
    new Product { Name = "Áo polo", Price = 1500000, Category = "Áo" },
};

// Tìm danh sách các sản phẩm có giá lớn hơn 500,000 đồng:
var productPriceBiggerThan500 = products.Where(x => x.Price > 500000).ToList();
//productPriceBiggerThan500.ForEach(x => Console.WriteLine(x.Name + "||" + x.Price));

// Lấy ra tên của các sản phẩm có giá nhỏ hơn 300,000 đồng và sắp xếp theo thứ tự giảm dần của giá:
var productPriceLower300 = products.Select(x => new Product { Name = x.Name, Price = x.Price }).Where(x => x.Price < 300000).OrderByDescending(x => x.Price).ToList();
//productPriceLower300.ForEach(x => Console.WriteLine(x.Name + "||" + x.Price));

// Tạo một dictionary với danh mục sản phẩm là key và danh sách các sản phẩm trong cùng danh mục là value:
var groupedProducts = products.GroupBy(p => p.Category);
var productDictionary = groupedProducts.ToDictionary(g => g.Key, g => g.ToList());

//foreach (var category in productDictionary.Keys)
//{
//    Console.WriteLine($"Danh mục: {category}");
//    foreach (var product in productDictionary[category])
//    {
//        Console.WriteLine($"- {product.Name} - {product.Price}");
//    }
//    Console.WriteLine();
//}

// Tính tổng giá tiền của các sản phẩm trong danh mục "Áo":
var sumShirtProducts = products.Where(x => x.Category == "Áo").Sum(x => x.Price);
Console.WriteLine("Sum of products in category Ao: " + sumShirtProducts);


// Tìm sản phẩm có giá cao nhất:
var maxShirtProducts = products.Max(x => x.Price);
Console.WriteLine("Max price of products: " + maxShirtProducts);

// Tạo danh sách các sản phẩm với tên đã viết hoa:
var toUpperProducts = products.Select(x => x.Name.ToUpper()).ToList();
toUpperProducts.ForEach(x => Console.WriteLine(x));

class Product
{
    public string Name { get; set; }
    public int Price { get; set; }
    public string Category { get; set; }
}
