﻿// See https://aka.ms/new-console-template for more information

List<string> image = new List<string>()
{
    "https://img.freepik.com/free-photo/cherry-blossoms-spring-chureito-pagoda-fuji-mountain-japan_335224-177.jpg?w=996&t=st=1696492129~exp=1696492729~hmac=27ad228e2d3e0c104caff27a25a134d1feaa71c26852209d43d2bc1dd79fdb3f",
    "https://img.freepik.com/free-photo/yasaka-pagoda-sannen-zaka-street-kyoto-japan_335224-12.jpg?w=996&t=st=1696492308~exp=1696492908~hmac=6ae71f7f3e2fb6d20bd14495763468965726f99c28c7f23e30f4dbe69d208d99",
    "https://img.freepik.com/free-photo/travel-shanghai-avenue-exterior-building-skyline_1417-1086.jpg?w=1380&t=st=1696492331~exp=1696492931~hmac=d4fb32530e38f7b416324ecef6b07dc07d708773bf0d2b73339051a48426ee3a"
};
string DirectoryPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images");
if (!Directory.Exists(DirectoryPath))
{
    Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images"));
}
foreach (var item in image)
{
    await DownloadImage(item, image.IndexOf(item).ToString() + ".jpg");
}
await DownloadImage("https://img.freepik.com/free-photo/cherry-blossoms-spring-chureito-pagoda-fuji-mountain-japan_335224-177.jpg?w=996&t=st=1696492129~exp=1696492729~hmac=27ad228e2d3e0c104caff27a25a134d1feaa71c26852209d43d2bc1dd79fdb3f", "ABC.jpg");

Console.ReadKey();
async Task DownloadImage(string imageUrl, string localFilePath)
{
    using (HttpClient httpClient = new HttpClient())
    {
        try
        {
            HttpResponseMessage response = await httpClient.GetAsync(imageUrl);

            if (response.IsSuccessStatusCode)
            {
                byte[] imageBytes = await response.Content.ReadAsByteArrayAsync();
                // Save the image to a local file
                System.IO.File.WriteAllBytes("Images\\" + localFilePath, imageBytes);
                Console.WriteLine($"Image downloaded and saved to {localFilePath}");
            }
            else
            {
                Console.WriteLine($"Failed to download the image. Status code: {response.StatusCode}");
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred: {ex.Message}");
        }
    }
}