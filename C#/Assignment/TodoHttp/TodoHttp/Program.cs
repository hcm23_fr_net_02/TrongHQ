﻿// See https://aka.ms/new-console-template for more information
using Newtonsoft.Json;
using System.Text;

HttpClient httpClient = new HttpClient();
string url = "https://651e360144e393af2d5a9956.mockapi.io/Todo";
Console.WriteLine("Fetching Data...");
HttpResponseMessage response = httpClient.GetAsync(url).Result;
string todoJson = await response.Content.ReadAsStringAsync();
List<Todo>? todoList = JsonConvert.DeserializeObject<List<Todo>>(todoJson);
Console.WriteLine("Fetched Data!");

bool isChoosing = true;
while (isChoosing)
{
    Console.WriteLine("---------------Todo Menu----------------");
    Console.WriteLine("1. Get List todo");
    Console.WriteLine("2. Add Todo");
    Console.WriteLine("3. Remove todo");
    Console.WriteLine("4. Mark complete");
    Console.WriteLine("5. Add priority");
    Console.WriteLine("6. Exit");
    Console.Write("Enter your choice: ");
    if (int.TryParse(Console.ReadLine(), out int choice))
    {
        switch (choice)
        {
            case 1:
                ShowList(todoList);
                break;
            case 2:
                Console.Write("Input title: ");
                string title = Console.ReadLine();
                Console.Write("Input description: ");
                string description = Console.ReadLine();
                Console.Write("Input priority: ");
                int priority = int.Parse(Console.ReadLine());
                Todo todo = new Todo(title, description, priority);
                todo.id = todoList.Count() - 1;
                todoList.Add(todo);
                await AddTodo(todo);
                break;
            case 3:
                Console.Write("Input title you want to delete: ");
                string titleDel = Console.ReadLine();
                var todoDel = todoList.Find(x => x.Title == titleDel);
                if(todoDel != null)
                {
                    await RemoveTodo(todoDel.id);
                }
                else
                {
                    Console.WriteLine("Todo not found!");
                }

                break;
            case 4:
                Console.Write("Input title you want to complete: ");
                string titleComplete = Console.ReadLine();
                var todoComplete = todoList.Find(x => x.Title == titleComplete);
                if (todoComplete != null)
                {
                    await UpdateTodo(todoComplete,true);
                }
                else
                {
                    Console.WriteLine("Todo not found!");
                }
                break;
            case 5:
                Console.Write("Input title you want to complete: ");
                string titlePriority = Console.ReadLine();
                var todoPriority = todoList.Find(x => x.Title == titlePriority);
                if (todoPriority != null)
                {
                    if (todoPriority.Priority == 1)
                    {
                        Console.WriteLine("This todo is already the highest priority");
                        break;
                    }
                    await UpdateTodo(todoPriority,false);
                }
                else
                {
                    Console.WriteLine("Todo not found!");
                }
                break;
            case 6:
                isChoosing = false; 
                break;
            default:
                Console.WriteLine("Invalid choice! please choose again");
                break;
        }
    }
}

httpClient.Dispose();
Console.ReadKey();

void ShowList(List<Todo> todoList)
{
    Console.WriteLine("---------------Show list menu----------------");
    Console.WriteLine("1. List Orderer by Date");
    Console.WriteLine("2. List Orderer by Priority");
    Console.Write("Enter your choice: ");
    if (int.TryParse(Console.ReadLine(), out int choice))
    {
        switch (choice)
        {
            case 1:
                //Show list todo order by Date Created
                Console.WriteLine("\n\n------------------Date------------------\n\n");

                var orderListByDate = todoList.OrderBy(todo => todo.DateCreated);
                foreach (var item in orderListByDate)
                {
                    Console.WriteLine("Title: " + item.Title + "|| Description: " + item.Description + " || Date: " + item.DateCreated.ToString() + " || isCompleted: " + item.isCompleted);
                }
                break;
            case 2:
                var orderListByPriority = todoList.OrderBy(todo => todo.Priority);
                Console.WriteLine("\n\n------------------Priority------------------\n\n");
                foreach (var item in orderListByPriority)
                {
                    Console.WriteLine("Title: " + item.Title + "|| Description:" + item.Description + " || Priority: " + item.Priority + " || isCompleted: " + item.isCompleted);
                }
                break;
            default:
                Console.WriteLine("Invalid choice!");
                break;
        }
    }
    else
    {
        Console.WriteLine("Invalid choice!");
    }
}

async Task AddTodo(Todo todo)
{
    var contentJson = JsonConvert.SerializeObject(todo);
    var content = new StringContent(contentJson, Encoding.UTF8, "application/json");

    HttpClient httpClient = new HttpClient();
    string url = "https://651e360144e393af2d5a9956.mockapi.io/Todo";
    Console.WriteLine("Adding Data...");
    HttpResponseMessage response = await httpClient.PostAsync(url,content);
    if (response.IsSuccessStatusCode)
    {
        Console.WriteLine("Added successfully");
    }
    else
    {
        Console.WriteLine("Connection error");
    }
}

async Task RemoveTodo(int ID)
{
    HttpClient httpClient = new HttpClient();
    string url = "https://651e360144e393af2d5a9956.mockapi.io/Todo/" + ID;
    Console.WriteLine("Removing Data...");
    HttpResponseMessage response = await httpClient.DeleteAsync(url);
    if (response.IsSuccessStatusCode)
    {
        Console.WriteLine("Removed successfully");
    }
    else
    {
        Console.WriteLine(response.StatusCode);
    }
}

async Task UpdateTodo(Todo todo, bool isCompleting = false)
{
    if(isCompleting)
        todo.isCompleted = true;
    else
        todo.Priority = todo.Priority - 1;
    var contentJson = JsonConvert.SerializeObject(todo);
    var content = new StringContent(contentJson, Encoding.UTF8, "application/json");
    HttpClient httpClient = new HttpClient();
    string url = "https://651e360144e393af2d5a9956.mockapi.io/Todo/" + todo.id;
    Console.WriteLine("Updating Data...");
    HttpResponseMessage response = await httpClient.PutAsync(url,content);
    if (response.IsSuccessStatusCode)
    {
        Console.WriteLine("Updated successfully");
    }
    else
    {
        Console.WriteLine(response.StatusCode);
    }
}



public class Todo
{
    public Todo(string Title, string Description, int Priority)
    {
        this.Title = Title;
        this.DateCreated = DateTime.Now;
        this.Description = Description;
        this.Priority = Priority;
        this.isCompleted = false;
    }

    public int id { get; set; }
    public string Title { get; set; }
    public DateTime DateCreated { get; set; }
    public string Description { get; set; }
    public int Priority { get; set; }
    public bool isCompleted { get; set; }
}
