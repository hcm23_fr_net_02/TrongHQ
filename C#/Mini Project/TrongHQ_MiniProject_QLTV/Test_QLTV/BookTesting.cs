﻿using Microsoft.Extensions.DependencyInjection;
using TrongHQ_MiniProject_QLTV.Common;
using TrongHQ_MiniProject_QLTV.DataAccess;
using TrongHQ_MiniProject_QLTV.Helper;
using TrongHQ_MiniProject_QLTV.Models;
using TrongHQ_MiniProject_QLTV.Services.Implements;
using TrongHQ_MiniProject_QLTV.Services.Interfaces;
using TrongHQ_MiniProject_QLTV.UnitOfWork.Interfaces;

namespace Test_QLTV
{
    public class BookTesting
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IBookServices _bookServices;
        private readonly IUnitOfWork _unitOfWork;

        public BookTesting()
        {
            _serviceProvider = new ServiceCollection().ConfigureDependencyInjection().BuildServiceProvider();
            _unitOfWork = _serviceProvider.GetService<IUnitOfWork>();
            _bookServices = new BookServices(_unitOfWork);
            SeedingJsonData.SeedBooks(_unitOfWork.BookRepository, _unitOfWork.ReviewRepository);
        }

        [Fact]
        public void GetBookById_WithValidId_ReturnsBook()
        {
            // Arrange
            int validId = 4;
            // Act
            var result = _bookServices.GetBookById(validId);
            // Assert
            Assert.Equal(validId, result.Id);
        }

        [Fact]
        public void AddBook_Success()
        {
            // Arrange
            int count = _bookServices.GetAll().Count;
            Book book = new Book("name", "author", 1970, "genre", 50, 0, new List<Review>(), new List<Borrow>());

            // Act
            _bookServices.AddBook(book);

            // Assert
            Assert.Equal(_bookServices.GetAll().LastOrDefault(), book);
            Assert.Equal(count + 1, _bookServices.GetAll().Count);
        }

        [Fact]
        public void PaginationBook_CheckNumOfElement()
        {
            for (int page = 0; page < 10; page++)
            {
                //Act
                int count = _bookServices.PaginationBook(ref page).Count;

                //Assert
                Assert.True(count >= 0 && count <= 10);
            }
        }

        [Fact]
        public void SearchBook_Test()
        {
            //Arrange
            string keyword = "Mr";
            int type = (int)Constant.SEARCH_BOOK.Name;

            //Act
            List<Book> searchBooks = _bookServices.SearchBook(keyword, type);

            //Assert
            foreach(Book book in searchBooks)
            {
                Assert.True(book.Name.Contains(keyword));
            }
        }

        [Fact]
        public void UpdateBook_Success()
        {
            // Arrange
            int fakeId = 2;
            Book book = _bookServices.GetBookById(fakeId);

            string fakeName = "abcd";
            string fakeAuthor = "test";
            Book updatedBook = new Book() { Name = fakeName, Author = fakeAuthor, PublicationYear = book.PublicationYear, Quantity = book.Quantity };
            // Act
            _bookServices.UpdateBook(fakeId, updatedBook);
            Book result = _bookServices.GetBookById(fakeId);

            // Assert
            Assert.Equal(updatedBook.Name, result.Name);
            Assert.Equal(updatedBook.Author, result.Author);
        }

        [Fact]
        public void DeleteBook_Success()
        {
            // Arrange
            Book book = _bookServices.GetBookById(2);
            // Act
            _bookServices.DeleteBook(book);

            // Assert
            Assert.Throws<Exception>(() => _bookServices.DeleteBook(_bookServices.GetBookById(2)));
        }



    }
}
