﻿
using Microsoft.Extensions.DependencyInjection;
using TrongHQ_MiniProject_QLTV.DataAccess;
using TrongHQ_MiniProject_QLTV.Helper;
using TrongHQ_MiniProject_QLTV.Models;
using TrongHQ_MiniProject_QLTV.Services.Implements;
using TrongHQ_MiniProject_QLTV.Services.Interfaces;
using TrongHQ_MiniProject_QLTV.UnitOfWork.Interfaces;

namespace Test_QLTV
{
    public class CustomerTesting
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ICustomerServices _customerServices;
        private readonly IUnitOfWork _unitOfWork;

        public CustomerTesting()
        {
            _serviceProvider = new ServiceCollection().ConfigureDependencyInjection().BuildServiceProvider();
            _unitOfWork = _serviceProvider.GetService<IUnitOfWork>();
            _customerServices = new CustomerServices(_unitOfWork);
            SeedingJsonData.SeedCustomers(_unitOfWork.CustomerRepository);
        }

        [Fact]
        public void AddCustomer_Success()
        {
            // Arrange
            int count = _customerServices.GetAll().Count;
            Customer customer = new Customer("name","address", new List<Borrow>());

            // Act
            _customerServices.AddCustomer(customer);

            // Assert
            Assert.Equal(_customerServices.GetAll().LastOrDefault(), customer);
            Assert.Equal(count + 1, _customerServices.GetAll().Count);
        }

        [Fact]
        public void UpdateCustomer_Success()
        {
            // Arrange
            int fakeId = 2;
            Customer book = _customerServices.GetCustomerById(fakeId);

            string fakeName = "abcd";
            string fakeAdress = "test";
            Customer updatedCustomer = new Customer() { Name = fakeName, Address = fakeAdress};
            // Act
            _customerServices.UpdateCustomer(fakeId, updatedCustomer);
            Customer result = _customerServices.GetCustomerById(fakeId);

            // Assert
            Assert.Equal(updatedCustomer.Name, result.Name);
            Assert.Equal(updatedCustomer.Address, result.Address);
        }
    }
}
