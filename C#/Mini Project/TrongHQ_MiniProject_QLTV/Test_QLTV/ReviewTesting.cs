﻿
using Microsoft.Extensions.DependencyInjection;
using TrongHQ_MiniProject_QLTV.DataAccess;
using TrongHQ_MiniProject_QLTV.Helper;
using TrongHQ_MiniProject_QLTV.Models;
using TrongHQ_MiniProject_QLTV.Services.Implements;
using TrongHQ_MiniProject_QLTV.Services.Interfaces;
using TrongHQ_MiniProject_QLTV.UnitOfWork.Interfaces;

namespace Test_QLTV
{
    public class ReviewTesting
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IReviewServices _reviewServices;
        private readonly IUnitOfWork _unitOfWork;

        public ReviewTesting()
        {
            _serviceProvider = new ServiceCollection().ConfigureDependencyInjection().BuildServiceProvider();
            _unitOfWork = _serviceProvider.GetService<IUnitOfWork>();
            _reviewServices = new ReviewServices(_unitOfWork);
            SeedingJsonData.SeedBooks(_unitOfWork.BookRepository, _unitOfWork.ReviewRepository);
        }

        [Fact]
        public void AddReview_Success()
        {
            // Arrange
            int count = _unitOfWork.ReviewRepository.GetAll().Count;
            int fakeBookId = 5;
            Book book = _unitOfWork.BookRepository.GetBookById(fakeBookId);
            Review review = new Review() { Book = book , Title = "abcd", Descriptions = "Description", Rating = 4};

            // Act
            _reviewServices.AddReview(review);

            // Assert
            Assert.Equal(_unitOfWork.ReviewRepository.GetAll().LastOrDefault(), review);
            Assert.Equal(count + 1, _unitOfWork.ReviewRepository.GetAll().Count);
            Assert.Equal(_unitOfWork.BookRepository.GetBookById(fakeBookId).Reviews.LastOrDefault(), review);
        }
    }
}
