﻿
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualBasic;
using TrongHQ_MiniProject_QLTV.DataAccess;
using TrongHQ_MiniProject_QLTV.Helper;
using TrongHQ_MiniProject_QLTV.Models;
using TrongHQ_MiniProject_QLTV.Services.Implements;
using TrongHQ_MiniProject_QLTV.Services.Interfaces;
using TrongHQ_MiniProject_QLTV.UnitOfWork.Interfaces;

namespace Test_QLTV
{
    public class BorrowTesting
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IBorrowService _borrowServices;
        private readonly IUnitOfWork _unitOfWork;

        public BorrowTesting()
        {
            _serviceProvider = new ServiceCollection().ConfigureDependencyInjection().BuildServiceProvider();
            _unitOfWork = _serviceProvider.GetService<IUnitOfWork>();
            _borrowServices = new BorrowServices(_unitOfWork);
            SeedingJsonData.SeedData(_unitOfWork);
        }

        [Fact]
        public void AddBorrow_Success()
        {
            // Arrange
            int count = _borrowServices.GetAll().Count;
            Book book = _unitOfWork.BookRepository.GetBookById(1);
            Customer customer = _unitOfWork.CustomerRepository.GetCustomerById(1);
            DateTime startDate = new DateTime(2021, 1, 1);
            DateTime endDate = new DateTime(2022, 1, 1);

            // Act
            Borrow borrow = new Borrow
            {
                Book = book,
                Customer = customer,
                StartDate = startDate,
                DueDate = endDate,
                Status = false
            };
            _borrowServices.AddBorrow(borrow);

            // Assert
            Assert.Equal(_borrowServices.GetAll().LastOrDefault(), borrow);
            Assert.Equal(count + 1, _borrowServices.GetAll().Count);
            Assert.Same(book.Borrows.LastOrDefault(), borrow);
            Assert.Same(customer.Borrows.LastOrDefault(), borrow);
        }

        [Fact]
        public void ReturnBook_Success()
        {
            //Arrange
            int fakeId = 5;

            //Act
            _borrowServices.ReturnBook(5);
            Borrow borrow = _borrowServices.GetAll().SingleOrDefault(x => x.Id == fakeId);

            //Assert;
            Assert.True(borrow.Status == true);
        }
    }
}
