﻿
using TrongHQ_MiniProject_QLTV.Repositories.Interfaces;

namespace TrongHQ_MiniProject_QLTV.UnitOfWork.Interfaces
{
    public interface IUnitOfWork
    {
        IBookRepository BookRepository { get; }
        ICustomerRepository CustomerRepository { get; }
        IBorrowRepository BorrowRepository { get; }
        IReviewRepository ReviewRepository { get; }
    }
}
