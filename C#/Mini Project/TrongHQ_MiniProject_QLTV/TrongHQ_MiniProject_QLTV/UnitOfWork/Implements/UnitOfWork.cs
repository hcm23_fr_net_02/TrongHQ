﻿
using TrongHQ_MiniProject_QLTV.DataAccess;
using TrongHQ_MiniProject_QLTV.Repositories.Implements;
using TrongHQ_MiniProject_QLTV.Repositories.Interfaces;
using TrongHQ_MiniProject_QLTV.UnitOfWork.Interfaces;

namespace TrongHQ_MiniProject_QLTV.UnitOfWork.Implements
{
    public class UnitOfWorkClass : IUnitOfWork
    {
        private readonly IBookRepository _bookRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IBorrowRepository _borrowRepository;
        private readonly IReviewRepository _reviewRepository;

        public UnitOfWorkClass(IBookRepository bookRepository,
                               ICustomerRepository customerRepository,
                               IBorrowRepository borrowRepository,
                               IReviewRepository reviewRepository)
        {
            _bookRepository = bookRepository;
            _customerRepository = customerRepository;
            _borrowRepository = borrowRepository;
            _reviewRepository = reviewRepository;
        }

        public IBookRepository BookRepository => _bookRepository;
        public ICustomerRepository CustomerRepository => _customerRepository;
        public IBorrowRepository BorrowRepository => _borrowRepository;
        public IReviewRepository ReviewRepository => _reviewRepository;
    }
}
