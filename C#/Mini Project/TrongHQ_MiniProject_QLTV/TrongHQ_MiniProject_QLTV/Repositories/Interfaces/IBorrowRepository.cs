﻿
using TrongHQ_MiniProject_QLTV.Models;

namespace TrongHQ_MiniProject_QLTV.Repositories.Interfaces
{
    public interface IBorrowRepository : IGenericRepository<Borrow>
    {
        void ReturnBook(int id);
    }
}
