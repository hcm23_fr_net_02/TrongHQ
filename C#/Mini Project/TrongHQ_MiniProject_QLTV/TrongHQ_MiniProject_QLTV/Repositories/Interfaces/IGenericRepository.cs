﻿
namespace TrongHQ_MiniProject_QLTV.Repositories.Interfaces
{
    public interface IGenericRepository<T> where T : class
    {
        List<T> Data { get; }
        List<T> GetAll();
        void Insert(T Entity);
        void Delete(T Entity);
    }
}
