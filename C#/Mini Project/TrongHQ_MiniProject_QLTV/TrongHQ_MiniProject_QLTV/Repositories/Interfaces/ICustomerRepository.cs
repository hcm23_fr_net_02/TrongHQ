﻿
using TrongHQ_MiniProject_QLTV.Models;

namespace TrongHQ_MiniProject_QLTV.Repositories.Interfaces
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
        void UpdateUser(int id, Customer customer);
        void AddBorrow(int bookId, Borrow borrow);
        Customer GetCustomerById(int id);

    }
}
