﻿    
using TrongHQ_MiniProject_QLTV.Models;

namespace TrongHQ_MiniProject_QLTV.Repositories.Interfaces
{
    public interface IReviewRepository: IGenericRepository<Review>
    { 
    }
}
