﻿
using TrongHQ_MiniProject_QLTV.Models;

namespace TrongHQ_MiniProject_QLTV.Repositories.Interfaces
{
    public interface IBookRepository : IGenericRepository<Book>
    {
        Book GetBookById(int id);
        void UpdateBookInfo(int bookId,Book book);
        void AddReview(int bookId, Review review);
        void AddBorrow(int bookId, Borrow borrow);
    }
}
