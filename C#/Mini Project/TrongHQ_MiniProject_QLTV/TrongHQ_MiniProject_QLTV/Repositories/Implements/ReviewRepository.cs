﻿
using TrongHQ_MiniProject_QLTV.Common;
using TrongHQ_MiniProject_QLTV.Models;
using TrongHQ_MiniProject_QLTV.Repositories.Interfaces;

namespace TrongHQ_MiniProject_QLTV.Repositories.Implements
{
    public class ReviewRepository : GenericRepository<Review>, IReviewRepository
    {
    }
}
