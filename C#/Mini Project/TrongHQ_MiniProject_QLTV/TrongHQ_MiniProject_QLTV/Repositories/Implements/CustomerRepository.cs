﻿
using TrongHQ_MiniProject_QLTV.Common;
using TrongHQ_MiniProject_QLTV.Models;
using TrongHQ_MiniProject_QLTV.Repositories.Interfaces;

namespace TrongHQ_MiniProject_QLTV.Repositories.Implements
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        public void UpdateUser(int id, Customer customer)
        {
            var updateCustomer = Data.Find(item => item.Id == id);
            if (updateCustomer == null)
            {
                throw new Exception(Constant.DATA_NOT_FOUND);
            }
            updateCustomer.Name = customer.Name;
            updateCustomer.Address = customer.Address;
        }
        public void AddBorrow(int bookId, Borrow borrow)
        {
            var customer = Data.FirstOrDefault(item => item.Id == bookId);
            if (customer == null)
            {
                throw new Exception(Constant.DATA_NOT_FOUND);
            }
            if (customer.Borrows == null)
            {
                customer.Borrows = new List<Borrow>();
            }
            customer.Borrows.Add(borrow);
        }

        public Customer GetCustomerById(int id)
        {
            var customer = Data.FirstOrDefault(_item => _item.Id == id);
            if (customer == null)
            {
                throw new Exception(Constant.DATA_NOT_FOUND);
            }
            return customer;
        }
    }
}
