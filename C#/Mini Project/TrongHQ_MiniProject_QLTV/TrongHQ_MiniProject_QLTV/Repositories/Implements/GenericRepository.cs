﻿
using TrongHQ_MiniProject_QLTV.Common;
using TrongHQ_MiniProject_QLTV.Repositories.Interfaces;

namespace TrongHQ_MiniProject_QLTV.Repositories.Implements
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private List<T> _data = new List<T>();

        public List<T> Data { get { return _data; } }
        public void Delete(T entity)
        {
            var isRemoved = _data.Remove(entity);
            if (!isRemoved)
            {
                throw new Exception(Constant.DATA_NOT_FOUND);
            }
        }

        public List<T> GetAll()
        {
            return _data;
        }

        public void Insert(T entity)
        {
            _data.Add(entity);
        }
    }
}