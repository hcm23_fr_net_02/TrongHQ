﻿
using TrongHQ_MiniProject_QLTV.Common;
using TrongHQ_MiniProject_QLTV.Models;
using TrongHQ_MiniProject_QLTV.Repositories.Interfaces;

namespace TrongHQ_MiniProject_QLTV.Repositories.Implements
{
    public class BorrowRepository : GenericRepository<Borrow>, IBorrowRepository
    {
        public void ReturnBook(int borrowId)
        {
            var borrow = Data.FirstOrDefault(item => item.Id == borrowId);
            if (borrow == null)
            {
                throw new Exception(Constant.DATA_NOT_FOUND);
            }
            if (borrow.Status == true)
                throw new Exception(Constant.BORROW_RETURNED);
            borrow.Status = true;
            
        }
    }
}
