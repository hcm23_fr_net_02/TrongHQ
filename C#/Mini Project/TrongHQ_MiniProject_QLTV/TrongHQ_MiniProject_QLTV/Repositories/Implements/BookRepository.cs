﻿
using TrongHQ_MiniProject_QLTV.Common;
using TrongHQ_MiniProject_QLTV.Models;
using TrongHQ_MiniProject_QLTV.Repositories.Interfaces;

namespace TrongHQ_MiniProject_QLTV.Repositories.Implements
{
    public class BookRepository : GenericRepository<Book>, IBookRepository
    {
        public void AddReview(int bookId, Review review)
        {
            var book = Data.FirstOrDefault(item => item.Id == bookId);
            if (book == null)
            {
                throw new Exception(Constant.DATA_NOT_FOUND);
            }
            if (book.Reviews == null)
            {
                book.Reviews = new List<Review>();
            }
            book.Reviews.Add(review);
        }

        public Book GetBookById(int id)
        {
            var book = Data.FirstOrDefault(item => item.Id == id);
            if (book == null)
            {
                throw new Exception(Constant.DATA_NOT_FOUND);
            }
            return book;
        }

        public void UpdateBookInfo(int bookId, Book book)
        {
            var updateBook = Data.FirstOrDefault(bookItem => bookItem.Id == bookId);
            if (updateBook == null)
            {
                throw new Exception(Constant.DATA_NOT_FOUND);
            }
            updateBook.Name = book.Name;
            updateBook.Author = book.Author;
            updateBook.PublicationYear = book.PublicationYear;
            updateBook.Quantity = book.Quantity;
        }
        public void AddBorrow(int bookId, Borrow borrow)
        {
            var book = Data.FirstOrDefault(item => item.Id == bookId);
            if (book == null)
            {
                throw new Exception(Constant.DATA_NOT_FOUND);
            }
            if (book.Borrows == null)
            {
                book.Borrows = new List<Borrow>();
            }
            book.Borrows.Add(borrow);
            book.BorrowedQuantity++;
        }

    }
}
