﻿
using TrongHQ_MiniProject_QLTV.Models;
using TrongHQ_MiniProject_QLTV.UnitOfWork.Interfaces;

namespace TrongHQ_MiniProject_QLTV.Services.Interfaces
{
    public interface IBookServices
    {
        void AddBook(Book book);
        Book GetBookById(int id);
        List<Book> GetAll();
        List<Book> SearchBook(string keyword, int type);
        List<Book> PaginationBook(ref int page);
        void UpdateBook(int bookId, Book book);
        void DeleteBook(Book book);
    }
}
