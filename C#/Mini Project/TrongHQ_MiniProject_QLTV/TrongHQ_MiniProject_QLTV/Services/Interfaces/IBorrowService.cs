﻿
using TrongHQ_MiniProject_QLTV.Models;
using TrongHQ_MiniProject_QLTV.UnitOfWork.Interfaces;

namespace TrongHQ_MiniProject_QLTV.Services.Interfaces
{
    public interface IBorrowService
    {
        void AddBorrow(Borrow borrow);
        void ReturnBook(int borrowId);
        List<Borrow> GetAll();
        List<Borrow> GetBorrowListPagination(ref int page);
        List<Borrow> GetBorrowListSearch(string keyword, int type);
    }
}
