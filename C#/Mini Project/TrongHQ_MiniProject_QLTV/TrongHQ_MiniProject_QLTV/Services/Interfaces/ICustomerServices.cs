﻿
using TrongHQ_MiniProject_QLTV.Models;
using TrongHQ_MiniProject_QLTV.UnitOfWork.Interfaces;

namespace TrongHQ_MiniProject_QLTV.Services.Interfaces
{
    public interface ICustomerServices
    {
        void AddCustomer(Customer customer);
        List<Customer> GetAll();
        List<Customer> GetListCustomerPagination(ref int page);
        void UpdateCustomer(int id,Customer customer);
        void DeleteCustomer(Customer customer);
        Customer GetCustomerById(int id);
    }
}
