﻿
using TrongHQ_MiniProject_QLTV.Models;
using TrongHQ_MiniProject_QLTV.UnitOfWork.Interfaces;

namespace TrongHQ_MiniProject_QLTV.Services.Interfaces
{
    public interface IReviewServices
    {
        void AddReview(Review review);
        List<Review> ListReviewOfBook(int BookId);
    }
}
