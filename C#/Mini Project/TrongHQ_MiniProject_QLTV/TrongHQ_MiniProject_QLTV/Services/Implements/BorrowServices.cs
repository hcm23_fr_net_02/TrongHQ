﻿
using TrongHQ_MiniProject_QLTV.Common;
using TrongHQ_MiniProject_QLTV.Models;
using TrongHQ_MiniProject_QLTV.Services.Interfaces;
using TrongHQ_MiniProject_QLTV.UnitOfWork.Interfaces;

namespace TrongHQ_MiniProject_QLTV.Services.Implements
{
    public class BorrowServices : IBorrowService
    {
        private IUnitOfWork _unitOfWork;

        public BorrowServices(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void AddBorrow(Borrow borrow)
        {
            _unitOfWork.BorrowRepository.Insert(borrow);
            _unitOfWork.BookRepository.AddBorrow(borrow.Book.Id, borrow);
            _unitOfWork.CustomerRepository.AddBorrow(borrow.Customer.Id, borrow);
        }

        public List<Borrow> GetBorrowListPagination(ref int page)
        {
            var borrows = _unitOfWork.BorrowRepository.GetAll().Skip(page * Constant.PAGE_NUM).Take(Constant.PAGE_NUM).ToList();
            if (page < 0)
            {
                page = 0;
                throw new Exception(Constant.FIRST_PAGE);
            }

            if (borrows.Count == 0)
            {
                page--;
                throw new Exception(Constant.LAST_PAGE);
            }

            return borrows;

        }

        public List<Borrow> GetBorrowListSearch(string keyword, int type)
        {
            List<Borrow> borrows;
            List<Borrow> allBorrow = _unitOfWork.BorrowRepository.GetAll();
            switch (type)
            {
                case (int)Constant.SEARCH_BORROW.Book:
                    borrows = allBorrow.FindAll(borrow => borrow.Book.Name.Contains(keyword));
                    break;
                case (int)Constant.SEARCH_BORROW.Customer:
                    borrows = allBorrow.FindAll(borrow => borrow.Customer.Name.Contains(keyword));
                    break;
                default:
                    throw new Exception(Constant.SEARCH_TYPE_NOT_VALID);
            }
            if (borrows.Count == 0 || borrows == null)
            {
                throw new Exception(Constant.DATA_EMPTY);
            }
            return borrows;
        }

        public void ReturnBook(int borrowId)
        {
            _unitOfWork.BorrowRepository.ReturnBook(borrowId);
            var borrow = _unitOfWork.BorrowRepository.GetAll().Find(item => item.Id == borrowId);
            if (borrow == null) throw new Exception(Constant.DATA_NOT_FOUND);
            var book = _unitOfWork.BookRepository.GetAll().Where(item => item == borrow.Book).FirstOrDefault();
            var customer = _unitOfWork.CustomerRepository.GetAll().Where(item => item == borrow.Customer).FirstOrDefault();
            Console.WriteLine("first quantity: " + book.BorrowedQuantity);
            book.BorrowedQuantity--;
            Console.WriteLine("second quantity: " + book.BorrowedQuantity);
            var statusBorrowBook = book.Borrows.Find(item => item.Id == borrowId).Status;
            statusBorrowBook = true;
            var statusBorrowCustomer = customer.Borrows.Find(item => item.Id == borrowId).Status;
            statusBorrowCustomer = true;
        }

        public List<Borrow> GetAll()
        {
            return _unitOfWork.BorrowRepository.GetAll();
        }
    }
}
