﻿
using TrongHQ_MiniProject_QLTV.Common;
using TrongHQ_MiniProject_QLTV.Models;
using TrongHQ_MiniProject_QLTV.Services.Interfaces;
using TrongHQ_MiniProject_QLTV.UnitOfWork.Interfaces;

namespace TrongHQ_MiniProject_QLTV.Services.Implements
{
    public class BookServices : IBookServices
    {
        private IUnitOfWork _unitOfWork;

        public BookServices(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        public Book GetBookById(int Id)
        {
            return _unitOfWork.BookRepository.GetBookById(Id);
        }

        public void AddBook(Book book)
        {
            _unitOfWork.BookRepository.Insert(book);
        }

        public List<Book> PaginationBook(ref int page)
        {
            var books = _unitOfWork.BookRepository.GetAll().Skip(page * Constant.PAGE_NUM).Take(Constant.PAGE_NUM).ToList();
            if (page < 0)
            {
                page = 0;
                throw new Exception(Constant.FIRST_PAGE);
            }
            if (books.Count == 0) {
                page--;
                throw new Exception(Constant.LAST_PAGE);
            }

            return books;
        }

        public List<Book> SearchBook(string keyword, int type)
        {
            List<Book> books;
            List<Book> allBooks = _unitOfWork.BookRepository.GetAll();
            switch (type)
            {
                case (int)Constant.SEARCH_BOOK.Author:
                    books = allBooks.FindAll(book => book.Author.Contains(keyword));
                    break;
                case (int)Constant.SEARCH_BOOK.Genre:
                    books = allBooks.FindAll(book => book.Genre.Contains(keyword));
                    break;
                case (int)Constant.SEARCH_BOOK.Name:
                    books = allBooks.FindAll(book => book.Name.Contains(keyword));
                    break;
                case (int)Constant.SEARCH_BOOK.PublicationYear:
                    if (int.TryParse(keyword, out int publicationYear))
                    {
                        books = allBooks.FindAll(book => book.PublicationYear == publicationYear);
                    }
                    else
                    {
                        throw new Exception(Constant.DATA_NOT_VALID);
                    }
                    break;
                default:
                    throw new Exception(Constant.SEARCH_TYPE_NOT_VALID);
            }
            if (books.Count == 0 || books == null)
            {
                throw new Exception(Constant.DATA_EMPTY);
            }
            return books;
        }

        public void UpdateBook(int bookId, Book book)
        {
            _unitOfWork.BookRepository.UpdateBookInfo(bookId, book);
        }

        public void DeleteBook(Book book)
        {
            _unitOfWork.BookRepository.Delete(book);
            foreach (Review review in book.Reviews)
            {
                _unitOfWork.ReviewRepository.Delete(review);
            }
        }

        public List<Book> GetAll()
        {
            return _unitOfWork.BookRepository.GetAll();
        }
    }
}
