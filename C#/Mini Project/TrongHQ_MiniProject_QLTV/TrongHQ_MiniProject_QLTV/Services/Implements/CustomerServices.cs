﻿using TrongHQ_MiniProject_QLTV.Common;
using TrongHQ_MiniProject_QLTV.Models;
using TrongHQ_MiniProject_QLTV.Services.Interfaces;
using TrongHQ_MiniProject_QLTV.UnitOfWork.Interfaces;

namespace TrongHQ_MiniProject_QLTV.Services.Implements
{
    public class CustomerServices : ICustomerServices
    {
        private IUnitOfWork _unitOfWork;

        public CustomerServices(IUnitOfWork unitOfWork) {
            _unitOfWork = unitOfWork;
        }
        public void AddCustomer(Customer customer)
        {
            _unitOfWork.CustomerRepository.Insert(customer);
        }

        public void DeleteCustomer(Customer customer)
        {
            _unitOfWork.CustomerRepository.Delete(customer);
        }

        public List<Customer> GetListCustomerPagination(ref int page)
        {
            var customers = _unitOfWork.CustomerRepository.GetAll().Skip(page * Constant.PAGE_NUM).Take(Constant.PAGE_NUM).ToList().ToList();
            if(page < 0)
            {
                page = 0;
                throw new Exception(Constant.FIRST_PAGE);
            }
            if (customers.Count == 0 || customers == null)
            {
                page--;
                throw new Exception(Constant.LAST_PAGE);
            }
            return customers;
        }
        public List<Customer> GetAll()
        {
            return _unitOfWork.CustomerRepository.GetAll();
        }

        public void UpdateCustomer(int id, Customer customer)
        {
            _unitOfWork.CustomerRepository.UpdateUser(id, customer);
        }

        public Customer GetCustomerById(int id)
        {
            return _unitOfWork.CustomerRepository.GetCustomerById(id);
        }
    }
}
