﻿
using TrongHQ_MiniProject_QLTV.Common;
using TrongHQ_MiniProject_QLTV.Models;
using TrongHQ_MiniProject_QLTV.Services.Interfaces;
using TrongHQ_MiniProject_QLTV.UnitOfWork.Interfaces;

namespace TrongHQ_MiniProject_QLTV.Services.Implements
{
    public class ReviewServices : IReviewServices
    {
        private IUnitOfWork _unitOfWork;
        public ReviewServices(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void AddReview(Review review)
        {
            _unitOfWork.ReviewRepository.Insert(review);
            _unitOfWork.BookRepository.AddReview(review.Book.Id, review);
        }

        public List<Review> ListReviewOfBook(int bookId)
        {
            var reviews = _unitOfWork.ReviewRepository.GetAll().FindAll(review => review.Book.Id == bookId);
            if (reviews.Count == 0)
                throw new Exception(Constant.DATA_EMPTY);
            return reviews;
        }
    }
}
