﻿
using Newtonsoft.Json;
using TrongHQ_MiniProject_QLTV.Models;
using TrongHQ_MiniProject_QLTV.Repositories.Interfaces;
using TrongHQ_MiniProject_QLTV.UnitOfWork.Interfaces;

namespace TrongHQ_MiniProject_QLTV.DataAccess
{
    public class SeedingJsonData
    {
        static string jsonPath = AppDomain.CurrentDomain.BaseDirectory.Split("bin").First() + "Data\\";

        public static void SeedData(IUnitOfWork unitOfWork, bool isTest = false)
        {
            SeedCustomers(unitOfWork.CustomerRepository);
            SeedBooks(unitOfWork.BookRepository, unitOfWork.ReviewRepository);
            SeedBorrow(unitOfWork.BookRepository, unitOfWork.CustomerRepository, unitOfWork.BorrowRepository);
        }

        public static void SeedBorrow(IBookRepository book, ICustomerRepository customer, IBorrowRepository borrowRepo)
        {
            string customerJson = File.ReadAllText(jsonPath + "borrow.json");
            var borrows = JsonConvert.DeserializeObject<List<Borrow>>(customerJson);
            var books = book.GetAll();
            var customers = customer.GetAll();
            Book getBook;
            Customer getCustomer;
            var random = new Random();

            foreach (var borrow in borrows)
            {
                do
                {
                    getBook = books.ElementAt(random.Next(books.Count() - 1));
                    if (getBook.PublicationYear > borrow.StartDate.Year)
                        continue;
                    getCustomer = customers.ElementAt(random.Next(customers.Count() - 1));
                    break;
                } while (true);
                var newBorrow = new Borrow(borrow.Id,getCustomer, getBook, false, borrow.StartDate, borrow.DueDate);
                books.ElementAt(getBook.Id - 1).Borrows.Add(newBorrow);
                customers.ElementAt(getCustomer.Id - 1).Borrows.Add(newBorrow);
                borrowRepo.Insert(newBorrow);
            }
        }

        public static void SeedCustomers(ICustomerRepository customerRepository)
        {
            string customerJson = File.ReadAllText(jsonPath + "customer.json");
            var customers = JsonConvert.DeserializeObject<List<Customer>>(customerJson);

            foreach (var customer in customers)
            {
                customer.Borrows = new List<Borrow>();
                customerRepository.Insert(customer);
            }
        }

        public static void SeedBooks(IBookRepository bookRepository, IReviewRepository reviewRepository)
        {
            string bookJson = File.ReadAllText(jsonPath + "book.json");
            var books = JsonConvert.DeserializeObject<List<Book>>(bookJson);
            int reviewId = 0;
            foreach (var book in books)
            {
                book.BorrowedQuantity = 0;
                book.Borrows = new List<Borrow>();
                var listReview = new List<Review>();
                foreach (var review in book.Reviews)
                {
                    var newReview = new Review(review.Id ,book, review.Title, review.Descriptions, review.Rating);
                    reviewRepository.Insert(newReview);
                    listReview.Add(newReview);
                    reviewId++;
                }
                book.Reviews = listReview;
                bookRepository.Insert(book);
            }
        }
    }
}
