﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrongHQ_MiniProject_QLTV.Common;

namespace TrongHQ_MiniProject_QLTV.Models
{
    public class Customer
    {
        private int _id;
        private string _name;
        private string _address;
        private List<Borrow> _borrows;
        private static int _idCount = 1;

        public int Id
        {
            get { return _id; }
            private set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception(Constant.DATA_INPUT_EMPTY);
                _name = value;
            }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public List<Borrow> Borrows
        {
            get { return _borrows; }
            set { _borrows = value; }
        }

        public Customer(string name, string address, List<Borrow> borrows)
        {
            Id = _idCount++;
            Name = name;
            Address = address;
            Borrows = borrows;
        }

        [JsonConstructor]
        public Customer(int id, string name, string address, List<Borrow> borrows)
        {
            Id = id;
            Name = name;
            Address = address;
            Borrows = borrows;
            _idCount = ++id;
        }

        public Customer()
        {
        }

    }
}
