﻿
using TrongHQ_MiniProject_QLTV.Common;
using Newtonsoft.Json;

namespace TrongHQ_MiniProject_QLTV.Models
{
    public class Book
    {
        private int _id;
        private int _quantity;
        private int _borrowedQuantity;
        private List<Review> _reviews;
        private List<Borrow> _borrows;
        private string _name;
        private string _author;
        private int _publicationYear;
        private string _genre;
        private static int _idCount = 1;

        public int Id
        {
            get { return _id; }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception(Constant.DATA_INPUT_EMPTY);
                _name = value;
            }
        }

        public string Author
        {
            get { return _author; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception(Constant.DATA_INPUT_EMPTY);
                _author = value;
            }
        }

        public int PublicationYear
        {
            get { return _publicationYear; }
            set
            {
                if (value > DateTime.Now.Year || value < 1000)
                    throw new Exception(Constant.DATE_NOT_VALID);
                _publicationYear = value;
            }
        }

        public string Genre
        {
            get { return _genre; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception(Constant.DATA_INPUT_EMPTY);
                _genre = value;
            }
        }

        public int Quantity
        {
            get { return _quantity; }
            set
            {
                if (value < 0)
                    throw new Exception(Constant.QUANTITY_CONSTRAINT);
                _quantity = value;
            }
        }

        public int BorrowedQuantity
        {
            get { return _borrowedQuantity; }
            set
            {
                if (value > Quantity)
                    throw new ArgumentException(Constant.BORROWED_QUANTITY_CONSTRAINT);
                _borrowedQuantity = value;

            }
        }

        public List<Review> Reviews
        {
            get { return _reviews; }
            set { _reviews = value; }
        }

        public List<Borrow> Borrows
        {
            get { return _borrows; }
            set { _borrows = value; }
        }
        public Book(string name, string author, int publicationYear, string genre,
            int quantity, int borrowedQuantity, List<Review> reviews, List<Borrow> borrows)
        {
            _id = _idCount++;
            Name = name;
            Author = author;
            PublicationYear = publicationYear;
            Genre = genre;
            Quantity = quantity;
            BorrowedQuantity = borrowedQuantity;
            Reviews = reviews;
            Borrows = borrows;
        }

        [JsonConstructor]
        public Book(int id, string name, string author, int publicationYear, string genre,
            int quantity, int borrowedQuantity, List<Review> reviews, List<Borrow> borrows)
        {
            _id = id;
            Name = name;
            Author = author;
            PublicationYear = publicationYear;
            Genre = genre;
            Quantity = quantity;
            BorrowedQuantity = borrowedQuantity;
            Reviews = reviews;
            Borrows = borrows;
            _idCount = ++id;
        }

        public Book() { }
    }
}
