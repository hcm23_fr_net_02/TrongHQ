﻿
using Newtonsoft.Json;
using TrongHQ_MiniProject_QLTV.Common;

namespace TrongHQ_MiniProject_QLTV.Models
{
    public class Borrow
    {
        private int _id;
        private Customer _customer;
        private Book _book;
        private bool _status;
        private DateTime _startDate;
        private DateTime _dueDate;
        private static int _idCount = 1;

        public int Id
        {
            get { return _id; }
        }

        public Customer Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }

        public Book Book
        {
            get { return _book; }
            set { _book = value; }
        }

        public bool Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public DateTime StartDate
        {
            get { return _startDate; }
            set
            {
                if (_book != null)
                {
                    if (value.Year < _book.PublicationYear)
                        throw new Exception(Constant.DATE_NOT_VALID);
                }
                _startDate = value;
            }
        }

        public DateTime DueDate
        {
            get { return _dueDate; }
            set
            {
                if (value < _startDate)
                    throw new Exception(Constant.DATE_NOT_VALID);
                _dueDate = value;
            }
        }

        public Borrow(Customer customer, Book book, bool status, DateTime startDate, DateTime dueDate)
        {
            _id = _idCount++;
            Customer = customer;
            Book = book;
            Status = status;
            StartDate = startDate;
            DueDate = dueDate;
        }

        [JsonConstructor]
        public Borrow(int id, Customer customer, Book book, bool status, DateTime startDate, DateTime dueDate)
        {
            _id = id;
            Customer = customer;
            Book = book;
            Status = status;
            StartDate = startDate;
            DueDate = dueDate;
            _idCount = ++id;
        }
        public Borrow() { }
    }
}
