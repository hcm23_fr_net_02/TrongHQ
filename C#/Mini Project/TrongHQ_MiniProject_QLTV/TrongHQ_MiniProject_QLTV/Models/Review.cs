﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrongHQ_MiniProject_QLTV.Common;

namespace TrongHQ_MiniProject_QLTV.Models
{
    public class Review
    {
        private static int _idCount = 1;
        private int _id;
        private Book _book;
        private string _title;
        private string _descriptions;
        private int _rating;

        public int Id
        {
            get { return _id; }
        }

        public Book Book
        {
            get { return _book; }
            set { _book = value; }
        }

        public string Title
        {
            get { return _title; }
            set {
                if (string.IsNullOrEmpty(value))
                    throw new Exception(Constant.DATA_INPUT_EMPTY);
                _title = value;
            }
        }

        public string Descriptions
        {
            get { return _descriptions; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception(Constant.DATA_INPUT_EMPTY);
                _descriptions = value;
            }
        }

        public int Rating
        {
            get { return _rating; }
            set
            {
                if (value < 1 || value > 5)
                    throw new Exception();
                _rating = value;
            }
        }

        public Review(Book book, string title, string descriptions, int rating)
        {
            _id = _idCount++;
            Book = book;
            Title = title;
            Descriptions = descriptions;
            Rating = rating; // Uses the property with validation
        }

        [JsonConstructor]
        public Review(int id, Book book, string title, string descriptions, int rating)
        {
            _id = id;
            Book = book;
            Title = title;
            Descriptions = descriptions;
            Rating = rating; // Uses the property with validation
            _idCount = id + 1;
        }

        public Review()
        {
        }
    }
}
