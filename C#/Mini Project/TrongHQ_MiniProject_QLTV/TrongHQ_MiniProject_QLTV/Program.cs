﻿// See https://aka.ms/new-console-template for more information
using Microsoft.Extensions.DependencyInjection;
using TrongHQ_MiniProject_QLTV.Helper;
using TrongHQ_MiniProject_QLTV.Presenter.Interfaces;


IServiceProvider service = new ServiceCollection().ConfigureDependencyInjection().BuildServiceProvider();
var mainPresenterService = service.GetService<IMainPresenter>();
mainPresenterService.Start();