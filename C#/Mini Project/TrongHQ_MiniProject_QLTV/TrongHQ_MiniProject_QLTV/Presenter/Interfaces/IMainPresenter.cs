﻿
using TrongHQ_MiniProject_QLTV.Models;

namespace TrongHQ_MiniProject_QLTV.Presenter.Interfaces
{
    public interface IMainPresenter
    {
        void Start();
        void SeedData();
        void MainMenu();
        void MenuViewBook();
        void ViewBooks(ref int page);
        void MenuViewBookDetail(Book book);
        Book EditBook(int id, Book book);
        bool Delete(Book book);
        void AddReview(Book book);
        void ViewAllReview(Book book);
        void Borrow(Book book);
        void AddBook();
        void SearchBook();
        void ViewBorrows(ref int page);
        void MenuBorrowDetail();
        void SearchBorrow();
        void MenuCustomers();
        Customer SearchCustomers();
        void ViewListCustomer(ref int page);
        Customer AddCustomer();
        void MenuCustomerDetail(Customer customer);
        Customer UpdateCustomer(int id, Customer customer);
        bool DeleteCustomer(Customer customer);
    }
}
