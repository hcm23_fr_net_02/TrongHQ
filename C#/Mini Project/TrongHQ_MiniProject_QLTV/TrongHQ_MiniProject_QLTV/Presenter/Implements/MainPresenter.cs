﻿using TrongHQ_MiniProject_QLTV.Presenter.Interfaces;
using TrongHQ_MiniProject_QLTV.Services.Interfaces;
using TrongHQ_MiniProject_QLTV.UnitOfWork.Interfaces;
using TrongHQ_MiniProject_QLTV.Common;
using System.Text;
using TrongHQ_MiniProject_QLTV.DataAccess;
using TrongHQ_MiniProject_QLTV.Models;
using System.Globalization;

namespace TrongHQ_MiniProject_QLTV.Presenter.Implements
{

    public class MainPresenter : IMainPresenter
    {
        private IUnitOfWork _unitOfWork;
        private ICustomerServices _customerServices;
        private IBookServices _bookServices;
        private IReviewServices _reviewServices;
        private IBorrowService _borrowService;
        public MainPresenter(IUnitOfWork unitOfWork,
            ICustomerServices customerServices,
            IBookServices bookServices,
            IReviewServices reviewServices,
            IBorrowService borrowService)
        {
            _unitOfWork = unitOfWork;
            _customerServices = customerServices;
            _bookServices = bookServices;
            _reviewServices = reviewServices;
            _borrowService = borrowService;
        }
        public void Start()
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            SeedData();
            MainMenu();
        }
        public void SeedData()
        {
            Console.Write(Constant.MESSAGE_GETTING_DATA);
            SeedingJsonData.SeedData(_unitOfWork);
            Console.Write(Constant.MESSAGE_SEED_DATA);
        }
        public void MainMenu()
        {
            while (true)
            {
                Console.Write(Constant.MAIN_MENU);
                Console.Write(Constant.MESSAGE_CHOICE);
                try
                {
                    if (int.TryParse(Console.ReadLine(), out int choice))
                    {
                        switch (choice)
                        {
                            case 1:
                                MenuViewBook();
                                break;
                            case 2:
                                AddBook();
                                break;
                            case 3:
                                SearchBook();
                                break;
                            case 4:
                                MenuBorrowDetail();
                                break;
                            case 5:
                                MenuCustomers();
                                break;
                            case 6:
                                Console.WriteLine(Constant.MESSAGE_END);
                                return;
                            default:
                                throw new Exception(Constant.DATA_NOT_VALID);
                        }
                    }
                    else
                    {
                        throw new Exception(Constant.DATA_NOT_VALID);
                    }
                }
                catch (Exception e)
                {
                    Console.Clear();
                    Console.WriteLine(e.Message);
                }
            }
        }

        public void AddBook()
        {
            Console.Write(Constant.MESSAGE_UPDATE_BOOK_TITLE);
            string title = Console.ReadLine();
            Console.Write(Constant.MESSAGE_UPDATE_BOOK_AUTHOR);
            string author = Console.ReadLine();
            Console.Write(Constant.MESSAGE_UPDATE_BOOK_GENRE);
            string genre = Console.ReadLine();
            Console.Write(Constant.MESSAGE_UPDATE_BOOK_PUBLICATION_YEAR);
            int publicationYear;
            if (!int.TryParse(Console.ReadLine(), out publicationYear))
            {
                throw new Exception(Constant.DATA_NOT_VALID);
            }
            Console.Write(Constant.MESSAGE_UPDATE_BOOK_QUANTITY);
            int quantity;
            if (!int.TryParse(Console.ReadLine(), out quantity))
            {
                throw new Exception(Constant.DATA_NOT_VALID);
            }

            _bookServices.AddBook(new Book(title, author, publicationYear, genre, quantity,
                0, new List<Review>(), new List<Borrow>()));
            Console.Clear();
            Console.WriteLine(Constant.MESSAGE_ADD_BOOK_SUCCESS);
        }

        public Customer AddCustomer()
        {
            Console.Write(Constant.MESSAGE_CUSTOMER_NAME);
            string name = Console.ReadLine();
            Console.Write(Constant.MESSAGE_CUSTOMER_ADDRESS);
            string address = Console.ReadLine();
            Customer customer = new Customer(name, address, new List<Borrow>());
            _customerServices.AddCustomer(customer);
            return customer;

        }

        public void AddReview(Book book)
        {
            Console.Write(Constant.MESSAGE_REVIEW_TITLE);
            string title = Console.ReadLine();
            Console.Write(Constant.MESSAGE_REVIEW_CONTENT);
            string content = Console.ReadLine();
            Console.Write(Constant.MESSAGE_REVIEW_RATING);
            if (int.TryParse(Console.ReadLine(), out int rating))
            {
                if (rating < 1 || rating > 5)
                {
                    throw new Exception(Constant.DATA_NOT_VALID);
                }
                _reviewServices.AddReview(new Review(book, title, content, rating));
                Console.Clear();
                Console.Write(Constant.MESSAGE_REVIEW_SUCCESS);
            }
            else
            {
                throw new Exception(Constant.DATA_NOT_VALID);
            }
        }

        public void Borrow(Book book)
        {
            DateTime startDate;
            DateTime dueDate;
            CultureInfo culture = new CultureInfo("vi-VN");
            Console.WriteLine("Nhập thông tin ngày mượn");
            if (DateTime.TryParseExact(Console.ReadLine(), "d/M/yyyy", culture, DateTimeStyles.None, out startDate))
            {
                Console.WriteLine("Nhập thông tin ngày hết hạn");
                if (DateTime.TryParseExact(Console.ReadLine(), "d/M/yyyy", culture, DateTimeStyles.None, out dueDate))
                {
                    if (dueDate <= startDate || dueDate < DateTime.Now)
                    {
                        throw new Exception(Constant.DATE_NOT_VALID);
                    }
                    Console.Write(Constant.SUB_MENU_BORROW_CUSTOMER);
                    Console.Write(Constant.MESSAGE_CHOICE);
                    string choice = Console.ReadLine();
                    Customer customer = new Customer();
                    switch (choice)
                    {
                        case "1":
                            AddCustomer();
                            customer = _customerServices.GetAll().LastOrDefault();
                            break;
                        case "2":
                            bool isSearching = true;
                            while (isSearching)
                            {
                                customer = SearchCustomers();
                                Console.WriteLine(customer.Id + " || " + customer.Name + " || " + customer.Address);
                                Console.Write(Constant.MESSAGE_CUSTOMER_CONFIRMATION);
                                switch (Console.ReadLine())
                                {
                                    case "Y":
                                    case "y":
                                        isSearching = false;
                                        break;
                                    case "N":
                                    case "n":
                                        break;
                                    default:
                                        throw new Exception(Constant.DATA_NOT_VALID);
                                }
                            }
                            break;
                        default:
                            throw new Exception(Constant.DATA_NOT_VALID);
                    }

                    Borrow borrow = new Borrow(customer, book, false, startDate, dueDate);
                    _borrowService.AddBorrow(borrow);
                    Console.Clear();
                    Console.Write(Constant.MESSAGE_BORROW_SUCCESS);
                }
                else
                {
                    throw new Exception(Constant.DATA_NOT_VALID);
                }
            }
            else
            {
                throw new Exception(Constant.DATA_NOT_VALID);
            }
        }

        public bool Delete(Book book)
        {
            Console.Write(Constant.MESSAGE_DELETE_CONFIRMATION);
            string confirmation = Console.ReadLine();
            switch (confirmation)
            {
                case "Y":
                case "y":
                    _bookServices.DeleteBook(book);
                    Console.Clear();
                    Console.WriteLine(Constant.MESSAGE_DELETE_SUCCESS);
                    return true;
                case "N":
                case "n":
                    return false;
                default:
                    throw new Exception(Constant.DATA_NOT_VALID);
            }
        }

        public bool DeleteCustomer(Customer customer)
        {
            Console.Write(Constant.MESSAGE_DELETE_CONFIRMATION);
            string confirmation = Console.ReadLine();
            switch (confirmation)
            {
                case "Y":
                case "y":
                    _customerServices.DeleteCustomer(customer);
                    Console.Clear();
                    Console.WriteLine(Constant.MESSAGE_DELETE_SUCCESS);
                    return true;
                case "N":
                case "n":
                    return false;
                default:
                    throw new Exception(Constant.DATA_NOT_VALID);
            }
        }

        public Book EditBook(int id,Book book)
        {
            bool isChanged = false;
            Console.Clear();
            while (true)
            {
                Console.Write(Constant.MESSAGE_LIST_BOOK);
                Console.Write(Constant.MESSAGE_SLASH);
                Console.WriteLine(id + " || " + book.Name + " || " + book.Author + " || " + book.Genre + " || " + book.PublicationYear + " || " + book.Quantity);
                try
                {
                    Console.Write(Constant.SUB_MENU_EDIT_BOOK);
                    string choice;
                    switch (choice = Console.ReadLine())
                    {
                        case "1":
                            Console.Write(Constant.MESSAGE_UPDATE_BOOK_TITLE);
                            string title = Console.ReadLine();
                            if (book.Name == title)
                                throw new Exception(Constant.DATA_DUPLICATED);
                            book.Name = title;
                            isChanged = true;
                            Console.Clear();
                            break;
                        case "2":
                            Console.Write(Constant.MESSAGE_UPDATE_BOOK_AUTHOR);
                            string author = Console.ReadLine();
                            if (book.Author == author)
                                throw new Exception(Constant.DATA_DUPLICATED);
                            book.Author = author;
                            isChanged = true;
                            Console.Clear();
                            break;
                        case "3":
                            Console.Write(Constant.MESSAGE_UPDATE_BOOK_PUBLICATION_YEAR);
                            if (int.TryParse(Console.ReadLine(), out int publicationYear))
                            {
                                if (book.PublicationYear == publicationYear)
                                    throw new Exception(Constant.DATA_DUPLICATED);
                                book.PublicationYear = publicationYear;
                                isChanged = true;
                            }
                            else
                            {
                                throw new Exception(Constant.DATA_NOT_VALID);
                            }
                            Console.Clear();
                            break;
                        case "4":
                            Console.Write(Constant.MESSAGE_UPDATE_BOOK_QUANTITY);
                            if (int.TryParse(Console.ReadLine(), out int quantity))
                            {
                                if (book.Quantity == quantity)
                                    throw new Exception(Constant.DATA_DUPLICATED);
                                book.Quantity = quantity;
                                isChanged = true;
                            }
                            else
                            {
                                throw new Exception(Constant.DATA_NOT_VALID);
                            }
                            Console.Clear();
                            break;
                        case "5":
                            Console.Clear();
                            if (!isChanged)
                            {
                                return null;
                            }
                            return book;
                        default:
                            throw new Exception(Constant.DATA_NOT_VALID);
                    }
                }
                catch (Exception e)
                {
                    Console.Clear();
                    Console.WriteLine(e.Message);
                }
            }
        }

        public void MenuBorrowDetail()
        {
            int page = 0;
            Console.Clear();
            if (_borrowService.GetAll().Where(borrow => borrow.Status == false).Count() == 0)
            {
                throw new Exception(Constant.DATA_EMPTY);
            }
            while (true)
            {
                try
                {
                    ViewBorrows(ref page);
                    Console.Write(Constant.SUB_MENU_BORROW_BOOK);
                    Console.Write(Constant.MESSAGE_CHOICE);
                    string choice;
                    switch (choice = Console.ReadLine())
                    {
                        case "N":
                        case "n":
                            page++;
                            Console.Clear();
                            break;
                        case "P":
                        case "p":
                            page--;
                            Console.Clear();
                            break;
                        case "B":
                        case "b":
                            Console.Clear();
                            return;
                        case "I":
                        case "i":
                            Console.Write(Constant.MESSAGE_FIND_BORROW_BY_ID);
                            if (int.TryParse(Console.ReadLine(), out int idBorrow))
                            {
                                _borrowService.ReturnBook(idBorrow);
                                Console.Clear();
                                Console.WriteLine(Constant.MESSAGE_BORROW_RETURN);
                                if (_borrowService.GetAll().Where(borrow => borrow.Status == false).Count() == 0)
                                {
                                    Console.WriteLine(Constant.DATA_EMPTY);
                                    return;
                                }
                            }
                            else
                            {
                                throw new Exception(Constant.DATA_NOT_VALID);
                            }
                            break;
                        case "S":
                        case "s":
                            SearchBorrow();
                            break;
                        default:
                            throw new Exception(Constant.DATA_NOT_VALID);
                    }
                }
                catch (Exception e)
                {
                    Console.Clear();
                    Console.WriteLine(e.Message);
                }
            }
        }

        public void MenuCustomerDetail(Customer customer)
        {
            while (true)
            {
                try
                {
                    Console.Write(Constant.MESSAGE_LIST_CUSTOMER);
                    Console.Write(Constant.MESSAGE_SLASH);
                    Console.WriteLine(customer.Id + " || " + customer.Name + " || " + customer.Address);
                    Console.Write(Constant.SUB_MENU_CUSTOMER_DETAIL);
                    Console.Write(Constant.MESSAGE_CHOICE);
                    string choice;
                    switch (choice = Console.ReadLine())
                    {
                        case "1":
                            Customer editCustomer = UpdateCustomer(customer.Id, new Customer() { Name = customer.Name, Address = customer.Address});
                            if (editCustomer == null)
                                throw new Exception(Constant.MESSAGE_UPDATE_BOOK_CHANGE_FAIL);
                            _customerServices.UpdateCustomer(customer.Id ,editCustomer);
                            Console.Clear();
                            break;
                        case "2":
                            if (DeleteCustomer(customer))
                                return;
                            Console.Clear();
                            break;
                        case "3":
                            Console.Clear();
                            return;
                        default:
                            throw new Exception(Constant.DATA_NOT_VALID);
                    }
                }
                catch (Exception e)
                {
                    Console.Clear();
                    Console.WriteLine(e.Message);
                }
            }
        }

        public void MenuCustomers()
        {
            Console.Clear();
            int page = 0;
            while (true)
            {
                try
                {
                    ViewListCustomer(ref page);
                    Console.Write(Constant.SUB_MENU_MANAGE_CUSTOMER);
                    Console.Write(Constant.MESSAGE_CHOICE);
                    string choice;
                    switch (choice = Console.ReadLine())
                    {
                        case "N":
                        case "n":
                            page++;
                            Console.Clear();
                            break;
                        case "P":
                        case "p":
                            page--;
                            Console.Clear();
                            break;
                        case "B":
                        case "b":
                            Console.Clear();
                            return;
                        case "F":
                        case "f":
                            Console.Write(Constant.MESSAGE_FIND_CUSTOMER_BY_ID);
                            if (int.TryParse(Console.ReadLine(), out int id))
                            {
                                var customer = _customerServices.GetCustomerById(id);
                                Console.Clear();
                                MenuCustomerDetail(customer);
                            }
                            else
                            {
                                throw new Exception(Constant.DATA_NOT_VALID);
                            }
                            break;
                        case "A":
                        case "a":
                            Customer customerAdd = AddCustomer();
                            Console.Clear();
                            if (customerAdd != null)
                            {
                                Console.WriteLine(Constant.MESSAGE_ADD_CUSTOMER_SUCCESS);
                                Console.WriteLine(customerAdd.Id + " || " + customerAdd.Name + " || " + customerAdd.Address);
                                Console.Write(Constant.MESSAGE_SLASH);
                            }
                            break;
                        default:
                            throw new Exception(Constant.DATA_NOT_VALID);

                    }
                }
                catch (Exception e)
                {
                    Console.Clear();
                    Console.WriteLine(e.Message);
                }
            }
        }

        public void MenuViewBook()
        {
            Console.Clear();
            int page = 0;
            while (true)
            {
                try
                {
                    ViewBooks(ref page);
                    Console.Write(Constant.SUB_MENU_VIEW_BOOK);
                    Console.Write(Constant.MESSAGE_CHOICE);
                    string choice;
                    switch (choice = Console.ReadLine())
                    {
                        case "N":
                        case "n":
                            page++;
                            Console.Clear();
                            break;
                        case "P":
                        case "p":
                            page--;
                            Console.Clear();
                            break;
                        case "B":
                        case "b":
                            Console.Clear();
                            return;
                        case "F":
                        case "f":
                            Console.Write(Constant.MESSAGE_FIND_BOOK_BY_ID);
                            if (int.TryParse(Console.ReadLine(), out int id))
                            {
                                var book = _bookServices.GetBookById(id);
                                Console.Clear();
                                MenuViewBookDetail(book);
                            }
                            else
                            {
                                throw new Exception(Constant.DATA_NOT_VALID);
                            }
                            break;
                        default:
                            throw new Exception(Constant.DATA_NOT_VALID);

                    }
                }
                catch (Exception e)
                {
                    Console.Clear();
                    Console.WriteLine(e.Message);
                }
            }
        }

        public void MenuViewBookDetail(Book book)
        {
            while (true)
            {
                try
                {
                    Console.Write(Constant.MESSAGE_LIST_BOOK);
                    Console.Write(Constant.MESSAGE_SLASH);
                    Console.WriteLine(book.Id + " || " + book.Name + " || " + book.Author + " || " + book.Genre + " || " + book.PublicationYear + " || " + book.Quantity);
                    Console.Write(Constant.SUB_MENU_BOOK_DETAIL);
                    Console.Write(Constant.MESSAGE_CHOICE);
                    string choice;
                    switch (choice = Console.ReadLine())
                    {
                        case "1":
                            Book editBook = EditBook(book.Id,new Book()
                            { 
                                Name = book.Name,
                                Author = book.Author,
                                Quantity = book.Quantity,
                                PublicationYear = book.PublicationYear,
                                Genre = book.Genre
                            });
                            if (editBook == null)
                                throw new Exception(Constant.MESSAGE_UPDATE_BOOK_CHANGE_FAIL);
                            _bookServices.UpdateBook(book.Id, editBook);
                            Console.Clear();
                            break;
                        case "2":
                            if (book.BorrowedQuantity > 0)
                                throw new Exception(Constant.MESSAGE_DELETE_BORROWED);
                            if (Delete(book))
                                return;
                            Console.Clear();
                            break;
                        case "3":
                            AddReview(book);
                            break;
                        case "4":
                            ViewAllReview(book);
                            Console.Clear();
                            break;
                        case "5":
                            Borrow(book);
                            break;
                        case "6":
                            Console.Clear();
                            return;
                        default:
                            throw new Exception(Constant.DATA_NOT_VALID);
                    }
                }
                catch (Exception e)
                {
                    Console.Clear();
                    Console.WriteLine(e.Message);
                }
            }
        }

        public void SearchBook()
        {
            Console.Clear();
            while (true)
            {
                try
                {
                    Console.Write(Constant.SUB_MENU_SEARCH_BOOK);
                    Console.Write(Constant.MESSAGE_CHOICE);
                    if (int.TryParse(Console.ReadLine(), out int choice))
                    {
                        if (choice < 0 || choice > 5)
                        {
                            throw new Exception(Constant.SEARCH_TYPE_NOT_VALID);
                        }
                        if (choice == 5)
                        {
                            Console.Clear();
                            return;
                        }
                        Console.Write(Constant.MESSAGE_BOOK_SEARCH_KEYWORD);
                        string keyword = Console.ReadLine();
                        var books = _bookServices.SearchBook(keyword, choice);
                        if (books.Count == 0)
                            throw new Exception(Constant.DATA_EMPTY);
                        Console.Clear();
                        Console.Write(Constant.MESSAGE_LIST_BOOK);
                        Console.Write(Constant.MESSAGE_SLASH);
                        foreach (var book in books)
                        {
                            int bookLeft = book.Quantity - book.BorrowedQuantity;
                            Console.WriteLine(book.Id + " || " + book.Name + " || " + book.Author + " || " + book.Genre + " || " + book.PublicationYear + " || " + bookLeft);
                        }
                    }
                    else
                    {
                        throw new Exception(Constant.DATA_NOT_VALID);
                    }
                }
                catch (Exception e)
                {
                    Console.Clear();
                    Console.WriteLine(e.Message);
                }
            }
        }

        public void SearchBorrow()
        {
            Console.Write(Constant.SUB_MENU_SEARCH_BORROW);
            if (int.TryParse(Console.ReadLine(), out int type))
            {
                if (type < 0 || type > 3)
                {
                    throw new Exception(Constant.DATA_NOT_VALID);
                }
                if (type == 3)
                    return;
                Console.Write(Constant.MESSAGE_BOOK_SEARCH_KEYWORD);
                string keyword = Console.ReadLine();
                var borrows = _borrowService.GetBorrowListSearch(keyword, type);
                if (borrows.Count == 0 || borrows == null)
                {
                    throw new Exception(Constant.DATA_EMPTY);
                }
                Console.Clear();
                Console.Write(Constant.MESSAGE_LIST_BORROW);
                Console.Write(Constant.MESSAGE_SLASH);
                foreach (var borrow in borrows)
                {
                    string status = borrow.Status ? "Đã trả sách" : "Chưa trả sách";
                    Console.WriteLine(borrow.Id + " || " + borrow.Book.Id + " || " + borrow.Book.Name + " || " + borrow.Customer.Id + " || " + borrow.Customer.Name + " || " + borrow.StartDate.ToString("d/M/yyyy") + " || " + borrow.DueDate.ToString("d/M/yyyy") + " || " + status);
                }
                Console.Write(Constant.MESSAGE_SLASH);
            }
            else
            {
                throw new Exception(Constant.DATA_NOT_VALID);
            }
        }

        public Customer UpdateCustomer(int id, Customer customer)
        {
            bool isChanged = false;
            Console.Clear();
            while (true)
            {
                Console.Write(Constant.MESSAGE_LIST_CUSTOMER);
                Console.Write(Constant.MESSAGE_SLASH);
                Console.WriteLine(id + " || " + customer.Name + " || " + customer.Address);
                try
                {
                    Console.Write(Constant.SUB_MENU_CUSTOMER_UPDATE);
                    string choice;
                    switch (choice = Console.ReadLine())
                    {
                        case "1":
                            Console.Write(Constant.MESSAGE_UPDATE_BOOK_TITLE);
                            string name = Console.ReadLine();
                            if (customer.Name == name)
                                throw new Exception(Constant.DATA_DUPLICATED);
                            customer.Name = name;
                            isChanged = true;
                            Console.Clear();
                            break;
                        case "2":
                            Console.Write(Constant.MESSAGE_UPDATE_BOOK_AUTHOR);
                            string address = Console.ReadLine();
                            if (customer.Address == address)
                                throw new Exception(Constant.DATA_DUPLICATED);
                            customer.Address = address;
                            isChanged = true;
                            Console.Clear();
                            break;
                        case "3":
                            Console.Clear();
                            if (!isChanged)
                            {
                                return null;
                            }
                            return customer;
                        default:
                            throw new Exception(Constant.DATA_NOT_VALID);
                    }
                }
                catch (Exception e)
                {
                    Console.Clear();
                    Console.WriteLine(e.Message);
                }
            }
        }

        public void ViewAllReview(Book book)
        {
            Console.Clear();
            if (book.Reviews == null || book.Reviews.Count == 0)
                throw new Exception(Constant.DATA_EMPTY);
            foreach (var review in book.Reviews)
            {
                Console.Write(Constant.MESSAGE_SLASH);
                Console.WriteLine(Constant.MESSAGE_ID + review.Id);
                Console.WriteLine(Constant.MESSAGE_TITLE + review.Title);
                Console.WriteLine(Constant.MESSAGE_CONTENT + review.Descriptions);
                Console.WriteLine(Constant.MESSAGE_RATING + review.Rating);
                Console.Write(Constant.MESSAGE_SLASH);
            }
            Console.WriteLine(Constant.MESSAGE_PRESS_ANYTHING);
            Console.ReadKey();
        }

        public void ViewBooks(ref int page)
        {
            var listBook = _bookServices.PaginationBook(ref page);
            Console.Write(Constant.MESSAGE_LIST_BOOK);
            foreach (var book in listBook)
            {
                int bookLeft = book.Quantity - book.BorrowedQuantity;
                Console.WriteLine(book.Id + " || " + book.Name + " || " + book.Author + " || " + book.Genre + " || " + book.PublicationYear + " || " + bookLeft);
            }
        }

        public void ViewBorrows(ref int page)
        {
            var listBorrows = _borrowService.GetBorrowListPagination(ref page);
            Console.Write(Constant.MESSAGE_LIST_BORROW);
            Console.Write(Constant.MESSAGE_SLASH);
            foreach (var borrow in listBorrows)
            {
                string status = borrow.Status ? "Đã trả sách" : "Chưa trả sách";
                Console.WriteLine(borrow.Id + " || " + borrow.Book.Id + " || " + borrow.Book.Name + " || " + borrow.Customer.Id + " || " + borrow.Customer.Name + " || " + borrow.StartDate.ToString("d/M/yyyy") + " || " + borrow.DueDate.ToString("d/M/yyyy") + " || " + status);
            }
        }

        public void ViewListCustomer(ref int page)
        {
            var listCustomer = _customerServices.GetListCustomerPagination(ref page);
            Console.Write(Constant.MESSAGE_LIST_CUSTOMER);
            Console.Write(Constant.MESSAGE_SLASH);
            foreach (var customer in listCustomer)
            {
                Console.WriteLine(customer.Id + " || " + customer.Name + " || " + customer.Address);
            }
        }

        public Customer SearchCustomers()
        {
            Console.Write(Constant.MESSAGE_CUSTOMER_ID);
            if (int.TryParse(Console.ReadLine(), out int customerId))
            {
                return _customerServices.GetCustomerById(customerId);
            }
            throw new Exception(Constant.DATA_NOT_VALID);
        }
    }
}
