﻿
namespace TrongHQ_MiniProject_QLTV.Common
{
    public static class Constant
    {
        #region Constant value

        public const int PAGE_NUM = 10;

        #endregion

        #region Message
        public const string MESSAGE_PRESS_ANYTHING = "Bấm bất kỳ nút nào để thoát";
        public const string MESSAGE_CHOICE = "Lựa chọn của bạn: ";
        public const string MESSAGE_LIST_BOOK = "ID || Tiêu đề || Tác giả || Thể loại || Năm xuất bản || Số lượng\n";
        public const string MESSAGE_LIST_BORROW = "ID || ID sách || Tên sách || ID khách hàng || Tên khách hàng || Ngày mượn || Ngày hết hạn || Trạng thái\n";
        public const string MESSAGE_LIST_CUSTOMER = "ID || Tên khách hàng || Địa chỉ\n";
        public const string MESSAGE_SLASH = "----------------------------------------------------\n";
        public const string MESSAGE_FIND_BOOK_BY_ID = "Nhập ID của cuốn sách để xem chi tiết: ";
        public const string MESSAGE_FIND_CUSTOMER_BY_ID = "Nhập ID của khách hàng để xem chi tiết: ";
        public const string MESSAGE_FIND_BORROW_BY_ID = "Nhập ID mượn sách để trả sách: ";
        public const string MESSAGE_DELETE_CONFIRMATION = "Bạn có chắc chắn muốn xóa? (Y/N) ";
        public const string MESSAGE_DELETE_SUCCESS = "Xóa thành công!\n";
        public const string MESSAGE_END = "Cảm ơn vì đã sử dụng ứng dụng của chúng tôi!";
        public const string MESSAGE_SEED_DATA = "Lấy dữ liệu thành công!\n";
        public const string MESSAGE_GETTING_DATA = "Đang lấy dữ liệu...\n";

        //Update Book

        public const string MESSAGE_UPDATE_BOOK_TITLE = "Nhập tiêu đề: ";
        public const string MESSAGE_UPDATE_BOOK_AUTHOR = "Nhập tên tác giả: ";
        public const string MESSAGE_UPDATE_BOOK_PUBLICATION_YEAR = "Nhập năm sản xuất: ";
        public const string MESSAGE_UPDATE_BOOK_QUANTITY = "Nhập số lượng: ";
        public const string MESSAGE_UPDATE_BOOK_GENRE = "Nhập thể loại: ";
        public const string MESSAGE_UPDATE_BOOK_CHANGE_SUCCESS = "Chỉnh sửa thành công!\n";
        public const string MESSAGE_UPDATE_BOOK_CHANGE_FAIL = "Không có thay đổi!\n";
        public const string MESSAGE_ADD_BOOK_SUCCESS = "Thêm sách thành công!\n";
        public const string MESSAGE_BOOK_SEARCH_KEYWORD = "Nhập từ khóa: ";

        //Review

        public const string MESSAGE_REVIEW_TITLE = "Nhập tiêu đề đánh giá\n";
        public const string MESSAGE_REVIEW_CONTENT = "Nhập nội dung đánh giá\n";
        public const string MESSAGE_REVIEW_RATING = "Nhập xếp hạng đánh giá (1-5)\n";
        public const string MESSAGE_REVIEW_SUCCESS = "Thêm đánh giá thành công!\n";
        public const string MESSAGE_ID = "ID: ";
        public const string MESSAGE_TITLE = "Title: ";
        public const string MESSAGE_CONTENT = "Content: ";
        public const string MESSAGE_RATING = "Rating: ";

        //Customer
        public const string MESSAGE_CUSTOMER_ID= "Nhập Id của khách hàng: ";
        public const string MESSAGE_CUSTOMER_NAME= "Nhập tên của khách hàng: ";
        public const string MESSAGE_CUSTOMER_ADDRESS= "Nhập địa chỉ của khách hàng: ";
        public const string MESSAGE_ADD_CUSTOMER_SUCCESS= "Thêm khách hàng thành công ";

        //Borrow
        public const string MESSAGE_DATE_START = "Nhập thông tin ngày mượn: ";
        public const string MESSAGE_DATE_DUE = "Nhập thông tin ngày hết hạn: ";
        public const string MESSAGE_CUSTOMER_CONFIRMATION = "Bạn có chắc chắn chọn khách hàng này không? (Y/N) ";
        public const string MESSAGE_BORROW_SUCCESS = "Mượn sách thành công!\n";
        public const string MESSAGE_BORROW_RETURN = "Trả sách thành công!\n";



        #endregion

        #region Menu
        public const string MAIN_MENU =
              "----------- Menu thư viện -----------\n"
            + "1. Xem danh sách trong thư viện\n"
            + "2. Thêm sách mới vào thư viện\n"
            + "3. Lọc sách\n"
            + "4. Thông tin sách mượn\n"
            + "5. Quản lý người đọc\n"
            + "6. Thoát ứng dụng\n";

        public const string SUB_MENU_VIEW_BOOK =
              "----------- Menu danh sách -----------\n"
            + "\"N\" để xem trang tiếp theo\n"
            + "\"P\" để quay lại trang trước\n"
            + "\"B\" để quay lại menu chính\n"
            + "\"F\" để tìm thông tin chi tiết của cuốn sách\n";

        public const string SUB_MENU_BOOK_DETAIL =
              "----------- Menu chi tiết sách -----------\n"
            + "1. Chỉnh sửa thông tin sách\n"
            + "2. Xóa cuốn sách\n"
            + "3. Thêm đánh giá cho cuốn sách\n"
            + "4. Xem danh sách tất cả đánh giá của sách\n"
            + "5. Mượn sách\n"
            + "6. Quay lại menu danh sách\n";

        public const string SUB_MENU_EDIT_BOOK =
              "----------- Menu chỉnh sửa thông tin sách -----------\n"
            + "1. Chỉnh sửa tiêu đề sách\n"
            + "2. Chỉnh sửa tên tác giả\n"
            + "3. Chỉnh sửa năm xuất bản\n"
            + "4. Chỉnh sửa số lượng\n"
            + "5. Hoàn thành và quay về trang chi tiết\n";

        public const string SUB_MENU_BORROW_CUSTOMER =
              "----------- Menu loại khách hàng mượn -----------\n"
            + "1. Khách hàng mới\n"
            + "2. Khách hàng cũ\n";

        public const string SUB_MENU_SEARCH_BOOK =
              "----------- Menu tìm sách -----------\n"
            + "1. Tìm theo tựa đề (Title)\n"
            + "2. Tìm theo tên tác giá (Author)\n"
            + "3. Tìm theo năm xuất bản (Publication Year)\n"
            + "4. Tìm theo thể loại (Genre)\n"
            + "5. Quay lại menu chính\n";

        public const string SUB_MENU_BORROW_BOOK =
              "----------- Menu sách mượn -----------\n"
            + "\"N\" để xem trang tiếp theo\n"
            + "\"P\" để quay lại trang trước\n"
            + "\"S\" để tìm kiếm danh sách người mượn sách\n"
            + "\"I\" để nhập ID trả sách\n"
            + "\"B\" để quay lại menu chính\n";

        public const string SUB_MENU_SEARCH_BORROW =
              "----------- Menu tìm người mượn sách -----------\n"
            + "1. Tìm kiếm bằng tên người mượn\n"
            + "2. Tìm kiếm bằng tên cuốn sách\n"
            + "3. Quay lại menu mượn sách\n";

        public const string SUB_MENU_MANAGE_CUSTOMER =
              "----------- Menu danh sách người đọc -----------\n"
            + "\"N\" để xem trang tiếp theo\n"
            + "\"P\" để quay lại trang trước\n"
            + "\"A\" để thêm khách hàng\n"
            + "\"F\" để tìm thông tin chi tiết của khách hàng\n"
            + "\"B\" để quay lại menu chính\n";

        public const string SUB_MENU_CUSTOMER_DETAIL =
              "----------- Menu chi tiết khách hàng -----------\n"
            + "1. Chỉnh sửa thông tin khách hàng\n"
            + "2. Xóa khách hàng\n"
            + "3. Quay lại menu danh sách khách hàng\n";

        public const string SUB_MENU_CUSTOMER_UPDATE =
              "----------- Menu chỉnh sửa thông tin khách hàng -----------\n"
            + "1. Chỉnh sửa tên khách hàng\n"
            + "2. Chỉnh sửa địa chỉ khách hàng\n"
            + "3. Hoàn thành và quay về trang chi tiết\n";

        #endregion

        #region Enum
        public enum SEARCH_BOOK
        {
            Name = 1,
            Author = 2,
            PublicationYear = 3,
            Genre = 4
        }
        public enum BORROW_STATUS
        {
            Borrowed = 0,
            Returned = 1
        }
        public enum SEARCH_BORROW
        {
            Customer = 1,
            Book = 2
        }
        #endregion

        #region Exception
        public const string DATA_NOT_FOUND = "Exception: Không tìm thấy dữ liệu!";
        public const string DATA_NOT_VALID = "Exception: Dữ liệu không hợp lệ!";
        public const string DATA_DUPLICATED = "Exception: Dữ liệu trùng với dữ liệu ban đầu!";
        public const string SEARCH_TYPE_NOT_VALID = "Exception: Không tìm thấy loại tìm kiếm!";
        public const string DATA_EMPTY = "Exception: Danh sách rỗng!";
        public const string DATA_INPUT_EMPTY = "Exception: Dữ liệu rỗng!";
        public const string LAST_PAGE = "Exception: Đây đã là trang cuối cùng!";
        public const string FIRST_PAGE = "Exception: Đây là trang đầu tiên!";
        public const string MESSAGE_DELETE_BORROWED = "Exception: Cuốn sách đang được mượn không xóa được!\n";
        public const string DATE_NOT_VALID = "Exception: Thời gian nhập không hợp lý!";
        public const string BORROW_RETURNED = "Exception: Khách hàng đã trả cuốn sách này!\n";
        public const string QUANTITY_CONSTRAINT = "Exception: Số lượng không được là số âm!\n";
        public const string BORROWED_QUANTITY_CONSTRAINT = "Exception: Số lượng mượn không được nhỏ hơn số lượng\n";
        #endregion
    }
}
