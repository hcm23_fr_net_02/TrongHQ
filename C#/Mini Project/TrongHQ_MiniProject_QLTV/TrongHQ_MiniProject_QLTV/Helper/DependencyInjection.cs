﻿using Microsoft.Extensions.DependencyInjection;
using TrongHQ_MiniProject_QLTV.Presenter.Implements;
using TrongHQ_MiniProject_QLTV.Presenter.Interfaces;
using TrongHQ_MiniProject_QLTV.Repositories.Implements;
using TrongHQ_MiniProject_QLTV.Repositories.Interfaces;
using TrongHQ_MiniProject_QLTV.Services.Implements;
using TrongHQ_MiniProject_QLTV.Services.Interfaces;
using TrongHQ_MiniProject_QLTV.UnitOfWork.Implements;
using TrongHQ_MiniProject_QLTV.UnitOfWork.Interfaces;

namespace TrongHQ_MiniProject_QLTV.Helper
{
    public static class DependencyInjection
    {
        public static IServiceCollection ConfigureDependencyInjection(this IServiceCollection services)
        {
            // Unit of work
            services.AddScoped<IUnitOfWork, UnitOfWorkClass>();

            // Repositories
            services.AddScoped<IBookRepository, BookRepository>();
            services.AddScoped<IBorrowRepository, BorrowRepository>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IReviewRepository, ReviewRepository>();

            // Services
            services.AddScoped<IBookServices, BookServices>();
            services.AddScoped<IBorrowService, BorrowServices>();
            services.AddScoped<ICustomerServices, CustomerServices>();
            services.AddScoped<IReviewServices, ReviewServices>();

            // Presenter
            services.AddScoped<IMainPresenter, MainPresenter>();

            return services;
        }
    }
}
